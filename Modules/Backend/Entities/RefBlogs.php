<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefBlogs extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'ref_blogs';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\BannerFactory::new();
    }
}
