<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefKategoriAnggunan extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'ref_kategori_anggunan';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\RefKategoriAnggunanFactory::new();
    }
}
