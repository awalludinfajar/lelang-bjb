<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KisaranLuas extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'mst_luas';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\KisaranLuasFactory::new();
    }
}
