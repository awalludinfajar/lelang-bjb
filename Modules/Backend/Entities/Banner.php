<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class banner extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'banner_management';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\BannerFactory::new();
    }
}
