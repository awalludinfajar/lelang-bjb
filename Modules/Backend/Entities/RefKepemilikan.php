<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefKepemilikan extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'mst_kepemilikan';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\RefKepemilikanFactory::new();
    }
}
