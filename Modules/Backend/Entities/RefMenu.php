<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefMenu extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'ref_menu_frontend';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\RefMenuFactory::new();
    }
}
