<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefDuration extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'ref_duration_slide';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\RefDurationFactory::new();
    }
}
