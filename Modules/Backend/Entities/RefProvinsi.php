<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RefProvinsi extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'ref_provinsi';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\RefProvinsiFactory::new();
    }
}
