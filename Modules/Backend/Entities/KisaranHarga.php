<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KisaranHarga extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'mst_harga';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\KisaranHargaFactory::new();
    }
}
