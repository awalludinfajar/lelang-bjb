<?php

namespace Modules\Backend\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Userrole extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'userrole';

    protected static function newFactory()
    {
        return \Modules\Backend\Database\factories\UserroleFactory::new();
    }
}
