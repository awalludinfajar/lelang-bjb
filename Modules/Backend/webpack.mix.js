// const dotenvExpand = require('dotenv-expand');
// dotenvExpand(require('dotenv').config({ path: '../../.env'/*, debug: true*/}));

const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.styles([
  __dirname + '/Resources/assets/backend/vendors/iconfonts/mdi/css/materialdesignicons.min.css',
  __dirname + '/Resources/assets/backend/vendors/iconfonts/ionicons/dist/css/ionicons.css',
  __dirname + '/Resources/assets/backend/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css',
  __dirname + '/Resources/assets/backend/vendors/css/vendor.bundle.base.css',
  __dirname + '/Resources/assets/backend/vendors/css/vendor.bundle.addons.css',
  __dirname + '/Resources/assets/backend/css/shared/style.css',
  __dirname + '/Resources/assets/backend/css/demo_1/style.css',
],__dirname + '/Resources/assets/css/backend.css');

mix.scripts([
  __dirname + '/Resources/assets/backend/vendors/js/vendor.bundle.base.js',
  __dirname + '/Resources/assets/backend/vendors/js/vendor.bundle.addons.js',
  __dirname + '/Resources/assets/backend/js/shared/off-canvas.js',
  __dirname + '/Resources/assets/backend/js/shared/misc.js',
],__dirname + '/Resources/assets/js/backend.js');

// mix.js(__dirname + '/Resources/assets/js/app.js', 'js/backend.js')
//     .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/backend.css');

if (mix.inProduction()) {
    mix.version();
}
