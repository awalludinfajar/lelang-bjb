<div class="modal fade bs-example-modal-md" id="showupdate" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Input Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="margin-right: 6px;">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="post">
          @csrf
          <div class="form-group row">
            <label for="nm_kep" class="col-sm-3 col-form-label">Nama Kepemilikan</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="nm_kep" id="nm_kep" placeholder="Kepemilikan" data-rule-required="true" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="desk" class="col-sm-3 col-form-label">Deskripsi</label>
            <div class="col-sm-9">
              <textarea name="deskr" id="deskr" class="form-control" rows="8" cols="80" placeholder="Deskripsikan jenis Kepemilikan"></textarea>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block">Simpan</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
