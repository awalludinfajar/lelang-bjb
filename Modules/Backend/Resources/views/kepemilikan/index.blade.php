@extends('backend::layouts.master')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="float-right" style="margin-bottom:15px;">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md" id="menux" style="margin-top: -15px;">Input Jenis</button>
        </div>
        <table id="tabledat" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Deskripsi</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

@include('backend::kepemilikan.inputkepemilikan')

@endsection

@section('custom_js')
<script type="text/javascript">
  var table = $('#tabledat').DataTable({
    processing: true,
    'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
      },
      serverSide: true,
      ordering: true,
      ajax: {
        url: `{{ route('kepemilikanajax') }}`,
        type: 'GET',
      },
      columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
        {data: 'jenis_kepemilikan',name: 'jenis_kepemilikan'},
        {data: 'deskripsi',name: 'deskripsi'},
        {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
      ]
    });

    function editdata(id) {
      $.ajax({
        type:"GET",
        url : "{{ url('backend/jeniskepemilikan/get') }}/"+id,
        contentType:"application/json",
        dataType: "json",
        success:function(res) {
          $('#nm_kep').val(res['jenis_kepemilikan']);
          $('#deskr').val(res['deskripsi']);
          $('form[class="forms-sample"]').attr('action', "{{ url('backend/jeniskepemilikan/set') }}/"+id);
        }
      });
    }
</script>
@endsection
