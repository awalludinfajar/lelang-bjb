@extends('backend::layouts.master')

@section('content')
<meta name="_token" content="{{ csrf_token() }}">
<ul class="nav nav-tabs" role="tablist" style="margin-top: -25px;">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#bunga" role="tab">Bunga</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#jhuls" role="tab">Jumlah Hari Untuk Lelang Segera</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#jhat" role="tab">Jumlah Hari Anggunan Terbaru</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#bdt" role="tab">Banner Delay Time</a>
  </li>
</ul>

<div class="tab-content">
  <div class="tab-pane row fade in active show" id="bunga"  name="backend" role="tabpanel">
    @include('backend::parameter.bunga')
  </div>
  <div class="tab-pane row fade" id="jhuls" name="frontend" role="tabpanel">
  </div>
  <div class="tab-pane row fade" id="jhat" name="frontend" role="tabpanel">
  </div>
  <div class="tab-pane row fade" id="bdt" name="frontend" role="tabpanel">
    @include('backend::parameter.slideduration')
  </div>
</div>
@endsection

@section('custom_js')
<script type="text/javascript">
$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
  });
});

  $.ajax({
    type:"GET",
    url : "{{ url('api/api_duration_banner') }}",
    contentType:"application/json",
    dataType: "json",
    success:function(res) {
      $('#durati').val(res['duration']);
    }
  });

  $('#send').click(function () {
    let du = $('#durati').val();
    $.ajax({
      type:"GET",
      url : "{{ url('backend/parameter/duration/store') }}",
      contentType:"application/json",
      dataType: "json",
      data : {
        'duration':du
      },
      success:function(res) {
        console.log(res);
      }
    });
  });

  $.ajax({
    type:"GET",
    url : "{{ url('api/api_kategori_agunan') }}",
    contentType:"application/json",
    dataType: "json",
    success:function(res) {
      let isi = '<option value="">- Kategori Agunan -</option>';
      for (var i = 0; i < res.length; i++) {
        isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama_kategori']+'</option>';
      }
      $("#kategori").html(isi);

      $("#kategori").select2({
        dropdownParent: $('#showupdate')
      });
    }
  });

  var tablebunga = $('#tablebunga').DataTable({
    processing: true,
    'language': {
      'loadingRecords': '&nbsp;',
      'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
      url: `{{ route('show_bunga') }}`,
      type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'nama_kategori',name: 'nama_kategori'},
      {data: 'bunga',name: 'bunga'},
      {data: 'deskripsi',name: 'deskripsi'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  $('#menux').click(function () {
    $('#kategori').val(null).trigger('change');
    $('#bngaa').val('');
    $('#desk').val('');
    $('label[for="kategori"]').html("Kategori");
  });

  $('#sendbunga').click(function () {
    var isi = {
      katid: $('#kategori').val(),
      bunga: $('#bngaa').val(),
      durat: $('#desk').val()
    }
    if ($('#idx').val() == undefined) {
      $.ajax({
        type:"POST",
        url : "{{ route('save_bunga') }}",
        contentType:"application/json",
        dataType: "json",
        data : JSON.stringify(isi),
        success:function(res) {
          alert('Data Berhasil di Tambahkan');
          $('#kategori').val(null).trigger('change');
          $('#bngaa').val('');
          $('#desk').val('');
          $('#showupdate').modal('hide');
          $('div[class="modal-backdrop fade show"]').removeAttr('class');
        }
      });
    } else {
      $.ajax({
        type:"POST",
        url : "{{ url('backend/parameter/bunga/update') }}/"+$('#idx').val(),
        contentType:"application/json",
        dataType: "json",
        data : JSON.stringify(isi),
        success:function(res) {
          alert('Data Berhasil di Update');
          $('#kategori').val(null).trigger('change');
          $('#bngaa').val('');
          $('#desk').val('');
          $('#showupdate').modal('hide');
          $('div[class="modal-backdrop fade show"]').removeAttr('class');
          tablebunga.ajax.reload();
        }
      });
    }

  });

  function editdataluas(id) {
    $.ajax({
      type:"GET",
      url : "{{ url('backend/parameter/bunga/set') }}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('label[for="kategori"]').html('<input type="hidden" id="idx" value="'+res['id']+'"> Kategori')
        var newOption = new Option(res['nama_kategori'], res['kategori_id'], true, true);
        $('#kategori').append(newOption).trigger('change');
        $('#bngaa').val(res['bunga']);
        $('#desk').val(res['deskripsi']);
      }
    });
  }

  function removeluas(id) {
    let cof = confirm('Apakah Anda yakin akan menghapus data Tersebut ?');
    if (cof) {
      $.ajax({
        type:"GET",
        url : "{{ url('backend/parameter/bunga/destroy') }}/"+id,
        contentType:"application/json",
        dataType: "json",
        success:function(res) {
          alert(res['message']);
          tablebunga.ajax.reload();
        }
      });
    }
  }
</script>
@endsection
