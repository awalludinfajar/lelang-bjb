<div class="modal fade bs-example-modal-md" id="showupdate" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Input Bunga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="margin-right: 6px;">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <label for="kategori" class="col-sm-3 col-form-label">Kategori</label>
          <div class="col-sm-9">
            <select class="form-control" name="kategori" id="kategori" style="width: 480px;"></select>
          </div>
        </div>
        <div class="form-group row">
          <label for="bunga" class="col-sm-3 col-form-label">Bunga</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="bunga" id="bngaa" placeholder="Bunga (%)">
          </div>
        </div>
        <div class="form-group row">
          <label for="deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
          <div class="col-sm-9">
            <textarea class="form-control" name="desk" placeholder="Deskripsi" id="desk" rows="8" cols="80"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block" id="sendbunga">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</div>
