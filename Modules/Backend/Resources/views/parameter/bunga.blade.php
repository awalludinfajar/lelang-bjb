<div class="col-md-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="float-right" style="margin-bottom:15px;">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md" id="menux" style="margin-top: -15px;">Input Bunga</button>
      </div>
      <table id="tablebunga" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
        <thead>
          <tr>
            <th>No</th>
            <th>Kategori Agunan</th>
            <th>Bunga</th>
            <th>Deskripsi</th>
            <th>Action</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

@include('backend::parameter.inputbunga')
