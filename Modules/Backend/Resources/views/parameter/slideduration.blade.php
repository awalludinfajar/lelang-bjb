<div class="col-md-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="form-group row">
        <label for="nm_banner" class="col-sm-3 col-form-label">time transition duration (Second)</label>
        <div class="col-sm-6">
          <input type="number" class="form-control" name="durati" id="durati" required>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <button class="btn btn-primary submit-btn btn-block" id="send">Simpan</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
