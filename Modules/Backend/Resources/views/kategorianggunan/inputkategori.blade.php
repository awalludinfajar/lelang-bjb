<div class="modal fade bs-example-modal-md" id="showupdate" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Input Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="margin-right: 6px;">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="post">
          @csrf
          <div class="form-group row">
            <label for="nm_banner" class="col-sm-3 col-form-label">Nama Kategori</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="nm_kate" id="nm_kate" placeholder="Nama" data-rule-required="true" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="nm_banner" class="col-sm-3 col-form-label">Simpan Pada Menu</label>
            <div class="col-sm-9">
              <div class="row" style="margin-left: 5px; margin-top: 5px;">
                <div class="col-sm-5">
                  <input type="radio" class="form-check-input" name="store" id="activenx" value="1"> Yes
                </div>
                <div class="col-sm-5">
                  <input type="radio" class="form-check-input" name="store" id="activeny" value="0"> No
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row" id="isyes">
            <label for="nm_banner" class="col-sm-3 col-form-label">Parrent Menu</label>
            <div class="col-sm-9">
              <select class="form-control" name="nm_parrent" id="nm_parrent" data-rule-required="true">
              </select>
            </div>
          </div>
          <div class="form-group row" id="issyes">
            <label for="nm_banner" class="col-sm-3 col-form-label">Link / urutan</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" name="linkm" id="linkm" placeholder="Link" data-rule-required="true">
            </div>
            <div class="col-sm-4">
              <input type="number" class="form-control" name="urutn" id="urutn" placeholder="Urutan Menu" data-rule-required="true">
            </div>
          </div>
          <div class="form-group row">
            <label for="nm_banner" class="col-sm-3 col-form-label">Status</label>
            <div class="col-sm-9">
              <div class="row" style="margin-left: 5px; margin-top: 5px;">
                <div class="col-sm-5">
                  <input type="radio" class="form-check-input" name="activen" id="activenx" value="1"> Active
                </div>
                <div class="col-sm-5">
                  <input type="radio" class="form-check-input" name="activen" id="activeny" value="0"> Non-active
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block">Simpan</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
