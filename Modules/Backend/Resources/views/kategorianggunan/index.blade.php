@extends('backend::layouts.master')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="float-right" style="margin-bottom:15px;">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md" id="menux" style="margin-top: -15px;">Input Kategori</button>
        </div>
        <table id="tabledat" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Tampilkan Pada Menu</th>
              <th>Active</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

@include('backend::kategorianggunan.inputkategori')

@endsection

@section('custom_js')
<script type="text/javascript">
  $('#menux').click(function () {
    setmenu();
    $('#nm_kate').val('');
    $('#isyes').hide();$('#issyes').hide();
    $("input[name=store]").prop('checked', false);
    $('#linkm').val('');
    $('#urutn').val('');
    $("input[name=activen]").prop('checked', false);
    $('form[class="forms-sample"]').removeAttr("action");
  });

  $('#isyes').hide();$('#issyes').hide();
  $('#activenx').click(function () {
    $('#isyes').show();$('#issyes').show();
  });
  $('#activeny').click(function () {
    $('#isyes').hide();$('#issyes').hide();
  });

  var table = $('#tabledat').DataTable({
    processing: true,
    'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
        url: `{{ route('showajax') }}`,
        type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'nama_kategori',name: 'nama_kategori'},
      {data: 'store_menu_frontend',name: 'store_menu_frontend'},
      {data: 'active',name: 'active'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  function editdata(id) {
    setmenu();
    $.ajax({
      type:"GET",
      url : "{{ url('backend/kategorianggunan/get') }}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#nm_kate').val(res['nama_kategori']);
        $("input[name=store][value=" + res['store_menu_frontend'] + "]").prop('checked', true);
        if (res['store_menu_frontend'] == '1') {
          $('#isyes').show();$('#issyes').show();
          $('#nm_parrent > [value="'+res['parrent']+'"]').attr("selected", "true");
          $('#linkm').val(res['link']);
          $('#urutn').val(res['urutan']);
        } else { $('#isyes').hide();$('#issyes').hide(); }
        $("input[name=activen][value=" + res['active'] + "]").prop('checked', true);
        $('form[class="forms-sample"]').attr("action","{{ url('backend/kategorianggunan/update') }}/"+id);
      }
    });
  }
</script>
@endsection
