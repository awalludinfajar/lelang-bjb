@extends('backend::layouts.master')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="float-right" style="margin-bottom:15px;">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md" id="bannersx" style="margin-top: -15px;">Input Banner</button>
        </div>
        <table id="tabledat" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Deskripsi</th>
              <th>Slide-ke</th>
              <th>Active</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="viewimg" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div style="width: 100%; height: 100%;">
          <img src="{{asset('assets/upload/banner/img/no-image.png')}}" alt="" id="setGambar" style="width: inherit; height: inherit;">
        </div>
      </div>
    </div>
  </div>
</div>


@include('backend::bannermanagement.inputbanner')

@endsection

@section('custom_js')
<script type="text/javascript">
  $('#upload').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#image_show').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  var table = $('#tabledat').DataTable({
    processing: true,
    'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
        url: `{{ route('show_data_banner') }}`,
        type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'nama_banner',name: 'nama_banner'},
      {data: 'deskripsi',name: 'deskripsi'},
      {data: 'slide',name: 'slide'},
      {data: 'active',name: 'active'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  function viewimage(id) {
    $.ajax({
      type:"GET",
      url : "{{url('backend/bannermanagement/get')}}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#setGambar').attr("src", "{{asset('upload/banner/img')}}/"+res['image']);
      }
    });
  }

  function editdata(id) {
    $.ajax({
      type:"GET",
      url : "{{url('backend/bannermanagement/get')}}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#nm_banner').val(res['nama_banner']);
        $('#desc').val(res['deskripsi']);
        $('#image_show').attr("src", "{{asset('upload/banner/img')}}/"+res['image']);
        $('#slide').val(res['slide']);
        $("input[name=activen][value=" + res['active'] + "]").prop('checked', true);
        $("form[class=forms-sample]").attr("action","{{url('backend/bannermanagement/set')}}/"+id);
      }
    });
  }

  $('#bannersx').click(function () {
    $("form[class=forms-sample]").removeAttr("action");
    $('#nm_banner').val('');
    $('#desc').val('');
    $('#image_show').attr("src", "{{asset('assets/upload/banner/img/no-image.png')}}");
    $('#slide').val('');
    $("input[name=activen]").prop('checked', false);
  });
</script>
@endsection
