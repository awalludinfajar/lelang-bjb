<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Input Banner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="margin-right: 6px;">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group row">
            <label for="nm_banner" class="col-sm-3 col-form-label">Nama Banner</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="nm_banner" id="nm_banner" placeholder="Nama" required>
            </div>
          </div>
          <div class="form-group row">
            <label for="desc" class="col-sm-3 col-form-label">Deskripsi</label>
            <div class="col-sm-9">
              <textarea class="form-control" name="desc" id="desc" rows="8" cols="80" placeholder="Deskripsi" required></textarea>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-3">
              <center class="m-t-40">
                <input type="file" name="url_image" accept="image/*" id="upload" hidden>
                <a href="javascript:void(0)" onclick="$('#upload').click()" class="btn btn-secondary">
                  <i class="fa fa-image" aria-hidden="true"></i>
                  <span class="value-digit">Upload Gambar</span>
                </a>
              </center>
            </div>
            <!-- <label for="image_upload" class="col-sm-3 col-form-label">Upload Gambar</label> -->
            <div class="col-sm-9">
              <img id="image_show" style="width: 100%; height: 250px;">
            </div>
          </div>
          <div class="form-group row">
            <label for="image_upload" class="col-sm-3 col-form-label">Slide ke / active</label>
            <div class="col-sm-4">
              <input type="number" name="slide" id="slide" class="form-control">
            </div>
            <div class="col-sm-5">
              <div class="row" style="margin-left: 5px; margin-top: 5px;">
                <div class="col-sm-5">
                  <input type="radio" class="form-check-input" name="activen" id="activenx" value="1"> Active
                </div>
                <div class="col-sm-5">
                  <input type="radio" class="form-check-input" name="activen" id="activeny" value="0"> Non-Active
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block">Simpan</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
