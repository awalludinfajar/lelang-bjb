@extends('backend::layouts.master')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="float-right" style="margin-bottom:15px;">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md" id="menux" style="margin-top: -15px;">Input Menu</button>
        </div>
        <table id="tabledat" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Menu</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

@include('backend::menumanagement.inputmenu')

@endsection

@section('custom_js')
<script type="text/javascript">
  $('#menux').click(function () {
    setmenu();
    $('#nm_menu').val('');
    $('#lnk_mnu').val('');
    $('#urutan').val('');
    $("input[name=activen]").prop('checked', false);
    $('form[class="forms-sample"]').removeAttr("action");
  });

  var table = $('#tabledat').DataTable({
    processing: true,
    'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
        url: `{{ route('damenu') }}`,
        type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'nama_menu',name: 'nama_menu'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  function editdata(id) {
    setmenu();
    $.ajax({
      type:"GET",
      url : "{{ url('backend/menumanagement/update') }}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#nm_menu').val(res['nama_menu']);
        $('#nm_parrent > [value="'+res['parrent']+'"]').attr("selected", "true");
        $('#lnk_mnu').val(res['link']);
        $('#urutan').val(res['urutan']);
        $("input[name=activen][value=" + res['class_active'] + "]").prop('checked', true);
        $('form[class="forms-sample"]').attr("action","{{ url('backend/menumanagement/update') }}/"+id);
      }
    });
  }
</script>
@endsection
