<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-category">Main Menu</li>
    <li class="nav-item">
      <a class="nav-link" href="/backend">
        <span class="menu-title">Dashboard</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('agunan') }}">
        <span class="menu-title">Input Agunan</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('paramm') }}">
        <span class="menu-title">Parameter</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('rata') }}">
        <span class="menu-title">Kisaran Harga & Luas</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('kepemilik') }}">
        <span class="menu-title">Jenis Kepemilikan</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('anggunankategori') }}">
        <span class="menu-title">Jenis Agunan</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('menus') }}">
        <span class="menu-title">Menu Management</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('banner') }}">
        <span class="menu-title">Banner Setting</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('berita') }}">
        <span class="menu-title">News Update</span>
      </a>
    </li>

    <!-- <li class="nav-item">
      <a class="nav-link" href="{{ route('userm') }}">
        <span class="menu-title">User setting</span>
      </a>
    </li> -->

  </ul>
</nav>
