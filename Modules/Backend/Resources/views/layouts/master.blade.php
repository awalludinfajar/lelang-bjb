<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>Module Backend</title>

      {{-- Laravel Mix - CSS File --}}
      <link rel="stylesheet" href="{{ mix('css/backend.css') }}">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.css">
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
      @yield('custom_css')
    </head>
    <body>
      <div class="container-scroller">
        @include('backend::layouts.header')
        <div class="container-fluid page-body-wrapper">
          @include('backend::layouts.sidebar')
          <div class="main-panel">
            <div class="content-wrapper">
              <div class="row page-title-header">
                <div class="col-12">
                  <div class="page-header">
                    <h4 class="page-title">{{ $module ?? '' }}</h4>
                  </div>
                </div>
              </div>
              @include('backend::layouts.message_alert')
              @yield('content')
            </div>
            @include('backend::layouts.footer')
          </div>
        </div>
      </div>

      {{-- Laravel Mix - JS File --}}
      <script src="{{ mix('js/backend.js') }}"></script>
      <script src="{{ asset('js/public.js') }}"></script>
      <script src="{{asset('js/sweetalert/sweetalert2.all.min.js')}}"></script>
      <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js" type="text/javascript"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.js" type="text/javascript"></script>
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js" type="text/javascript"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
      <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js" type="text/javascript"></script>
      <script type="text/javascript">
      function formatRupiah(angka, prefix){
        let ang = angka == NaN ? 0 : angka;
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        if(ribuan){
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
      }

        function setmenu() {
          $.ajax({
            type:"GET",
            url : "{{ route('datamenu') }}",
            contentType:"application/json",
            dataType: "json",
            success:function(res) {
              let list = '<option value="0">-- Pilih Parrent Menu --</option>';
              for (var i = 0; i < res.length; i++) {
                list = list + '<option value="'+res[i]['id']+'">'+res[i]['nama_menu']+'</option>';
                list = list + '<tr><td>';
                if (getlength(res[i]['id']) == 0) {
                  list = list + '<em style="color:#555;">'+res[i]['nama_menu']+'</em>';
                } else {
                  list = list + '<strong>'+res[i]['nama_menu']+'</strong>';
                }
                list = list + '</td></tr>';
              }
              $('#nm_parrent').html(list);
            }
          });
        }

        function getlength(parrent) {
          let hs = 0;
          $.ajax({
            type:"GET",
            url : "{{ url('backend/menumanagement/get') }}/"+parrent,
            contentType:"application/json",
            dataType: "json",
            success:function(rez) {
              hs = rez.length;
            },
            async: hs
          });
          return hs;
        }
      </script>
      @yield('custom_js')
    </body>
</html>
