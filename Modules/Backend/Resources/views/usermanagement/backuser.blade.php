<div class="col-md-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <div class="float-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md">Data Category user</button>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newuser">Create New User</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 65%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Category User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="form-group row">
          <div class="col-md-6">
            <input type="hidden" name="id_kn" id="id_kn" value="">
            <input type="text" class="form-control" name="cat_name" id="cat_name" placeholder="Category Name">
          </div>
          <div class="col-md-6">
            <div class="row" style="margin-left: 5px; margin-top: 5px;">
              <div class="col-sm-5">
                <input type="radio" class="form-check-input" name="activen" id="activen" value="1"> Active
              </div>
              <div class="col-sm-5">
                <input type="radio" class="form-check-input" name="activen" id="activen" value="0"> Non-Active
              </div>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <textarea name="descr" id="descrpt" class="form-control" rows="3" cols="50" placeholder="Description"></textarea>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-12">
            <button type="button" class="btn btn-primary btn-block" id="send_data">Save</button>
          </div>
        </div>
        <hr>
        <table id="tabledat" class="table table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Jenis User</th>
              <th>Active</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="newuser">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 65%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Category User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ route('saveregis') }}" method="post">
          @csrf
          @if(session('errors'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              Something it's wrong:
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
          @endif
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="nama" placeholder="Full Name">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="username" placeholder="Username">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="email" class="form-control" name="email" placeholder="E-mail">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <select class="form-control" name="role" id="role">
              </select>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="password" class="form-control" name="password" placeholder="Password">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-check-circle-outline"></i>
                </span>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block">Register</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

@section('custom_js')
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type:"GET",
    contentType:"application/json",
    url : "{{ route('datax') }}",
    dataType: "json",
    success:function(res) {
      console.log(res);
      let me = "";
      for (var i = 0; i < res.length; i++) {
        me = me+"<option value='"+res[i]['id']+"'>"+res[i]['jenis_user']+"</option>";
      }
      $('#role').html(me);
    }
  });

  var table = $('#tabledat').DataTable({
    processing: true,
    'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
        url: `{{ route('role') }}`,
        type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex'},
      {data: 'jenis_user',name: 'jenis_user'},
      {data: 'active',name: 'active'},
      {data: 'deskripsi',name: 'deskripsi'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  $("#table-data_filter").addClass('d-flex justify-content-end');
  $("#table-data_filter>label>input").removeClass('form-control-sm');
  $("[name=table-data_length]").removeClass('form-control-sm');
  $("[name=table-data_length]").removeClass('custom-select-sm');

  $('#send_data').click(function () {
    let nam = $('#cat_name').val();
    let dec = $('#descrpt').val();
    let act = $("input[name='activen']:checked").val();
    let kondisi = $('#id_kn').val();

    if (kondisi == '') {
      $.ajax({
        type:"GET",
        contentType:"application/json",
        url : "{{ route('userrole') }}",
        dataType: "json",
        data : {
          'jenis_user': nam,
          'deskripsi':dec,
          'active':act
        },
        success:function(res) {
          var cnf = confirm('data berhasil di tambahkan');
          if (cnf == true) {
            $('#cat_name').val('');
            $('#descrpt').val('');
            $("input[name='activen']").prop('checked', false);
            table.ajax.reload();
            $(".modal-body").animate({scrollTop: 1000}, 0);
          }
        }
      });
    } else {
      $.ajax({
        type:"GET",
        contentType:"application/json",
        url : "{{ url('backend/usermanagement/category/store_edit') }}/"+kondisi,
        dataType: "json",
        data : {
          'jenis_user': nam,
          'deskripsi':dec,
          'active':act
        },
        success:function(res) {
          var cnf = confirm('data berhasil di ubah');
          if (cnf == true) {
            $('#cat_name').val('');
            $('#descrpt').val('');
            $("input[name='activen']").prop('checked', false);
            table.ajax.reload();
            $(".modal-body").animate({scrollTop: 1000}, 0);
          }
        }
      });
    }
  });

  function removeordelete(id) {
    $(this).closest("tr").remove();
    let cof = confirm('Data akan di hapus ?');
    if (cof) {
      $.ajax({
        type:"GET",
        url : "{{ url('backend/usermanagement/category/delete') }}/"+id,
        contentType:"application/json",
        dataType: "json",
        success:function(res) {
          table.ajax.reload();
          var cnf = confirm('Data Berhasil Di hapus..');
          if (cnf == true) {
            $('#cat_name').val('');
            $('#descrpt').val('');
            $("input[name='activen']").prop('checked', false);
          }
        }
      });
    }
  }

  function editdata(id) {
    $.ajax({
      type:"GET",
      url : "{{ url('backend/usermanagement/category/edit/') }}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#id_kn').val(res['id']);
        $('#cat_name').val(res['jenis_user']);
        $('#descrpt').val(res['deskripsi']);
        $("input[name=activen][value=" + res['active'] + "]").prop('checked', true);
        $(".modal-body").animate({scrollTop: 0}, 1000);
      }
    });
  }
</script>
@endsection
