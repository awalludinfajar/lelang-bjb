@extends('backend::layouts.master')

@section('content')
<ul class="nav nav-tabs" role="tablist" style="margin-top: -25px;">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#backend" role="tab">User Backend</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#frontend" role="tab">User Frontend</a>
  </li>
</ul>

<div class="tab-content">
  <div class="tab-pane row fade in active show" id="backend"  name="backend" role="tabpanel">
    @include('backend::usermanagement.backuser')
  </div>
  <div class="tab-pane row fade" id="frontend" name="frontend" role="tabpanel">
    @include('backend::usermanagement.frontuser')
  </div>
</div>
@endsection
