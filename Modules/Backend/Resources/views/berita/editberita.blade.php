@extends('backend::layouts.master')
@section('content')

<form action="{{ asset('/backend/beritamanagement/set/'.$data->id) }}" method="post" id="add_blogs" enctype="multipart/form-data" accept-charset="utf-8">
@csrf
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="page-content-wrapper">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="jenkep" class="col-sm-3 col-form-label">Judul Berita</label>
                        <div class="col-sm-9">
                            <input type="text" name="judul" id="judul" class="form-control" placeholder=""
                                value="<?php echo $data->judul ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="provin" class="col-sm-3 col-form-label">Jadikan Hot News</label>
                        <div class="col-sm-9">
                            <div class="row" style="margin-left: 5px; margin-top: 5px;">
                                <div class="col-sm-5">
                                    <input type="radio" class="form-check-input" name="hot" id="hot" value="1"> Yes
                                </div>
                                <div class="col-sm-5">
                                    <input type="radio" class="form-check-input" name="hot" id="hot" value="0"> No
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="jenis_berita" class="col-sm-3 col-form-label">Jenis Berita</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="jenis_berita" name="jenis_berita"
                                value="<?php echo $data->jenis_berita ?>">
                                <option value="">- Pilih Jenis Jual -</option>
                                <option value="BJB EVENT">BJB EVENT</option>
                                <option value="NEWS & RELEASE">NEWS & RELEASE</option>
                                <option value="ECONOMIC REVIEW">ECONOMIC REVIEW</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <!-- <center class="m-t-40"> -->
                            <input type="file" name="url_image" accept="image/*" id="upload" hidden>
                            <a href="javascript:void(0)" onclick="$('#upload').click()" class="btn btn-secondary">
                                <i class="fa fa-image" aria-hidden="true"></i>
                                <span class="value-digit">Upload Gambar</span>
                            </a>
                            <!-- </center> -->
                        </div>
                        <!-- <label for="image_upload" class="col-sm-3 col-form-label">Upload Gambar</label> -->
                        <div class="col-sm-9">
                            <img id="image_show" style="width: 100%; height: 250px;">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="description" class="col-sm-3 col-form-label"><u>Isi Berita</u></label>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <textarea name="description" id="description" rows="10" cols="80"
                                value="<?php echo $data->description ?>"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="image_upload" class="col-sm-3 col-form-label">Active</label>
                        <div class="col-sm-5">
                            <div class="row" style="margin-left: 5px; margin-top: 5px;">
                                <div class="col-sm-5">
                                    <input type="radio" class="form-check-input" name="active" id="activenx" value="1">
                                    Yes
                                </div>
                                <div class="col-sm-5">
                                    <input type="radio" class="form-check-input" name="active" id="activeny" value="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="tampil_beranda" class="col-sm-3 col-form-label">Tampilkan di Halaman Utama</label>
                        <div class="col-sm-5">
                            <div class="row" style="margin-left: 5px; margin-top: 5px;">
                                <div class="col-sm-5">
                                    <input type="radio" class="form-check-input" name="tampil_beranda" id="hot"
                                        value="1"> Yes
                                </div>
                                <div class="col-sm-5">
                                    <input type="radio" class="form-check-input" name="tampil_beranda" id="hot"
                                        value="0"> No
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <button class="btn btn-primary submit-btn btn-block">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection




@section('custom_js')
<script type="text/javascript">
    CKEDITOR.replace('description');
    
    $('#upload').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#image_show').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

    function viewimage(id) {
    $.ajax({
      type:"GET",
      url : "{{url('backend/bannermanagement/get')}}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#setGambar').attr("src", "{{asset('upload/banner/img')}}/"+res['image']);
      }
    });
  }
</script>
@endsection
