@extends('backend::layouts.master')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="float-right" style="margin-bottom:15px;">
        <a class="btn btn-primary nav-link" href="/backend/beritamanagement/add">
          <span class="menu-title">Input Berita</span>
        </a>
        </div>
        <div class="table-responsive">
        <table id="tablenews" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
          <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>Deskripsi</th>
              <th>Jenis</th>
              <th>Active</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="viewimg" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div style="width: 100%; height: 100%;">
          <img src="{{asset('assets/upload/banner/img/no-image.png')}}" alt="" id="setGambar" style="width: inherit; height: inherit;">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="viewdesc" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="showdesc" style="width: 100%; height: 100%;">

        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('custom_js')
<script type="text/javascript">
  $('#upload').change(function() {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#image_show').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  var table = $('#tablenews').DataTable({
    processing: true,
    'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
        url: `{{ route('show_data') }}`,
        type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'judul',name: 'judul'},
      {data: 'description',name: 'description'},
      {data: 'jenis_berita',name: 'jenis_berita'},
      {data: 'active',name: 'active'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  function viewimage(id) {
    $.ajax({
      type:"GET",
      url : "{{url('api/api_data_blogsdanberita/x')}}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        console.log(res);
        $('#setGambar').attr("src", "{{asset('upload/blogs/img')}}/"+res[0]['image']);
      }
    });
  }

  function viewdesc(id) {
    $.ajax({
      type:"GET",
      url : "{{url('api/api_data_blogsdanberita/x')}}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('#showdesc').html(res[0]['description']);
      }
    });
  }

  // function editdata(id) {
  //   $.ajax({
  //     type:"GET",
  //     url : "{{url('backend/beritamanagement/get')}}/"+id,
  //     contentType:"application/json",
  //     dataType: "json",
  //     success:function(res) {
  //       $('#judul').val(res['judul']);
  //       $('#description').val(res['description']);
  //       $('#image_show').attr("src", "{{asset('upload/banner/img')}}/"+res['image']);
  //       $('#jenis_berita').val(res['jenis_berita']);
  //       $("input[name=activen][value=" + res['active'] + "]").prop('checked', true);
  //       $("form[class=forms-sample]").attr("action","{{url('backend/beritamanagement/set')}}/"+id);
  //     }
  //   });
  // }

  // $('#bannersx').click(function () {
  //   $("form[class=forms-sample]").removeAttr("action");
  //   $('#judul').val('');
  //   $('#desc').val('');
  //   $('#image_show').attr("src", "{{asset('assets/upload/banner/img/no-image.png')}}");
  //   $('#slide').val('');
  //   $("input[name=activen]").prop('checked', false);
  // });
</script>
@endsection
