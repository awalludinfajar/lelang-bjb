@extends('backend::auth.base')

@section('login_content')
<div class="container-fluid page-body-wrapper full-page-wrapper">
  <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
    <div class="row w-100" style="margin-top: 90px;">
      <div class="col-lg-4 mx-auto">
        <div class="auto-form-wrapper">
          <form action="{{ route('store_login') }}" method="post">
            @csrf
            @if(session('errors'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              Something it's wrong:
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <ul>
                <li>{{ $errors }}</li>
              </ul>
            </div>
            @endif
            <div class="form-group">
              <label class="label">Username</label>
              <div class="input-group">
                <input type="text" class="form-control" name="username" placeholder="Username">
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="mdi mdi-check-circle-outline"></i>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="label">Password</label>
              <div class="input-group">
                <input type="password" class="form-control" name="password" placeholder="*********">
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="mdi mdi-check-circle-outline"></i>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <div class="captcha">
                  <span>{!! captcha_img() !!}</span>
                  <button type="button" class="btn btn-primary" id="reload">↻</button>
                </div>
              </div>
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="mdi mdi-check-circle-outline"></i>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <button class="btn btn-primary submit-btn btn-block">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- content-wrapper ends -->
</div>
@endsection
