<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module Backend</title>

       {{-- Laravel Mix - CSS File --}}
       <link rel="stylesheet" href="{{ mix('css/backend.css') }}">

    </head>
    <body>
      <div class="container-scroller">
        @yield('login_content')
      </div>

        {{-- Laravel Mix - JS File --}}
        <script src="{{ mix('js/backend.js') }}"></script>
    </body>
</html>

<script type="text/javascript">
$('#reload').click(function () {
  $.ajax({
    type: 'GET',
    url: "{{ route('reloadcapt') }}",
    success: function (data) {
      $(".captcha span").html(data.captcha);
    }
  });
});
</script>
