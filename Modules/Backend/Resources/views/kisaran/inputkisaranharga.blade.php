<div class="modal fade bs-example-modal-md" id="kisarharga" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Input Kisaran Harga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="margin-right: 6px;">×</span>
        </button>
      </div>
      <div class="modal-body">
        @csrf
        <div class="form-group row">
          <label for="kisarhrga" class="col-sm-3 col-form-label">Nama</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="kisarhrga" id="kisarhrga" placeholder="Kisaran Harga ex.100-200jt" data-rule-required="true" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="minha" class="col-sm-3 col-form-label">Kisaran Harga</label>
          <div class="col-sm-4">
            <input type="text" class="form-control" name="minha" id="minha" placeholder="Min" data-rule-required="true" required>
          </div>
          <div class="col-sm-1">s/d</div>
          <div class="col-sm-4">
            <input type="text" class="form-control" name="maxha" id="maxha" placeholder="Max" data-rule-required="true" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="deskhr" class="col-sm-3 col-form-label">Deskripsi</label>
          <div class="col-sm-9">
            <textarea name="desk" id="deskhr" class="form-control" rows="8" cols="80" placeholder="Deskripsi"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block"  id="inputharga">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</div>
