@extends('backend::layouts.master')

@section('content')
<meta name="_token" content="{{ csrf_token() }}">
<ul class="nav nav-tabs" role="tablist" style="margin-top: -25px;">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#luas" role="tab">Kisaran Luas</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#harga" role="tab">Kisaran Harga</a>
  </li>
</ul>

<div class="tab-content">
  <div class="tab-pane row fade in active show" id="luas"  name="backend" role="tabpanel">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="float-right" style="margin-bottom:15px;">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#kisarluas" id="menux_luas" style="margin-top: -15px;">Input Kisatan Luas</button>
            </div>
            <table id="tableluas" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kisaran Luas</th>
                  <th>Min</th>
                  <th>Max</th>
                  <th>Deskripsi</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane row fade" id="harga" name="frontend" role="tabpanel">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="float-right" style="margin-bottom:15px;">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#kisarharga" id="menux_harga" style="margin-top: -15px;">Input Kisatan Harga</button>
            </div>
            <table id="tableharga" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kisaran Harga</th>
                  <th>Min</th>
                  <th>Max</th>
                  <th>Deskripsi</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('backend::kisaran.inputkisaranluas')
@include('backend::kisaran.inputkisaranharga')

@endsection

@section('custom_js')
<script type="text/javascript">
  $(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
      }
    });
  });

  $('#menux_luas').click(function () {
    $('#kisarlus').val('');
    $('#minlu').val('');
    $('#maxlu').val('');
    $('#desk').val('');
    $('label[for="kisarlus"]').html("Nama");
  });

  $('#menux_harga').click(function () {
    $('#kisarhrga').val('');
    $('#minha').val('');
    $('#maxha').val('');
    $('#deskhr').val('');
    $('label[for="kisarhrga"]').html("Nama");
  });

  $('#minlu').keyup(function() { $(this).val(formatRupiah(this.value)); });
  $('#maxlu').keyup(function() { $(this).val(formatRupiah(this.value)); });
  $('#minha').keyup(function() { $(this).val(formatRupiah(this.value)); });
  $('#maxha').keyup(function() { $(this).val(formatRupiah(this.value)); });

  var tableluas = $('#tableluas').DataTable({
    processing: true,
    'language': {
      'loadingRecords': '&nbsp;',
      'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
      url: `{{ route('luasajax') }}`,
      type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'kisaran_luas',name: 'kisaran_luas'},
      {data: 'min_luas',name: 'min_luas'},
      {data: 'max_luas',name: 'max_luas'},
      {data: 'deskripsi',name: 'deskripsi'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  var tableharga = $('#tableharga').DataTable({
    processing: true,
    'language': {
      'loadingRecords': '&nbsp;',
      'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
      url: `{{ route('hargajax') }}`,
      type: 'GET',
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'kisaran_harga',name: 'kisaran_harga'},
      {data: 'min_harga',name: 'min_harga'},
      {data: 'max_harga',name: 'max_harga'},
      {data: 'deskripsi',name: 'deskripsi'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ]
  });

  $('#inputluas').click(function () {
    var isi = {
      kil: $('#kisarlus').val(),
      min: parseInt($('#minlu').val().replace(/\./g,'')),
      max: parseInt($('#maxlu').val().replace(/\./g,'')),
      dec: $('#desk').val()
    }
    if ($('#idluas').val() == undefined) {
      $.ajax({
        type:"POST",
        url : "{{ route('send_luas') }}",
        contentType:"application/json",
        dataType: "json",
        data: JSON.stringify(isi),
        success:function(res) {
          alert('Data Berhasil di Tambahkan');
          $('#kisarlus').val('');
          $('#minlu').val('');
          $('#maxlu').val('');
          $('#desk').val('');
          $('#kisarluas').modal('hide');
          $('div[class="modal-backdrop fade show"]').removeAttr('class');
          tableluas.ajax.reload();
        }
      });
    } else {
      $.ajax({
        type:"POST",
        url : "{{ url('backend/kisaran/luas/storeupdate') }}/"+$('#idluas').val(),
        contentType:"application/json",
        dataType: "json",
        data: JSON.stringify(isi),
        success:function(res) {
          alert('Data Berhasil di Update');
          $('#kisarlus').val('');
          $('#minlu').val('');
          $('#maxlu').val('');
          $('#desk').val('');
          $('#kisarluas').modal('hide');
          $('label[for="kisarlus"]').html("Nama");
          $('div[class="modal-backdrop fade show"]').removeAttr('class');
          tableluas.ajax.reload();
        }
      });
    }
  });

  $('#inputharga').click(function () {
    let min = $('#minha').val(); let minn = '';
    for (var i = 0; i < min.length; i++) { if (min[i] != '.') { minn = minn+min[i]; } }
    let max = $('#maxha').val(); let maxx = '';
    for (var i = 0; i < max.length; i++) { if (max[i] != '.') { maxx = maxx+max[i]; } }
    var isi = {
      kil: $('#kisarhrga').val(),
      min: minn,
      max: maxx,
      dec: $('#deskhr').val()
    }

    if ($('#idhrga').val() == undefined) {
      $.ajax({
        type:"POST",
        url : "{{ route('send_harga') }}",
        contentType:"application/json",
        dataType: "json",
        data: JSON.stringify(isi),
        success:function(res) {
          alert('Data Telah di Tambahkan');
          $('#kisarhrga').val('');
          $('#minha').val('');
          $('#maxha').val('');
          $('#desk').val('');
          $('div[class="modal-backdrop fade show"]').removeAttr('class');
          $('#kisarharga').modal('hide');
          tableharga.ajax.reload();
        }
      });
    } else {
      $.ajax({
        type:"POST",
        url : "{{ url('backend/kisaran/harga/storeupdate') }}/"+$('#idhrga').val(),
        contentType:"application/json",
        dataType: "json",
        data: JSON.stringify(isi),
        success:function(res) {
          alert('Data Telah di Update');
          $('#kisarhrga').val('');
          $('#minha').val('');
          $('#maxha').val('');
          $('#desk').val('');
          $('label[for="kisarhrga"]').html("Nama");
          $('div[class="modal-backdrop fade show"]').removeAttr('class');
          $('#kisarharga').modal('hide');
          tableharga.ajax.reload();
        }
      });
    }

  });

  function editdataluas(id) {
    $.ajax({
      type:"GET",
      url : "{{ url('backend/kisaran/luas/edit') }}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('label[for="kisarlus"]').html("Nama <input type='hidden' id='idluas' value='"+id+"'>");
        $('#kisarlus').val(res['kisaran_luas']);
        $('#minlu').val(res['min_luas']);
        $('#maxlu').val(res['max_luas']);
        $('#desk').val(res['deskripsi']);
      }
    });
  }

  function editdataharga(id) {
    $.ajax({
      type:"GET",
      url : "{{ url('backend/kisaran/harga/edit') }}/"+id,
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        $('label[for="kisarhrga"]').html("Nama <input type='hidden' id='idhrga' value='"+id+"'>");
        $('#kisarhrga').val(res['kisaran_harga']);
        $('#minha').val(formatRupiah("'"+res['min_harga']+"'"));
        $('#maxha').val(formatRupiah("'"+res['max_harga']+"'"));
        $('#deskhr').val(res['deskripsi']);
      }
    });
  }

  function removeluas(id) {
    let cof = confirm('Apakah Anda yakin akan menghapus data Tersebut ?');
    if (cof) {
      $.ajax({
        type:"GET",
        url : "{{ url('backend/kisaran/luas/destroy') }}/"+id,
        contentType:"application/json",
        dataType: "json",
        success:function(res) {
          alert(res['message']);
          tableluas.ajax.reload();
        }
      });
    }
  }

  function removeharga(id) {
    let cof = confirm('Apakah Anda yakin akan menghapus data Tersebut ?');
    if (cof) {
      $.ajax({
        type:"GET",
        url : "{{ url('backend/kisaran/harga/destroy') }}/"+id,
        contentType:"application/json",
        dataType: "json",
        success:function(res) {
          alert(res['message']);
          tableharga.ajax.reload();
        }
      });
    }
  }
</script>
@endsection
