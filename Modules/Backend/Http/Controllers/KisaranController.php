<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Entities\KisaranLuas;
use Modules\Backend\Entities\KisaranHarga;

class KisaranController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Kisaran Harga & Luas',
        'resres' => $request->session()->all()
      ];
      return view('backend::kisaran.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('backend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store_luas(Request $request)
    {
      try {
        $data = new KisaranLuas;
        $data->kisaran_luas = $request->kil;
        $data->max_luas = $request->max;
        $data->min_luas = $request->min;
        $data->deskripsi = $request->dec;
        $data->save();

        return response()->json(['message'=>'Data Berhasil di Tambahkan', 'response'=>$data], 200);
      } catch (Queryexception $e) {
        return response()->json(['message'=>'Data Gagal di Tambahkan', 'response'=>$e], 500);
      }
    }

    public function store_harga(Request $request)
    {
      try {
        $data = new KisaranHarga;
        $data->kisaran_harga = $request->kil;
        $data->max_harga = $request->max;
        $data->min_harga = $request->min;
        $data->deskripsi = $request->dec;
        $data->save();

        return response()->json(['message'=>'Data Berhasil di Tambahkan', 'response'=>$data], 200);
      } catch (Queryexception $e) {
        return response()->json(['message'=>'Data Gagal di Tambahkan', 'response'=>$e], 500);
      }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show_luas()
    {
      $data = KisaranLuas::All();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#kisarluas" onclick="editdataluas('.$data->id.')">Edit</button>
                     <a href="javascript:void(0);" class="btn btn-danger waves-effect waves-light" onclick="removeluas('.$data->id.')">Delete</a>';
          return $button;
        })
        ->rawColumns(['kisaran_luas', 'max_luas', 'min_luas', 'deskripsi', 'action'])
        ->addIndexColumn()
        ->make(true);
    }

    public function show_harga()
    {
      $data = KisaranHarga::All();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#kisarharga" onclick="editdataharga('.$data->id.')">Edit</button>
                     <a href="javascript:void(0);" class="btn btn-danger waves-effect waves-light" onclick="removeharga('.$data->id.')">Delete</a>';
          return $button;
        })
        ->editColumn('min_harga', function ($data) { return number_format($data->min_harga); })
        ->editColumn('max_harga', function ($data) { return number_format($data->max_harga); })
        ->rawColumns(['kisaran_harga', 'max_harga', 'min_harga', 'deskripsi', 'action'])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit_luas($id)
    {
        return KisaranLuas::whereId($id)->first();
    }

    public function edit_harga($id)
    {
        return KisaranHarga::whereId($id)->first();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update_luas(Request $request, $id)
    {
      try {
        $data = KisaranLuas::whereId($id)->first();
        $data->kisaran_luas = $request->kil;
        $data->max_luas = $request->max;
        $data->min_luas = $request->min;
        $data->deskripsi = $request->dec;
        $data->save();

        return response()->json(['message'=>'Data Berhasil di Update', 'response'=>$data], 200);
      } catch (Queryexception $e) {
        return response()->json(['message'=>'Data Gagal di Update', 'response'=>$e], 500);

      }

    }

    public function update_harga(Request $request, $id)
    {
      try {
        $data = KisaranHarga::whereId($id)->first();
        $data->kisaran_harga = $request->kil;
        $data->max_harga = $request->max;
        $data->min_harga = $request->min;
        $data->deskripsi = $request->dec;
        $data->save();

        return response()->json(['message'=>'Data Berhasil di Update', 'response'=>$data], 200);
      } catch (Queryexception $e) {
        return response()->json(['message'=>'Data Gagal di Update', 'response'=>$e], 500);

      }

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy_luas($id)
    {
      $data = KisaranLuas::whereId($id)->first();
      $data->delete();
      return response()->json(['message'=>'Data Berhasil di Delete'], 200);
    }

    public function destroy_harga($id)
    {
      $data = KisaranHarga::whereId($id)->first();
      $data->delete();
      return response()->json(['message'=>'Data Berhasil di Delete'], 200);
    }
}
