<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Backend\Entities\RefMenu;
use Modules\Backend\Entities\RefKategoriAnggunan;
use Modules\Agunan\Entities\TrnAgunan;
use DB;

class MenumanagementController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Menu Management',
        'resres' => $request->session()->all()
      ];
      return view('backend::menumanagement.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function getdatamenu($parent = 0, $spacing = '', $category_tree_array = '')
    {
      if (!is_array($category_tree_array)){
        $category_tree_array = array();
      }
      $data = RefMenu::where('parrent', $parent)->orderBy('id', 'ASC')->orderBy('urutan', 'ASC')->get();
      if (count($data) > 0) {
        foreach ($data as $row) {
          $category_tree_array[] = array("id" => $row->id,"parrent" => $row->parrent, "nama_menu" => $spacing . $row->nama_menu);
          $category_tree_array = $this->getdatamenu($row->id, '&nbsp;&nbsp;&nbsp;&nbsp;'.$spacing . '-&nbsp;', $category_tree_array);
        }
      }
      return $category_tree_array;
    }

    public function getbyparrent($id)
    {
      return RefMenu::where('parrent',$id)->get();
    }

    public function ajaxdatatable()
    {
      $data = $this->getdatamenu();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#showupdate" onclick="editdata('.$data['id'].')">Edit</button>
                     <a href="'.url('backend/menumanagement/remove').'/'.$data['id'].'" class="btn btn-danger waves-effect waves-light" onclick="return confirm(\'Are you sure want to delete? \')">Delete</a>';
          return $button;
        })
        ->editColumn('nama_menu', function ($data) {
          if ($data['parrent'] != 0) {
            return str_replace('&nbsp;', ' ', $data['nama_menu']);
          } else {
            return '<i><b>'.$data['nama_menu'].'</b></i>';
          }
        })
        ->rawColumns(['nama_menu', 'action'])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      try {
        $data = new RefMenu;
        $data->parrent      = $request->nm_parrent;
        $data->nama_menu    = $request->nm_menu;
        $data->link         = $request->lnk_mnu;
        $data->urutan       = $request->urutan;
        $data->class_active = $request->activen;
        $data->save();

        return redirect()->route('menus')->with('success', 'Data Berhasil di Tambahkan');
      } catch (Queryexception $e) {
        return redirect()->route('menus')->with('error', 'Terjadi kesalahan, Data Gagal di tambahkan, '.$e);
      }

    }

    public function getdata($id)
    {
      return RefMenu::where('parrent',$id)->orderBy('id', 'ASC')->get();
    }

    public function countdata($id)
    {
      $data = $this->getdata($id);
      return count($data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show()
    {
      $data = $this->getdata(0);
      $mnv = '<ul class="navbar-nav mx-auto ">';
      foreach ($data as $key) {
        if ($this->countdata($key->id) > 0) {
          $mnv .= '<li class="nav-item dropdown">';
          $mnv .= '<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">'.$key->nama_menu.'</a>';
          $mnv .= $this->formatree($key->id);
          $mnv .= '</li>';
        } else {
          $mnv .= '<li class="nav-item">';
          $mnv .= '<a class="nav-link" href="'.$key->link.'">'.$key->nama_menu.'</a>';
          $mnv .= '</li>';
        }
      }
      $mnv .= '</ul>';
      $mnv .= '<ul class="navbar-nav"><li><a href="login.html" class="btn btn-primary text-capitalize"><i class="fa fa-lock mr-1"></i> MASUK</a></li></ul>';
      return response()->json(['data' => $mnv, 'status' => 200]);
    }

    public function formatree($id)
    {
      $data = $this->getdata($id);
      $isi = '<ul class="dropdown-menu animate fade-up">';
      foreach ($data as $key) {
        if ($this->countdata($key->id) > 0) {
          $isi .= '<li class="nav-item dropdown">';
          $isi .= '<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">'.$key->nama_menu.'</a>';
          $isi .= $this->formatree($key->id);
          $isi .= '</li>';
        } else {
          if ($id == 2) { // this hardcode for count conditional
            $isi .= '<li><a class="dropdown-item" href="/'.$key->link.'">'.$key->nama_menu.'&nbsp;<span class="badge badge-secondary badge-pill">'.$this->calcisi($key->id).'</span></a></li>';
          } else {
            $isi .= '<li><a class="dropdown-item" href="/'.$key->link.'">'.$key->nama_menu.'</a></li>';
          }
        }
      }
      $isi .= '</ul>';
      return $isi;
    }

    public function calcisi($idd)
    {
      $let = RefKategoriAnggunan::when($idd, function ($let, $idd) {
        if ($idd != 9) {
          $let->where('id_menu', $idd);
        }
      })->first();
      if ($idd == 9) {
        $data = TrnAgunan::select(DB::raw('count(*) as jumlah'))->where('publikasi', 1)->first();
        return $data->jumlah;
      } else {
        $data = TrnAgunan::select(DB::raw('count(*) as jumlah'))->where('publikasi', 1)->where('id_jenis_agunan', $let->id)->first();
        return $data->jumlah;
      }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
      return RefMenu::whereId($id)->first();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
      try {
        $data = $this->edit($id);
        $data->nama_menu    = $request->nm_menu;
        $data->parrent      = $request->nm_parrent;
        $data->link         = $request->lnk_mnu;
        $data->urutan       = $request->urutan;
        $data->class_active = $request->activen;
        $data->save();

        if ($data) {
          return redirect()->route('menus')->with('success', 'Data Berhasil di Update');
        }
      } catch (Queryexception $e) {
        return redirect()->route('menus')->with('error', 'Terjadi kesalahan, Data Gagal di update, '.$e);
      }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $data = $this->edit($id);
        $data->delete();
        return redirect()->route('menus')->with('warning', 'Data Berhasil di Hapus');
    }
}
