<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Entities\RefKategoriAnggunan;
use Modules\Backend\Entities\RefMenu;

class KategoriAnggunanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Jenis Agunan',
        'resres' => $request->session()->all()
      ];
      return view('backend::kategorianggunan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('backend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      try {
        $data = new RefKategoriAnggunan;
        if ($request->store == '0') {
          $data->nama_kategori       = $request->nm_kate;
          $data->store_menu_frontend = $request->store;
          $data->id_menu             = null;
          $data->active              = $request->activen;
          $data->save();
        } else {
          $send = new RefMenu;
          $send->parrent      = $request->nm_parrent;
          $send->nama_menu    = $request->nm_kate;
          $send->link         = $request->linkm;
          $send->urutan       = $request->urutn;
          $send->class_active = '1';
          $send->save();

          $data->nama_kategori       = $send->nama_menu;
          $data->store_menu_frontend = $request->store;
          $data->id_menu             = $send->id;
          $data->active              = $request->activen;
          $data->save();
        }

        return redirect()->route('anggunankategori')->with('success', 'Data Berhasil di Tambahkan');
      } catch (Queryexception $e) {
        return redirect()->route('anggunankategori')->with('error', 'Terjadi kesalahan, Data Gagal di tambahkan, '.$e);
      }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show()
    {
        $data = RefKategoriAnggunan::All();
        return DataTables()
          ->of($data)
          ->addColumn('action', function ($data) {
            $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#showupdate" onclick="editdata('.$data->id.')">Edit</button>
                       <a href="'.url('backend/kategorianggunan/remove').'/'.$data->id.'" class="btn btn-danger waves-effect waves-light" onclick="return confirm(\'Are you sure want to delete? \')">Delete</a>';
            return $button;
          })
          ->editColumn('active', function ($data) {
            return $data->active == '1' ? 'Yes' : 'No';
          })
          ->editColumn('store_menu_frontend', function ($data) {
            return $data->store_menu_frontend == '1' ? 'Yes' : 'No';
          })
          ->rawColumns(['nama_kategori', 'store_menu_frontend', 'active', 'action'])
          ->addIndexColumn()
          ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
      return RefKategoriAnggunan::select(
          'ref_kategori_anggunan.*',
          'ref_menu_frontend.parrent',
          'ref_menu_frontend.link',
          'ref_menu_frontend.urutan')
        ->leftjoin('ref_menu_frontend','ref_kategori_anggunan.id_menu','=','ref_menu_frontend.id')
        ->where('ref_kategori_anggunan.id',$id)
        ->first();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
      try {
        $data = RefKategoriAnggunan::whereId($id)->first();
        if ($request->store == '0') {
          $data->nama_kategori       = $request->nm_kate;
          $data->store_menu_frontend = $request->store;
          $data->id_menu             = null;
          $data->active              = $request->activen;
          $data->save();
        } else {
          $send = RefMenu::whereId($data->id_menu)->first();
          $send->parrent      = $request->nm_parrent;
          $send->nama_menu    = $request->nm_kate;
          $send->link         = $request->linkm;
          $send->urutan       = $request->urutn;
          $send->class_active = '1';
          $send->save();

          $data->nama_kategori       = $send->nama_menu;
          $data->store_menu_frontend = $request->store;
          $data->id_menu             = $send->id;
          $data->active              = $request->activen;
          $data->save();
        }

        return redirect()->route('anggunankategori')->with('success', 'Data Berhasil di Update');
      } catch (Queryexception $e) {
        return redirect()->route('anggunankategori')->with('error', 'Terjadi kesalahan, Data Gagal di update, '.$e);
      }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
      $first = RefKategoriAnggunan::whereId($id)->first();
      $second = RefMenu::whereId($first->id_menu)->first();
      if ($second == null) {
        $first->delete();
      } else {
        $first->delete();
        $second->delete();
      }
      return redirect()->route('anggunankategori')->with('warning', 'Data Berhasil di Hapus');
    }
}
