<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Entities\RefDuration;
use Modules\Backend\Entities\RefBunga;

class ParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Parameter',
        'resres' => $request->session()->all()
      ];
      return view('backend::parameter.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('backend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    public function store_duration(Request $request)
    {
      $data = RefDuration::whereId(1)->first();
      $data->duration = $request->duration;
      $data->save();
      return response()->json(['message' => 'done'], 200);
    }

    public function store_bunga(Request $request)
    {
      try {
        $data = new RefBunga;
        $data->kategori_id = $request->katid;
        $data->bunga       = $request->bunga;
        $data->deskripsi   = $request->durat;
        $data->save();

        return response()->json(['message' => 'done'], 200);
      } catch (Queryexception $e) {
        return response()->json(['message' => 'failed with message'.$e], 500);
      }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('backend::show');
    }

    public function show_bunga()
    {
      $data = RefBunga::join('ref_kategori_anggunan','ref_bunga.kategori_id','=','ref_kategori_anggunan.id')->get();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#showupdate" onclick="editdataluas('.$data->id.')">Edit</button>
                     <a href="javascript:void(0);" class="btn btn-danger waves-effect waves-light" onclick="removeluas('.$data->id.')">Delete</a>';
          return $button;
        })
        ->editColumn('bunga', function ($data) {
          return $data->bunga." %";
        })
        ->rawColumns(['nama_kategori', 'bunga', 'deskripsi', 'action'])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('backend::edit');
    }

    public function edit_bunga($id)
    {
      return RefBunga::join('ref_kategori_anggunan','ref_kategori_anggunan.id','=','ref_bunga.kategori_id')->where('ref_bunga.id', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function update_bunga(Request $request, $id)
    {
      try {
        $data = RefBunga::whereId($id)->first();
        $data->kategori_id = $request->katid;
        $data->bunga       = $request->bunga;
        $data->deskripsi   = $request->durat;
        $data->save();

        return response()->json(['message' => 'done'], 200);
      } catch (Queryexception $e) {
        return response()->json(['message' => 'failed with message'.$e], 500);
      }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function destroy_bunga($id)
    {
      $data = RefBunga::whereId($id)->first();
      $data->delete();
      return response()->json(['message'=>'Data Berhasil di Delete'], 200);
    }
}
