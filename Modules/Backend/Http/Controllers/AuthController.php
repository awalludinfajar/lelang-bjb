<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('backend::auth.login');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function register()
    {
        return view('backend::auth.register');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
          'username'  => 'required|min:4|max:38',
          'email'     => 'required|email',
          'password'  => 'required|confirmed',
          'role'      => 'required'
        ];

        $message = [
          'username.required' => 'wajib di isi',
          'username.min' => 'username minimal 4 karakter',
          'username.max' => 'username maksimal 35 karakter',
          'email.required' => 'Email wajib diisi',
          'email.email' => 'Email tidak valid',
          // 'email.unique' => 'Email sudah terdaftar',
          'password.required' => 'Password wajib diisi',
          'password.confirmed' => 'Password tidak sama dengan konfirmasi password',
          'role.required' => 'role wajib di isi'
        ];

        $validator = Validator::make($request->All(), $rules, $message);

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = new User;
        $data->name               = $request->nama;
        $data->username           = ucwords(strtolower($request->username));
        $data->email              = strtolower($request->email);
        $data->email_verified_at  = \Carbon\Carbon::now();
        $data->password           = Hash::make($request->password);
        $data->role               = $request->role;
        $data->created_at         = \Carbon\Carbon::now();
        $save = $data->save();

        if ($save) {
          Session::flash('success', 'Register berhasil! Silahkan login untuk mengakses data');
          return redirect()->route('login');
        } else {
          Session::flash('errors', ['' => 'Register gagal! Silahkan ulangi beberapa saat lagi']);
          return redirect()->route('signin');
        }

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Request $request)
    {
        $rules = [
          'username' => 'required',
          'password' => 'required|string'
        ];

        $message = [
          'username.required' => 'Nama wajib diisi',
          'password.required' => 'Password wajib diisi',
          'password.string' => 'Password harus berupa string'
        ];

        $validator = Validator::make($request->all(), $rules, $message);

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = [
          'username' => $request->username,
          'password' => $request->password
        ];

        Auth::attempt($data);

        if (Auth::check()) {
          return redirect()->route('home');
        } else {
          Session::flash('error', 'Email atau password salah');
          return redirect()->route('login');
        }
        // return view('backend::show');
    }

    public function showapi(Request $request)
    {
      $rules = [
        'username' => 'required',
        'password' => 'required|string',
        'captcha' => 'required|captcha'
      ];

      $message = [
        'username.required' => 'Nama wajib diisi',
        'password.required' => 'Password wajib diisi',
        'password.string' => 'Password harus berupa string'
      ];

      $validator = Validator::make($request->all(), $rules, $message);

      if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->all);
      }

      $response = Http::asForm()->post('http://simonpro.skdigital.id/Uim_lelang', [
        'username' => $request->username,
        'password' => $request->password,
        'appid'    => 201
      ]);

      if ($response['status'] == 'success') {
        $save = new User;
        $save->name     = $response['response']['nama'];
        $save->username = $response['response']['userid'];
        $save->jabatan  = $response['response']['jabatan'];
        $save->email    = $response['response']['email'];
        $save->role     = 1;
        $save->password = Hash::make($request->password);

        $cek = User::where('username',$response['response']['userid'])->where('email', $response['response']['email'])->first();
        if ($cek == null) {
          $save->save();
        }

        Auth::attempt([ 'username' => $request->username, 'password' => $request->password ]);
        if (Auth::check()) {
          // $user = Auth::user();
          // $success['token'] =  $user->createToken('nApp')->accessToken;
          Session::push('resdata', collect($response['response']));
          return redirect()->route('home');
        } else {
          Session::flash('errors', 'Username atau password salah');
          return redirect()->route('login');
        }
      } else {
        Session::flash('errors', 'Username atau Password Tidak Terdaftar');
        return redirect()->route('login');
      }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit()
    {
      return response()->json(['captcha'=> captcha_img()]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy()
    {
      Session::flush();
      Auth::logout(); // menghapus session yang aktif
      return redirect()->route('login');
    }
}
