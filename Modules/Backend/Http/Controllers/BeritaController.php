<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Backend\Entities\RefBlogs;
use File;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Berita Management',
        'resres' => $request->session()->all()
      ];
      return view('backend::berita.index', $data);
    }

    public function add(Request $request)
    {
      $data = [
        'module' => 'Tambah Berita',
        'resres' => $request->session()->all()
      ];
      return view('backend::berita.inputberita', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
      $data = RefBlogs::All();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<a href="'.url('backend/beritamanagement/get').'/'.$data->id.'" class="btn btn-success waves-effect waves-light">Edit</a>
                    <a href="'.url('backend/beritamanagement/remove').'/'.$data->id.'" class="btn btn-danger waves-effect waves-light" onclick="return confirm(\'Are you sure want to delete? \')">Delete</a>';
          return $button;
        })->editColumn('active', function ($data) {
          if ($data->active == 1) {
            return "Yes";
          } else {
            return "No";
          }
        })->editColumn('judul', function($data) {
          return '<ins><a href="javascript:void(0)" onclick="viewimage('.$data->id.')" data-toggle="modal" data-target="#viewimg" data-placement="top" title="Lihat Gambar">'.$data->judul.'</a></ins>';
        })->editColumn('description', function ($data) {
          $set = '<ins><a href="javascript:void(0)" onclick="viewdesc('.$data->id.')" data-toggle="modal" data-target="#viewdesc" data-placement="top" title="Lihat Gambar">'.substr($data->description,0,50).'...</a></ins>';
          return $set;
        })->rawColumns(['judul', 'description', 'jenis_berita', 'active', 'action'])
        ->addIndexColumn()
        ->make(true);
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      $data = new RefBlogs;

      $file = $request->file('url_image');
      if($file != null && $file != "") {
        $re_imange = $request->judul.".".$file->extension();
        $rule = public_path('upload/blogs/img/'.$re_imange);
        if(File::exists($rule)){
          File::delete($rule);
        }
        $path = public_path('upload/blogs/img/');
        $file->move($path, $re_imange);
        $data->author       = Auth::user()->name ?? '';
        $data->judul        = $request->judul;
        $data->image        = $re_imange;
        $data->jenis_berita = $request->jenis_berita;
        $data->description = $request->description;
        $data->active       = $request->active;
        $data->tampil_beranda       = $request->tampil_beranda;
        $data->created_at   = date('Y-m-d H:i:s');
        $data->save();

        return redirect()->route('berita')->with('success', 'Data Berhasil di Tambahkan');
      } else {
        return redirect()->route('berita')->with('error', 'Data Gagal di Tambahkan');
      }
    }

    public function show($id)
    {
      $data = RefBlogs::whereId($id)->first();
      return view('backend::berita.editberita',['data'=>$data]);
    }

    /**
     * Show the data for frontend resource.
     * @param int $id
     * @return Renderable
     */
    public function showfrontend($jdl,$id)
    {
      $data = RefBlogs::where('active','1')
        ->when($jdl, function ($data, $jdl) {
          if ($jdl != 'x') {
            $data->where('ref_blogs.judul', 'like', '%'.$jdl.'%');
          }
        })->when($id, function ($data, $id) {
          if ($id != 0) {
            $data->where('ref_blogs.id', $id);
          }
        })->get();
      if ($jdl == 'x') {
        return response()->json($data);
      } else {
        return view('blogs.blogs-read', ['data' => $data]);
      }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('url_image');
        $re_imange = "";
        $data = RefBlogs::whereId($id)->first();
        if($file != null && $file != "") {
          $re_imange = $data->image;
          $rule = public_path('upload/blogs/img/'.$re_imange);
          if(File::exists($rule)){
            File::delete($rule);
          }
          $path = public_path('upload/blogs/img/');
          $file->move($path, $re_imange);
        }
        $data->author         = Auth::user()->name ?? '';
        $data->judul          = $request->judul;
        $data->jenis_berita   = $request->jenis_berita;
        $data->description    = $request->description;
        $data->active         = $request->active;
        $data->tampil_beranda = $request->tampil_beranda;
        if ($re_imange != "") {
          $data->image      = $re_imange;
        }
        $data->save();

        return redirect()->route('berita')->with('success', 'Data Berhasil di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $data = RefBlogs::whereId($id)->first();
        $re_imange = $data->image;
        $rule = public_path('upload/blogs/img/'.$re_imange);
        if(File::exists($rule)){
          File::delete($rule);
        }
        $data->delete();
        return redirect()->route('berita')->with('warning', 'Data Berhasil di Hapus');
    }
}
