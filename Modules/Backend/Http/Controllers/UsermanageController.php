<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Entities\Userrole;

class UsermanageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
      $data = [
        'module' => 'User Management'
      ];
      return view('backend::usermanagement.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('backend::create');
    }

    public function show_category($value='')
    {
      return Userrole::where('active', '1')->get();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      $data = $request->except("_token","id","url_image");
      $data['created_at'] = date('Y-m-d H:i:s');
      $res = array();
      try {
        $category = Userrole::insert($data);
        $res['status'] = 200;
        $res['message'] = '';
      } catch (QueryException $e) {
        $res['status'] = 500;
        $res['message']= $e;
      }
      return response()->json($res);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show()
    {
        $data = Userrole::All();
        return DataTables()
          ->of($data)
          ->addColumn('action', function ($data) {
            $button = '<button class="btn btn-success waves-effect waves-light" onclick="editdata('.$data->id.')">Edit</button>
                       <button class="btn btn-danger waves-effect waves-light" onclick="removeordelete('.$data->id.')">Delete</button>';
            return $button;
          })
          ->editColumn('active', function ($data) {
            if ($data->active == 1) {
              return "Yes";
            } else {
              return "No";
            }
          })
          ->rawColumns(['jenis_user', 'active', 'deskripsi', 'action'])
          ->addIndexColumn()
          ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit_category($id)
    {
      return Userrole::whereId($id)->first();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update_category(Request $request, $id)
    {
        $data = Userrole::whereId($id)->first();
        $data->jenis_user = $request->jenis_user;
        $data->deskripsi  = $request->deskripsi;
        $data->active     = $request->active;
        $data->save();

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy_category($id)
    {
      $data = Userrole::whereId($id)->first();
      $data->delete();
      if ($data) {
        return $data;
      }
    }
}
