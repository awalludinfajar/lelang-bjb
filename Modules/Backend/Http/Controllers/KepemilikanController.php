<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Entities\RefKepemilikan;

class KepemilikanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Jenis Kepemilikan',
        'resres' => $request->session()->all()
      ];
      return view('backend::kepemilikan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('backend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      try {
        $data = new RefKepemilikan;
        $data->jenis_kepemilikan = $request->nm_kep;
        $data->deskripsi         = $request->deskr;
        $data->save();

        return redirect()->route('kepemilik')->with('success', 'Data Berhasil di Tambahkan');
      } catch (Queryexception $e) {
        return redirect()->route('kepemilik')->with('error', 'Terjadi kesalahan, Data Gagal di tambahkan, '.$e);
      }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show()
    {
      $data = RefKepemilikan::All();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#showupdate" onclick="editdata('.$data->id.')">Edit</button>
                     <a href="'.url('backend/jeniskepemilikan/destroy').'/'.$data->id.'" class="btn btn-danger waves-effect waves-light" onclick="return confirm(\'Are you sure want to delete? \')">Delete</a>';
          return $button;
        })
        ->rawColumns(['jenis_kepemilikan', 'deskripsi', 'action'])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
      return RefKepemilikan::whereId($id)->first();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
      try {
        $data = RefKepemilikan::whereId($id)->first();
        $data->jenis_kepemilikan = $request->nm_kep;
        $data->deskripsi         = $request->deskr;
        $data->save();

        return redirect()->route('kepemilik')->with('success', 'Data Berhasil di Update');
      } catch (Queryexception $e) {
        return redirect()->route('kepemilik')->with('error', 'Terjadi kesalahan, Data Gagal di Update, '.$e);
      }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
      $first = RefKepemilikan::whereId($id)->first();
      $first->delete();
      return redirect()->route('kepemilik')->with('warning', 'Data Berhasil di Hapus');
    }
}
