<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Backend\Entities\Banner;
use File;

class BannermanagementController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Banenr Management',
        'resres' => $request->session()->all()
      ];
      return view('backend::bannermanagement.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
      $data = Banner::All();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $button = '<button class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-md" onclick="editdata('.$data->id.')">Edit</button>
                     <a href="'.url('backend/bannermanagement/remove').'/'.$data->id.'" class="btn btn-danger waves-effect waves-light" onclick="return confirm(\'Are you sure want to delete? \')">Delete</a>';
          return $button;
        })
        ->editColumn('active', function ($data) {
          if ($data->active == 1) {
            return "Yes";
          } else {
            return "No";
          }
        })
        ->editColumn('nama_banner', function($data) {
          return '<ins><a href="javascript:void(0)" onclick="viewimage('.$data->id.')" data-toggle="modal" data-target="#viewimg" data-placement="top" title="Lihat Gambar">'.$data->nama_banner.'</a></ins>';
        })
        ->rawColumns(['nama_banner', 'deskripsi', 'slide', 'active', 'action'])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
      $data = new Banner;

      $file = $request->file('url_image');
      if($file != null && $file != "") {
        $re_imange = $request->nm_banner.".".$file->extension();
        $rule = public_path('upload/banner/img/'.$re_imange);
        if(File::exists($rule)){
          File::delete($rule);
        }
        $path = public_path('upload/banner/img/');
        $file->move($path, $re_imange);

        $data->nama_banner  = $request->nm_banner;
        $data->deskripsi    = $request->desc;
        $data->image        = $re_imange;
        $data->slide        = $request->slide;
        $data->active       = $request->activen;
        $data->created_at   = date('Y-m-d H:i:s');
        $data->save();

        return redirect()->route('banner')->with('success', 'Data Berhasil di Tambahkan');
      } else {
        return redirect()->route('banner')->with('error', 'Data Gagal di Tambahkan');
      }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return Banner::whereId($id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('backend::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('url_image');
        $re_imange = "";
        $data = Banner::whereId($id)->first();
        if($file != null && $file != "") {
          $re_imange = $data->image;
          $rule = public_path('upload/banner/img/'.$re_imange);
          if(File::exists($rule)){
            File::delete($rule);
          }
          $path = public_path('upload/banner/img/');
          $file->move($path, $re_imange);
        }

        $data->nama_banner  = $request->nm_banner;
        $data->deskripsi    = $request->desc;
        if ($re_imange != "") {
          $data->image      = $re_imange;
        }
        $data->slide        = $request->slide;
        $data->active       = $request->activen;
        $data->created_at   = date('Y-m-d H:i:s');
        $data->save();

        return redirect()->route('banner')->with('success', 'Data Berhasil di Tambahkan');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $data = Banner::whereId($id)->first();
        $re_imange = $data->image;
        $rule = public_path('upload/banner/img/'.$re_imange);
        if(File::exists($rule)){
          File::delete($rule);
        }
        $data->delete();
        return redirect()->route('banner')->with('warning', 'Data Berhasil di Hapus');
    }
}
