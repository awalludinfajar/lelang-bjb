<?php

use Illuminate\Http\Request;
use Modules\Backend\Entities\Banner;
use Modules\Backend\Entities\RefMenu;
use Modules\Backend\Http\Controllers\MenumanagementController;
use Modules\Backend\Http\Controllers\BeritaController;
use Modules\Backend\Entities\RefDuration;
use Modules\Backend\Entities\RefKategoriAnggunan;
use Modules\Backend\Entities\KisaranHarga;
use Modules\Backend\Entities\RefKepemilikan;
use Modules\Backend\Entities\RefProvinsi;
use Modules\Backend\Entities\RefKabkota;
use Modules\Backend\Entities\RefKecamatan;
use Modules\Backend\Entities\RefKelurahan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/backend', function (Request $request) {
    return $request->user();
});

// setup menu navigation
Route::get('api_data_menufr', [MenumanagementController::class, 'show']);

// get all banner dashboad
Route::get('api_data_banner', function() {
  return Banner::where('active', '1')->get();
});

// duration banner
Route::get('api_duration_banner', function() {
  return RefDuration::whereId(1)->first();
});

// get jenis kepemilikan / jenis agunan
Route::get('api_agunan_kategori', function() {
  return RefKepemilikan::All();
});

Route::get('api_kategori_agunan', function() {
  return RefKategoriAnggunan::All();
});

// get kisaran harga
Route::get('api_kisaran_harga', function() {
  return KisaranHarga::All();
});

// getprovinsi
Route::get('api_data_provinsi', function() {
  return RefProvinsi::All();
});

// getkabkota by id_provinsi
Route::get('api_data_kabkot/{id}', function($id) {
  return RefKabkota::where('provinsi_id', $id)->get();
});

// get kecamatan by id_kabkot
Route::get('api_data_kecamatan/{id}', function($id) {
  return RefKecamatan::where('kabkot_id', $id)->get();
});

// get kelurahan by id_kecamatan
Route::get('api_data_kelurahan/{id}', function($id) {
  return RefKelurahan::where('kec_id', $id)->get();
});

// get list berita
Route::get('api_data_blogsdanberita/{judul}/{id}', [BeritaController::class, 'showfrontend']);
