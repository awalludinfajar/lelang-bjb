<?php

use Modules\Backend\Http\Controllers\AuthController;
use Modules\Backend\Http\Controllers\BackendController;
use Modules\Backend\Http\Controllers\UsermanageController;
use Modules\Backend\Http\Controllers\BannermanagementController;
use Modules\Backend\Http\Controllers\MenumanagementController;
use Modules\Backend\Http\Controllers\ParameterController;
use Modules\Backend\Http\Controllers\KategoriAnggunanController;
use Modules\Backend\Http\Controllers\KisaranController;
use Modules\Backend\Http\Controllers\KepemilikanController;
use Modules\Backend\Http\Controllers\BeritaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('backend')->group(function() {
  Route::get('/', [AuthController::class, 'index'])->name('login');
  Route::get('login',[AuthController::class, 'index'])->name('login');
  Route::get('signin',[AuthController::class, 'register'])->name('signin');
  Route::post('send_signin', [AuthController::class, 'store'])->name('saveregis');
  // Route::post('send_login', [AuthController::class, 'show'])->name('store_login');
  Route::post('send_login', [AuthController::class, 'showapi'])->name('store_login');
  Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [BackendController::class, 'index'])->name('home');
    Route::get('logout',[AuthController::class, 'destroy'])->name('logout');
  });

  Route::get('usermanagement', [UsermanageController::class, 'index'])->middleware('auth')->name('userm');

  // baner m
  Route::get('bannermanagement', [BannermanagementController::class, 'index'])->middleware('auth')->name('banner');
  Route::post('bannermanagement', [BannermanagementController::class, 'store'])->middleware('auth')->name('setbanner');
  Route::get('bannermanagement/show', [BannermanagementController::class, 'create'])->middleware('auth')->name('show_data');
  Route::get('bannermanagement/get/{id}', [BannermanagementController::class, 'show'])->middleware('auth');
  Route::post('bannermanagement/set/{id}', [BannermanagementController::class, 'update'])->middleware('auth');
  Route::get('bannermanagement/remove/{id}', [BannermanagementController::class, 'destroy'])->middleware('auth');

  // menus m
  Route::get('menumanagement', [MenumanagementController::class, 'index'])->middleware('auth')->name('menus');
  Route::post('menumanagement', [MenumanagementController::class, 'store'])->middleware('auth')->name('savemenu');
  Route::get('menumanagement/get', [MenumanagementController::class, 'getdatamenu'])->middleware('auth')->name('datamenu');
  Route::get('menumanagement/get/{id}', [MenumanagementController::class, 'getbyparrent'])->middleware('auth');
  Route::get('menumanagement/show', [MenumanagementController::class, 'ajaxdatatable'])->middleware('auth')->name('damenu');
  Route::get('menumanagement/update/{id}', [MenumanagementController::class, 'edit'])->middleware('auth');
  Route::post('menumanagement/update/{id}', [MenumanagementController::class, 'update'])->middleware('auth');
  Route::get('menumanagement/remove/{id}', [MenumanagementController::class, 'destroy'])->middleware('auth');

  Route::get('parameter', [ParameterController::class, 'index'])->middleware('auth')->name('paramm');
  // paramenter---duration
  Route::get('parameter/duration/store', [ParameterController::class, 'store_duration'])->middleware('auth');
  // parameter---bunga
  Route::post('parameter/bunga/store', [ParameterController::class, 'store_bunga'])->middleware('auth')->name('save_bunga');
  Route::get('parameter/bunga/show', [ParameterController::class, 'show_bunga'])->middleware('auth')->name('show_bunga');
  Route::get('parameter/bunga/set/{id}', [ParameterController::class, 'edit_bunga'])->middleware('auth');
  Route::post('parameter/bunga/update/{id}', [ParameterController::class, 'update_bunga'])->middleware('auth');
  Route::get('parameter/bunga/destroy/{id}', [ParameterController::class, 'destroy_bunga'])->middleware('auth');

  // kategori anggunan
  Route::get('kategorianggunan', [KategoriAnggunanController::class, 'index'])->middleware('auth')->name('anggunankategori');
  Route::post('kategorianggunan', [KategoriAnggunanController::class, 'store'])->middleware('auth')->name('storekategori');
  Route::get('kategorianggunan/show', [KategoriAnggunanController::class, 'show'])->middleware('auth')->name('showajax');
  Route::get('kategorianggunan/get/{id}', [KategoriAnggunanController::class, 'edit'])->middleware('auth');
  Route::post('kategorianggunan/update/{id}', [KategoriAnggunanController::class, 'update'])->middleware('auth');
  Route::get('kategorianggunan/remove/{id}', [KategoriAnggunanController::class, 'destroy'])->middleware('auth');

  // Kisaran
  Route::get('kisaran', [KisaranController::class, 'index'])->middleware('auth')->name('rata');
  Route::post('kisaran/luas/store', [KisaranController::class, 'store_luas'])->middleware('auth')->name('send_luas');
  Route::post('kisaran/harga/store', [KisaranController::class, 'store_harga'])->middleware('auth')->name('send_harga');
  Route::get('kisaran/luas/get', [KisaranController::class,'show_luas'])->middleware('auth')->name('luasajax');
  Route::get('kisaran/harga/get', [KisaranController::class,'show_harga'])->middleware('auth')->name('hargajax');
  Route::get('kisaran/luas/edit/{id}', [KisaranController::class,'edit_luas'])->middleware('auth');
  Route::get('kisaran/harga/edit/{id}', [KisaranController::class,'edit_harga'])->middleware('auth');
  Route::post('kisaran/luas/storeupdate/{id}', [KisaranController::class, 'update_luas'])->middleware('auth');
  Route::post('kisaran/harga/storeupdate/{id}', [KisaranController::class, 'update_harga'])->middleware('auth');
  Route::get('kisaran/luas/destroy/{id}', [KisaranController::class, 'destroy_luas'])->middleware('auth');
  Route::get('kisaran/harga/destroy/{id}', [KisaranController::class, 'destroy_harga'])->middleware('auth');

  // jenis Kepemilikan
  Route::get('jeniskepemilikan', [KepemilikanController::class, 'index'])->middleware('auth')->name('kepemilik');
  Route::post('jeniskepemilikan', [KepemilikanController::class, 'store'])->middleware('auth')->name('storekepemilikan');
  Route::get('jeniskepemilikan/show', [KepemilikanController::class, 'show'])->middleware('auth')->name('kepemilikanajax');
  Route::get('jeniskepemilikan/get/{id}', [KepemilikanController::class, 'edit'])->middleware('auth');
  Route::post('jeniskepemilikan/set/{id}', [KepemilikanController::class, 'update'])->middleware('auth');
  Route::get('jeniskepemilikan/destroy/{id}', [KepemilikanController::class, 'destroy'])->middleware('auth');

  // -- user category ajax route
  Route::get('usermanagement/category/data', [UsermanageController::class, 'show_category'])->middleware('auth')->name('datax');
  Route::get('usermanagement/category/save', [UsermanageController::class, 'store'])->middleware('auth')->name('userrole');
  Route::get('usermanagement/category/list', [UsermanageController::class, 'show'])->middleware('auth')->name('role');
  Route::get('usermanagement/category/delete/{id}', [UsermanageController::class, 'destroy_category'])->middleware('auth')->name('remove_role');
  Route::get('usermanagement/category/edit/{id}', [UsermanageController::class, 'edit_category'])->middleware('auth')->name('edit_category');
  Route::get('usermanagement/category/store_edit/{id}', [UsermanageController::class, 'update_category'])->middleware('auth')->name('store_edit');

  // berita m
  Route::get('beritamanagement', [BeritaController::class, 'index'])->middleware('auth')->name('berita');
  Route::get('beritamanagement/add', [BeritaController::class, 'add'])->middleware('auth')->name('addberita');
  Route::post('beritamanagement/tambah', [BeritaController::class, 'store'])->middleware('auth')->name('setberita');
  Route::get('beritamanagement/show', [BeritaController::class, 'create'])->middleware('auth')->name('show_berita');
  Route::get('beritamanagement/get/{id}', [BeritaController::class, 'show'])->middleware('auth');
  Route::post('beritamanagement/set/{id}', [BeritaController::class, 'update'])->middleware('auth');
  Route::get('beritamanagement/remove/{id}', [BeritaController::class, 'destroy'])->middleware('auth');
});
