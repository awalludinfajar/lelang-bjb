<?php

namespace Modules\Agunan\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Support\Renderable;

use Modules\Agunan\Entities\TrnAgunan;
use Modules\Agunan\Entities\TrnImgAgunan;
use Modules\Agunan\Entities\TrnImgDenah;
use Modules\Agunan\Entities\TrnVidAgunan;
use Modules\Agunan\Entities\TrnMakerMaps;
use Modules\Agunan\Entities\TrnJumlahHariLelangSegera;

use File;
use URL;
use DB;

class AgunanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
      $data = [
        'module' => 'Agunan',
        'resres' => $request->session()->all()
      ];
      return view('agunan::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(Request $request)
    {
      $data = [
        'module' => 'Agunan',
        'resres' => $request->session()->all(),
        'link' => URL::to('/')
      ];
      return view('agunan::input.insert', $data);
    }

    public function uploadImage(Request $request) {
      try {
        date_default_timezone_set('Asia/Bangkok');

        $image = $request->file('image');
        $time = date("YmdHis");
        $name_image = $time.$image->getClientOriginalName();

        $hasil = $image->move(public_path('assets/img/agunan'),$name_image);
        if($hasil){
          $response['status'] = 200;
          $response['message']= $image->getClientOriginalName();
          $response['fullName']= $name_image;
        }else{
          $response['status'] = 500;
        }

      } catch (QueryException $e) {
        $response['status'] = 500;
        $response['message']= $e;
      }
      return $response;
    }

    public function uploadDenah(Request $request) {
      try {
        date_default_timezone_set('Asia/Bangkok');

        $image = $request->file('image');
        $time = date("YmdHis");
        $name_image = $time.$image->getClientOriginalName();

        $hasil = $image->move(public_path('assets/img/denah'),$name_image);
        if($hasil){
          $response['status'] = 200;
          $response['message']= $image->getClientOriginalName();
          $response['fullName']= $name_image;
        }else{
          $response['status'] = 500;
        }

      } catch (QueryException $e) {
        $response['status'] = 500;
        $response['message']= $e;
      }
      return $response;
    }

    public function removeaset(Request $request) {
      try {
        $path = public_path('assets/img/'.$request->fil.'/'.$request->nme);
        if(File::exists($path)){
          File::delete($path);
        }
        $response['status'] = 200;
        $response['message']= "Berhasil Dihapus";
      } catch (Exception $e) {
        $response['status'] = 500;
        $response['message']= $e;
      }
      return $response;
    }

    // public function uploadVideo(Request $request) {
    //   try {
    //     date_default_timezone_set('Asia/Bangkok');
    //
    //     $image = $request->file('video');
    //     $time = date("YmdHis");
    //     $name_image = $time.$image->getClientOriginalName();
    //
    //     // $hasil = $image->move(public_path('assets/vid/agunan'),$name_image);
    //     if($hasil){
    //       $response['status'] = 200;
    //       $response['message']= $image->getClientOriginalName();
    //       $response['fullName']= $name_image;
    //     }else{
    //       $response['status'] = 500;
    //     }
    //
    //   } catch (QueryException $e) {
    //     $response['status'] = 500;
    //     $response['message']= $e;
    //   }
    //   return $response;
    // }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request) {

      $simpanAgunan = new TrnAgunan();
      $simpanAgunan->id_jenis_agunan = $request->jnskp;
      $simpanAgunan->judul_agunan = $request->jdlag;
      $simpanAgunan->alamat = $request->alamat;
      $simpanAgunan->rt = $request->rt;
      $simpanAgunan->rw = $request->rw;
      $simpanAgunan->id_kelurahan = $request->kelur;
      $simpanAgunan->id_kecamatan = $request->kecama;
      $simpanAgunan->id_kota = $request->ktaa;
      $simpanAgunan->id_provinsi = $request->provin;
      $simpanAgunan->id_kodepos = $request->kdps;
      $simpanAgunan->jumlah_kamar_tidur = $request->jmtidur;
      $simpanAgunan->jumlah_kamar_mandi = $request->jmandi;
      $simpanAgunan->jumlah_garasi_carport = $request->jmcarport;
      $simpanAgunan->jumlah_lantai = $request->jmlantai;
      $simpanAgunan->luas_tanah = $request->lstanah;
      $simpanAgunan->luas_bangunan = $request->lsbangun;
      $simpanAgunan->id_kepemilikan = $request->jnskep;
      $simpanAgunan->tanggal_kadaluarsa_kepemilikan = $request->tglkadaluarsa;
      $simpanAgunan->keterangan = $request->keter;
      $simpanAgunan->user_input = Auth::id();
      $simpanAgunan->status = $request->activen;
      $simpanAgunan->jenis_jual = $request->jnsjal;
      // $simpanAgunan->tanggal_lelang = Carbon::now();
      $simpanAgunan->user_publikasi =0;
      $simpanAgunan->publikasi = 0;
      $simpanAgunan->disabld = $request->activen;
      $simpanAgunan->price = $request->price;
      $simpanAgunan->hot_price = $request->hot;
      $simpanAgunan->kode_agunan = "TRN".mt_rand(000, 999);
      if( $simpanAgunan->save() ){
        $cnt = 1;
        foreach ($request->imgA as $key) {
          if(!empty($key)) {
            $imageA = new TrnImgAgunan();
            $imageA->agunan_id = $simpanAgunan->id;
            $imageA->image = $key;
            $imageA->active = 1;
            $imageA->posisi = $cnt;
            $imageA->save();
          }
          $cnt++;
        }

        foreach ($request->denahA as $key) {
          if(!empty($key)) {
            $denahA = new TrnImgDenah();
            $denahA->agunan_id = $simpanAgunan->id;
            $denahA->image = $key;
            $denahA->active = 1;
            $denahA->save();
          }
        }

        foreach ($request->video as $key) {
          if(!empty($key)){
            $denahA = new TrnVidAgunan();
            $denahA->agunan_id = $simpanAgunan->id;
            $denahA->linkyoutube = $key;
            $denahA->active = 1;
            $denahA->save();
          }
        }

        $mapS = new TrnMakerMaps();
        $mapS->agunan_id = $simpanAgunan->id;
        $mapS->logitude = $request->long;
        $mapS->latitude = $request->lat;
        $mapS->save();

        return response()->json(['status'=>true,'msg'=>'Data Telah Di Simpan'],200);
      }else{
        return response()->json(['status'=>false,'msg'=>'Terjadi Kesalahan'],400);
      }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show()
    {
      $data = TrnAgunan::join('ref_kategori_anggunan', 'trn_agunan.id_jenis_agunan','=','ref_kategori_anggunan.id')
                    ->join('mst_kepemilikan','trn_agunan.id_kepemilikan','=','mst_kepemilikan.id')
                    ->select('trn_agunan.id','trn_agunan.kode_agunan','ref_kategori_anggunan.nama_kategori','trn_agunan.jenis_jual','trn_agunan.alamat','mst_kepemilikan.jenis_kepemilikan', 'trn_agunan.publikasi', 'trn_agunan.judul_agunan')
                    ->orderby('trn_agunan.id','asc')
                    ->get();
      return DataTables()
        ->of($data)
        ->addColumn('action', function ($data) {
          $pub = $data->publikasi == 0 ? '<a href="javascript:void(0);" class="dropdown-item" data-toggle="modal" data-target="#publiss" onclick="publikasikan('.$data->id.')">Publikasi</a><div class="dropdown-divider"></div>' : '';
          $button = '<div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" id="downMenuButton'.$data->id.'" onclick="setupdropd(this.id)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</button>
                        <div class="dropdown-menu" id="mydropp'.$data->id.'" aria-labelledby="downMenuButton'.$data->id.'">
                          '.$pub.'
                          <a href="javascript:void(0);" class="dropdown-item" onclick="setagunanedit('.$data->id.')">Edit</a>
                          <a href="javascript:void(0);" class="dropdown-item" onclick="setagunandelete('.$data->id.')">Delete</a>
                        </div>
                      </div>';
          return $button;
        })
        ->editColumn('jenis_jual', function ($data) {
          return $data->jenis_jual == 1 ? 'Lelang' : 'Jual Sukarela';
        })->editColumn('kode_agunan', function ($data) {
          return $data->kode_agunan.' | '.$data->judul_agunan;
        })->addIndexColumn()
        ->rawColumns(['kode_agunan', 'nama_kategori', 'jenis_jual', 'alamat', 'jenis_kepemilikan', 'publikasi', 'action'])
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('agunan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
      $tagunan = TrnAgunan::whereId($id)->first();
      if ($tagunan->delete()) {
        $timdena = TrnImgDenah::where('agunan_id', $id)->first();
        $timdena->delete(); // remove denah image by id agunan

        $timagun = TrnImgAgunan::where('agunan_id', $id)->first();
        $timagun->delete(); // remove image agunan by id agunan

        $tviagun = TrnVidAgunan::where('agunan_id', $id)->first();
        $tviagun->delete(); // remove data link video agunan by id agunan

        $tmarmap = TrnMakerMaps::where('agunan_id', $id)->first();
        $tmarmap->delete(); // remove marker maps by id agunan

        $tjuhale = TrnJumlahHariLelangSegera::where('id_agunan', $id)->first();
        $tjuhale->delete(); // remove parameter jumlah hari lelang segera by id agunan
      }
      return response()->json(['status' => 'Agunan Berhasil Dihapus!']);
    }
}
