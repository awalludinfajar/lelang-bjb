<?php

namespace Modules\Agunan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Agunan\Entities\TrnAgunan;
use Modules\Agunan\Entities\TrnImgAgunan;
use Modules\Agunan\Entities\TrnImgDenah;
use Modules\Agunan\Entities\TrnVidAgunan;
use Modules\Agunan\Entities\TrnMakerMaps;
use Modules\Agunan\Entities\TrnJumlahHariLelangSegera;

use Modules\Backend\Entities\RefMenu;
use Modules\Backend\Entities\RefKategoriAnggunan;

use Illuminate\Support\Facades\Auth;

class DataAgunanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('agunan::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function showdetail($id)
    {
      $data = $this->store($id);
      $show = [
        'list' => $data,
        'img' => TrnImgAgunan::select('trn_img_agunan.image','trn_agunan.judul_agunan', 'ref_kategori_anggunan.nama_kategori')
          ->join('trn_agunan','trn_img_agunan.agunan_id','=','trn_agunan.id')
          ->join('ref_kategori_anggunan','ref_kategori_anggunan.id','=','trn_agunan.id_jenis_agunan')
          ->where('trn_img_agunan.agunan_id', $id)->get(),
        'dnh' => TrnImgDenah::where('agunan_id', $id)->get(),
        'vid' => TrnVidAgunan::where('agunan_id', $id)->get(),
        'mkr' => TrnMakerMaps::where('agunan_id', $id)->first()
      ];
      return view('detailagunan.detailagunan', $show);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store($id)
    {
      $data = TrnAgunan::select(
        'trn_agunan.id as id_ang',
        'trn_agunan.kode_agunan',
        'trn_agunan.judul_agunan',
        'trn_agunan.alamat',
        'trn_agunan.rt',
        'trn_agunan.rw',
        'trn_agunan.id_kodepos',
        'trn_agunan.jumlah_kamar_tidur',
        'trn_agunan.jumlah_kamar_mandi',
        'trn_agunan.jumlah_garasi_carport',
        'trn_agunan.jumlah_lantai',
        'trn_agunan.luas_bangunan',
        'trn_agunan.luas_tanah',
        'ref_kategori_anggunan.nama_kategori as agunan',
        'ref_kelurahan.nama as kelurahan',
        'ref_kecamatan.nama as kecamatan',
        'ref_kabkota.nama as kabkot',
        'ref_provinsi.nama as provinsi',
        'mst_kepemilikan.jenis_kepemilikan',
        'trn_agunan.tanggal_kadaluarsa_kepemilikan',
        'trn_agunan.keterangan',
        'trn_agunan.hot_price',
        'trn_agunan.jenis_jual',
        'trn_agunan.tanggal_lelang',
        'trn_agunan.price as harga')
        ->join('ref_kategori_anggunan','ref_kategori_anggunan.id','=','trn_agunan.id_jenis_agunan')
        ->join('ref_kelurahan','ref_kelurahan.id','=','trn_agunan.id_kelurahan')
        ->join('ref_kecamatan','ref_kecamatan.id','=','trn_agunan.id_kecamatan')
        ->join('ref_kabkota','ref_kabkota.id','=','trn_agunan.id_kota')
        ->join('ref_provinsi','ref_provinsi.id','=','trn_agunan.id_provinsi')
        ->join('mst_kepemilikan','mst_kepemilikan.id','=','trn_agunan.id_kepemilikan')
        ->when($id, function ($data, $id) {
          if ($id != 0) {
            $data->where('trn_agunan.id', $id);
          }
        })->where('trn_agunan.publikasi',1)->first();
      return $data;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Request $request)
    {
      $jenisk = $request->jnskp;
      $katego = $request->katgr;
      $provin = $request->provin;
      $kota   = $request->ktaa;
      $harga  = [$request->harmin, $request->harmax];
      $datepc = [$request->datepicker,$request->datepicker2];
      $ishot  = $request->checkbox3;
      $howsel = (int)$request->carajual;
      $period = $request->periodelelang;

      $lt = explode(';',$request->luas_tanah);      //luas tanah between
      $lb = explode(';',$request->luas_bangunan);   //luas bangunan between

      $data = TrnAgunan::select(
        'trn_agunan.id as id_ang',
        'trn_agunan.kode_agunan',
        'trn_agunan.judul_agunan',
        'trn_agunan.alamat',
        'trn_agunan.rt',
        'trn_agunan.rw',
        'trn_agunan.id_kodepos',
        'trn_agunan.jumlah_kamar_tidur',
        'trn_agunan.jumlah_kamar_mandi',
        'trn_agunan.jumlah_garasi_carport',
        'trn_agunan.jumlah_lantai',
        'trn_agunan.luas_bangunan',
        'trn_agunan.luas_tanah',
        'ref_kategori_anggunan.nama_kategori as agunan',
        'ref_kelurahan.nama as kelurahan',
        'ref_kecamatan.nama as kecamatan',
        'ref_kabkota.nama as kabkot',
        'ref_provinsi.nama as provinsi',
        'mst_kepemilikan.jenis_kepemilikan',
        'trn_agunan.tanggal_kadaluarsa_kepemilikan',
        'trn_agunan.keterangan',
        'trn_agunan.hot_price',
        'trn_agunan.jenis_jual',
        'trn_agunan.tanggal_lelang',
        'trn_agunan.price as harga',
        'trn_img_agunan.image')
        ->join('ref_kategori_anggunan','ref_kategori_anggunan.id','=','trn_agunan.id_jenis_agunan')
        ->join('ref_kelurahan','ref_kelurahan.id','=','trn_agunan.id_kelurahan')
        ->join('ref_kecamatan','ref_kecamatan.id','=','trn_agunan.id_kecamatan')
        ->join('ref_kabkota','ref_kabkota.id','=','trn_agunan.id_kota')
        ->join('ref_provinsi','ref_provinsi.id','=','trn_agunan.id_provinsi')
        ->join('mst_kepemilikan','mst_kepemilikan.id','=','trn_agunan.id_kepemilikan')
        ->join('trn_img_agunan', 'trn_img_agunan.agunan_id','=','trn_agunan.id')
        ->when($jenisk, function ($data, $jenisk) {
          if ($jenisk != null) {
            return $data->where('trn_agunan.id_kepemilikan',$jenisk);
          }
        })->when($katego, function ($data, $katego) {
          if ($katego != null) {
            return $data->where('trn_agunan.id_jenis_agunan','=', $katego);
          }
        })->when($provin, function ($data, $provin) {
          if ($provin != null) {
            return $data->where('trn_agunan.id_provinsi','=', $provin);
          }
        })->when($kota, function ($data, $kota) {
          if ($kota != null) {
            return $data->where('trn_agunan.id_kota','=', $kota);
          }
        })->when($harga, function ($data, $harga) {
          if ($harga[0] != null || $harga[1] != null) {
            return $data->whereBetween('trn_agunan.price', [$harga[0], $harga[1]]);
          }
        })->when($datepc, function ($data, $datepc) {
          if ($datepc[0] != null || $datepc[1] != null) {
            return $data->whereBetween('trn_agunan.tanggal_lelang', [$datepc[0], $datepc[1]]);
          }
        })->when($ishot, function ($data, $ishot) {
          if ($ishot != 'No') {
            return $data->where('trn_agunan.hot_price','=', $ishot);
          }
        })->when($howsel, function ($data, $howsel) {
          if (isset($howsel)) {
            return $data->where('trn_agunan.jenis_jual','=', (int)$howsel);
          }
        })->when($period, function ($data, $period) {
          if ($period != null) {
            return $data->WhereYear('trn_agunan.created_at', $period);
          }
        })->when($lt, function ($data, $lt) {
          return $data->whereBetween('trn_agunan.luas_tanah', [(int)$lt[0], (int)$lt[1]]);
        })->when($lb, function ($data, $lb) {
          return $data->whereBetween('trn_agunan.luas_bangunan', [(int)$lb[0], (int)$lb[1]]);
        })->where('trn_agunan.publikasi',1)->where('trn_img_agunan.posisi', 1)->get();
      return response()->json($data);
    }

    public function showlist($cek)
    {
      $data = TrnAgunan::select(
        'trn_agunan.id as id_ang',
        'trn_agunan.kode_agunan',
        'trn_agunan.judul_agunan',
        'trn_agunan.alamat',
        'trn_agunan.rt',
        'trn_agunan.rw',
        'trn_agunan.id_kodepos',
        'trn_agunan.jumlah_kamar_tidur',
        'trn_agunan.jumlah_kamar_mandi',
        'trn_agunan.jumlah_garasi_carport',
        'trn_agunan.jumlah_lantai',
        'trn_agunan.luas_bangunan',
        'trn_agunan.luas_tanah',
        'ref_kategori_anggunan.nama_kategori as agunan',
        'ref_kelurahan.nama as kelurahan',
        'ref_kecamatan.nama as kecamatan',
        'ref_kabkota.nama as kabkot',
        'ref_provinsi.nama as provinsi',
        'mst_kepemilikan.jenis_kepemilikan',
        'trn_agunan.tanggal_kadaluarsa_kepemilikan',
        'trn_agunan.keterangan',
        'trn_agunan.hot_price',
        'trn_agunan.jenis_jual',
        'trn_agunan.tanggal_lelang',
        'trn_agunan.price as harga',
        'trn_img_agunan.image')
        ->join('ref_kategori_anggunan','ref_kategori_anggunan.id','=','trn_agunan.id_jenis_agunan')
        ->join('ref_kelurahan','ref_kelurahan.id','=','trn_agunan.id_kelurahan')
        ->join('ref_kecamatan','ref_kecamatan.id','=','trn_agunan.id_kecamatan')
        ->join('ref_kabkota','ref_kabkota.id','=','trn_agunan.id_kota')
        ->join('ref_provinsi','ref_provinsi.id','=','trn_agunan.id_provinsi')
        ->join('mst_kepemilikan','mst_kepemilikan.id','=','trn_agunan.id_kepemilikan')
        ->join('trn_img_agunan', 'trn_img_agunan.agunan_id','=','trn_agunan.id')
        ->when($cek, function ($data, $cek) {
          if ($cek == 'jualsukarela') { // jual sukarela
            $data->where('trn_agunan.jenis_jual',0);
          } elseif ($cek == 'lelangsegera') { // lelang segera
            $data->where('trn_agunan.jenis_jual',1);
          } elseif ($cek == 'hotprice') { // hotprice
            $data->where('trn_agunan.hot_price','Yes');
          } elseif ($cek == 'terbaru') { // terbaru
            $data->orderByDesc('trn_agunan.created_at')->limit(5);
          }
        })
        ->where('trn_agunan.publikasi',1)
        ->where('trn_img_agunan.posisi', 1)->get();
      return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('agunan::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request)
    {
      $data = TrnAgunan::whereId($request->idpu)->first();
      $data->tanggal_lelang = $request->tgll;
      $data->publikasi      = 1;
      $data->user_publikasi = $request->idus;
      if ($data->save()) {
        $now = new TrnJumlahHariLelangSegera();
        $now->id_agunan           = $request->idpu;
        $now->tanggal_batas_akhir = $request->akhl;
        $now->save();
      }
      return response()->json(['status' => 'Agunan Published!']);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function listagunan($namnam)
    {
      $cekmnu = RefMenu::where('link', 'like','%'.$namnam.'%')->first();
      if ($namnam != 'semua') {
        $getcat = RefKategoriAnggunan::where('id_menu', $cekmnu->id)->first();
        $idd = $getcat->id;
        $data = TrnAgunan::select(
          'trn_agunan.id as id_ang',
          'trn_agunan.kode_agunan',
          'trn_agunan.judul_agunan',
          'trn_agunan.alamat',
          'trn_agunan.rt',
          'trn_agunan.rw',
          'trn_agunan.id_kodepos',
          'trn_agunan.jumlah_kamar_tidur',
          'trn_agunan.jumlah_kamar_mandi',
          'trn_agunan.jumlah_garasi_carport',
          'trn_agunan.jumlah_lantai',
          'trn_agunan.luas_bangunan',
          'trn_agunan.luas_tanah',
          'ref_kategori_anggunan.nama_kategori as agunan',
          'ref_kelurahan.nama as kelurahan',
          'ref_kecamatan.nama as kecamatan',
          'ref_kabkota.nama as kabkot',
          'ref_provinsi.nama as provinsi',
          'mst_kepemilikan.jenis_kepemilikan',
          'trn_agunan.tanggal_kadaluarsa_kepemilikan',
          'trn_agunan.keterangan',
          'trn_agunan.hot_price',
          'trn_agunan.jenis_jual',
          'trn_agunan.tanggal_lelang',
          'trn_img_agunan.image')
          ->join('ref_kategori_anggunan','ref_kategori_anggunan.id','=','trn_agunan.id_jenis_agunan')
          ->join('ref_kelurahan','ref_kelurahan.id','=','trn_agunan.id_kelurahan')
          ->join('ref_kecamatan','ref_kecamatan.id','=','trn_agunan.id_kecamatan')
          ->join('ref_kabkota','ref_kabkota.id','=','trn_agunan.id_kota')
          ->join('ref_provinsi','ref_provinsi.id','=','trn_agunan.id_provinsi')
          ->join('mst_kepemilikan','mst_kepemilikan.id','=','trn_agunan.id_kepemilikan')
          ->join('trn_img_agunan', 'trn_img_agunan.agunan_id','=','trn_agunan.id')
          ->when($idd, function ($data, $idd) {
            if ($idd != 0) {
              $data->where('trn_agunan.id_jenis_agunan', $idd);
            }
          })
          ->where('trn_agunan.publikasi',1)
          ->where('trn_img_agunan.posisi', 1)->get();
          $show = [
            'data' => $data,
            'bagian' => $cekmnu->nama_menu,
            'idbagi' => $idd
          ];
          return view('daftar.list', $show);
      }
    }
}
