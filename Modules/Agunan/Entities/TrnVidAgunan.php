<?php

namespace Modules\Agunan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TrnVidAgunan extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'trn_vid_agunan';

    protected static function newFactory()
    {
        return \Modules\Agunan\Database\factories\TrnVidAgunanFactory::new();
    }
}
