<?php

namespace Modules\Agunan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TrnAgunan extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'trn_agunan';

    protected static function newFactory()
    {
        return \Modules\Agunan\Database\factories\TrnAgunanFactory::new();
    }
}
