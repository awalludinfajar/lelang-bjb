<?php

namespace Modules\Agunan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TrnJumlahHariLelangSegera extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'trn_jumlah_hari_lelang_segera';

    protected static function newFactory()
    {
        return \Modules\Agunan\Database\factories\TrnJumlahHariLelangSegeraFactory::new();
    }
}
