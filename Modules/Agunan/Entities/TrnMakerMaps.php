<?php

namespace Modules\Agunan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TrnMakerMaps extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'trn_marker_maps';

    protected static function newFactory()
    {
        return \Modules\Agunan\Database\factories\TrnMakerMapsFactory::new();
    }
}
