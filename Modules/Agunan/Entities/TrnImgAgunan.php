<?php

namespace Modules\Agunan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TrnImgAgunan extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table   = 'trn_img_agunan';

    protected static function newFactory()
    {
        return \Modules\Agunan\Database\factories\TrnImgAgunanFactory::new();
    }
}
