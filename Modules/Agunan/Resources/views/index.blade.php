@extends('backend::layouts.master')

@section('custom_css')
<style type="text/css">
  .turnturn {
    max-width:150px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
</style>
@endsection

@section('content')
<div class="tab-content">
  <div class="tab-pane row fade in active show" id="luas"  name="backend" role="tabpanel">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="float-right" style="margin-bottom:15px;">
              <a href="{{ route('input_agunan') }}" class="btn btn-primary" style="margin-top: -15px;">Input Agunan</a>
            </div>
            <table id="tableAgunan" class="table table-hover table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode / Judul</th>
                  <th>Jenis Agunan</th>
                  <th>Jenis Jual</th>
                  <th>Alamat</th>
                  <th>Jenis Kepemilikan</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('agunan::input.modalpublikasi')

@endsection

@section('custom_js')
<script type="text/javascript">
  $("#tgllelang").datepicker();
  $("#akhlelang").datepicker();

  var tableluas = $('#tableAgunan').DataTable({
    processing: true,
    'language': {
      'loadingRecords': '&nbsp;',
      'processing': 'Loading...'
    },
    serverSide: true,
    ordering: true,
    ajax: {
      url: `{{ route('loaddataAgunan') }}`,
      type: 'GET',
    },
    columnDefs:[{targets:4,className:"turnturn"}],
    createdRow: function( row, data, dataIndex) {
      if( data['publikasi'] == 1) {
        $(row).attr('style','background: #a4ffa7e6;');
      }
    },
    columns: [
      {data: 'DT_RowIndex', name: 'DT_RowIndex', class: 'text-center'},
      {data: 'kode_agunan', name: 'kode_agunan'},
      {data: 'nama_kategori',name: 'nama_kategori'},
      {data: 'jenis_jual',name: 'jenis_jual'},
      {data: 'alamat',name: 'alamat'},
      {data: 'jenis_kepemilikan',name: 'jenis_kepemilikan'},
      {data: 'action',name: 'action',searchable: false, orderable: false, className: 'text-center'}
    ],
  });

  function setupdropd(iddd) {
    var classList = $('[aria-labelledby='+iddd+']').attr('id');
    document.getElementById(classList).classList.toggle("show");
  }

  window.onclick = function(event) {
    if (!event.target.matches('.dropdown-toggle')) {
      var dropdowns = document.getElementsByClassName("dropdown-menu");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }

  function publikasikan(idpub) {
    $('#idpub').val(idpub);
  }

  $('#stored').on('click', function () {
    Swal.fire({
      title: 'Yakin Akan Publikasikan Agunan ?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Publikasi`,
      denyButtonText: `Batalkan publikasi`,
    }).then((result) => {
      if (result.isConfirmed) {
        var send = {
          idus : {{ Auth::user()->id }},
          idpu : $('#idpub').val(),
          tgll : $('#tgllelang').val(),
          akhl : $('#akhlelang').val()
        };
        senpublikasi(send);
      } else if (result.isDenied) {
        Swal.fire({
          title: 'Anda Membatalkan Publikasi!',
          icon: 'info',
          timer: 3000
        });
      }
    });
  });

  function senpublikasi(send) {
    $.ajax({
      type:"POST",
      url : "{{ url('api/agunan/publikasikann') }}",
      contentType:"application/json",
      dataType: "json",
      data: JSON.stringify(send),
      beforeSend: function(){
        Swal.fire({
          title: 'Wait ...',
          onBeforeOpen () { Swal.showLoading () },
          onAfterClose () { Swal.hideLoading() },
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false
        })
      },
      success:function(data) {
        Swal.close();
        tableluas.ajax.reload();

        $('#publiss').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

        Swal.fire({
          title: data['status'],
          icon: 'success',
          timer: 2000
        });

        $('#idpub').val('');
        $('#tgllelang').val('');
        $('#akhlelang').val('');
      }
    });
  }

  function setagunanedit(idd) {
    console.log(idd);
  }

  function setagunandelete(idd) {
    Swal.fire({
      title: 'Yakin Akan Hapus Agunan ?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Hapus Agunan`,
      denyButtonText: `Batalkan`,
    }).then((result) => {
      if (result.isConfirmed) {
        agunanhapuskan(idd);
      } else if (result.isDenied) {
        Swal.fire({
          title: 'Anda Membatalkan Hapus Data!',
          icon: 'info',
          timer: 3000
        });
      }
    });
  }

  function agunanhapuskan(idd) {
    $.ajax({
      type:"GET",
      url : "{{route('removeagunan',"+idd+")}}",
      beforeSend: function(){
        Swal.fire({
          title: 'Wait ...',
          onBeforeOpen () { Swal.showLoading () },
          onAfterClose () { Swal.hideLoading() },
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false
        })
      },
      success:function(data) {
        Swal.close();
        tableluas.ajax.reload();
        Swal.fire({
          title: data['status'],
          icon: 'success',
          timer: 3000
        });
      }
    });
  }
</script>
@endsection
