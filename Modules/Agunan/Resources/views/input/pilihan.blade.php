<div class="card-body">
    <div class="form-group row">
        <label for="jenkep" class="col-sm-3 col-form-label">Price</label>
        <div class="col-sm-9">
            <input type="text" name="priceview" id="priceview" class="form-control" placeholder="price" value="">
            <input type="hidden" name="price" id="price">
        </div>
    </div>
    <div class="form-group row">
        <label for="jenkep" class="col-sm-3 col-form-label">Jenis Kepemilikan</label>
        <div class="col-sm-9">
            <select class="form-control" id="jnskep" name="jnskep" style="width: 565px;"></select>
        </div>
        </div>
        <div class="form-group row">
        <label for="kadalkepemilik" class="col-sm-3 col-form-label">Tanggal Kadaluarsa Kepemilikan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="kadalkepemilik" name="tglkadaluarsa" placeholder="mm/dd/yyyy"/>
        </div>
    </div>
    <div class="form-group row">
        <label for="provin" class="col-sm-3 col-form-label">Jadikan Hot Price</label>
        <div class="col-sm-9">
            <div class="row" style="margin-left: 5px; margin-top: 5px;">
            <div class="col-sm-5">
                <input type="radio" class="form-check-input" name="hot" id="hot" value="Yes"> Yes
            </div>
            <div class="col-sm-5">
                <input type="radio" class="form-check-input" name="hot" id="hot" value="No"> No
            </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="jenkep" class="col-sm-3 col-form-label">Jenis Jual</label>
        <div class="col-sm-9">
            <select class="form-control" id="jnsjal" name="jnsjal" style="width: 560px;">
            <option value="">- Pilih Jenis Jual -</option>
            <option value="1">Lelang</option>
            <option value="0">Jual Sukarela</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="keterangan" class="col-sm-3 col-form-label"><u>Keterangan</u></label>
        </div>
        <div class="form-group row">
        <div class="col-sm-12">
            <textarea name="keter" id="keter" rows="10" cols="80"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <label for="image_upload" class="col-sm-3 col-form-label">Active</label>
        <div class="col-sm-5">
            <div class="row" style="margin-left: 5px; margin-top: 5px;">
            <div class="col-sm-5">
                <input type="radio" class="form-check-input" name="activen" id="activenx" value="1"> Yes
            </div>
            <div class="col-sm-5">
                <input type="radio" class="form-check-input" name="activen" id="activeny" value="0"> No
            </div>
            </div>
        </div>
    </div>
</div>

@section('js_pilihan')
<script type="text/javascript">
  $("#kadalkepemilik").datepicker();
  $('#priceview').keyup(function() {
    $(this).val(formatRupiah(this.value));
    let shw = this.value.split('.');
    if (shw.length > 1) {
      let nmb = ""; for (var i = 0; i < shw.length; i++) { nmb = nmb+shw[i]; }
      $('#price').val(parseInt(nmb));
    } else {
      $('#price').val(this.value);
    }
  });

  CKEDITOR.replace('keter');

  setInterval(updateTex,80);
  function updateTex() {
    var editorText = CKEDITOR.instances.keter.getData();
    $('#keter').html(editorText);
  }

  $.ajax({
    type:"GET",
    url : "{{ url('api/api_agunan_kategori') }}",
    contentType:"application/json",
    dataType: "json",
    success:function(res) {
      let isi = '<option data-display="Jenis Agunan">- Jenis Kepemilikan -</option>';
      for (var i = 0; i < res.length; i++) {
        if (res[i]['id'] == 1 || res[i]['id'] == 5) {
          isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['jenis_kepemilikan']+'</option>';
        }
      }
      $("#jnskep").html(isi);
      $("#jnskep").select2();
    }
  });
</script>
@endsection
