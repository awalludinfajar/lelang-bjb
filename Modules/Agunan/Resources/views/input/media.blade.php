<hr>
<div class="card-body">
  <div class="form-group row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header text-center">Upload Gambar</div>
        <div class="card-body profile-card">
          <div id="drop-img" role="button" class="text-center rounded-lg border border-secondary" style="border-style: dashed !important;">
            <span><i class="mdi mdi-plus h1"></i><br>Drag & Drop Image</span>
          </div>
          <div id="progress-image" class="w-100 h-100 mt-2"></div>
          <div class="row" id="preview-image"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<hr>
<div class="card-body">
  <div class="form-group row">
    <label class="col-sm-12 col-form-label"><u>Input Max 5 link Youtube</u></label>
  </div>
  <div class="form-group row">
      <label class="col-sm-2 col-form-label">1. Link Video </label>
      <div class="col-sm-10">
        <input type="text" name="video[]" id="video1" class="form-control" placeholder="link Video (Youtube)" value="">
      </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-2 col-form-label">2. Link Video </label>
      <div class="col-sm-10">
        <input type="text" name="video[]" id="video2" class="form-control" placeholder="link Video (Youtube)" value="">
      </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-2 col-form-label">3. Link Video </label>
      <div class="col-sm-10">
        <input type="text" name="video[]" id="video3" class="form-control" placeholder="link Video (Youtube)" value="">
      </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-2 col-form-label">4. Link Video </label>
      <div class="col-sm-10">
        <input type="text" name="video[]" id="video4" class="form-control" placeholder="link Video (Youtube)" value="">
      </div>
  </div>
  <div class="form-group row">
      <label class="col-sm-2 col-form-label">5. Link Video </label>
      <div class="col-sm-10">
        <input type="text" name="video[]" id="video5" class="form-control" placeholder="link Video (Youtube)" value="">
      </div>
  </div>
</div>

<hr>
<div class="card-body">
 <div class="form-group row">
   <div class="col-lg-12">
     <div class="card">
       <div class="card-header text-center">Upload Denah</div>
       <div class="card-body profile-card">
         <div id="drop-dnh" role="button" class="text-center rounded-lg border border-secondary" style="border-style: dashed !important;">
           <span><i class="mdi mdi-plus h1"></i><br>Drag & Drop Denah (Image)</span>
         </div>
         <div id="progress-denah" class="w-100 h-100 mt-2"></div>
         <div class="row" id="preview-denah"></div>
       </div>
     </div>
   </div>
 </div>
</div>

@section('js_media')
<script type="text/javascript">
  $(document).ready(function(){
    Global.dragdropImage('imgA','agunan','drop-img','preview-image','progress-image',"{{route('upload_image')}}","{{$link}}");
    Global.dragdropImage('denahA','denah','drop-dnh','preview-denah','progress-denah',"{{route('upload_denah')}}","{{$link}}");
  });

  function destroyImage(name, fldr) {
    var token = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
      title: 'Apa anda yakin?',
      text: "Menghapus image ini",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Iya',
      cancelButtonText: 'Tidak'
    }).then((result) => {
      if (result.isConfirmed) {
        $.post("{{route('removeimage')}}",{nme:name, fil:fldr, _token: token}, function (data) {
          if (data.status == 200) {
            Swal.fire({
              title: 'Hapus!',
              text: data.message,
              icon: 'success'
            }).then((result) => {
              document.getElementById("image_"+name).remove();
            });
          } else {
            Swal.fire('Hapus!','Gagal Dihapus','error');
          }
        });
      } else {
        Swal.fire('Hapus!','Gagal Dihapus','Anda Membatalkan Hapus');
      }
    });
  }
</script>
@endsection
