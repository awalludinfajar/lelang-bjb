<div class="modal fade bs-example-modal-md" id="publiss" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog " style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title mt-0">Input Tanggal Batas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="margin-right: 6px;">×</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="idpub" id="idpub" value="">
        <div class="form-group row">
          <label for="nm_banner" class="col-sm-3 col-form-label">Tanggal Lelang</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="tgllelang" id="tgllelang" placeholder="Tanggal Lelang" data-rule-required="true" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="nm_banner" class="col-sm-3 col-form-label">Tanggal Batas Akhir</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" name="akhlelang" id="akhlelang" placeholder="Batas Akhir" data-rule-required="true" required>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-primary submit-btn btn-block" id="stored">Publikasikan</button>
        </div>
      </div>
    </div>
  </div>
</div>
