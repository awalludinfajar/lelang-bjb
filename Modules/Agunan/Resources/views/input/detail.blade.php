<div class="card-body">
  <div class="form-group row">
    <label for="lstanah" class="col-sm-3 col-form-label">Luas Tanah</label>
    <div class="col-sm-8">
      <div class="input-group">
        <input type="number" class="form-control" id="lstanah" name="lstanah" style="width: 420px;" placeholder="Luas Tanah">
        <div class="input-group-append">
          <span class="input-group-text">Meter Persegi</span>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="lsbangun" class="col-sm-3 col-form-label">Luas Bangunan</label>
    <div class="col-sm-8">
      <div class="input-group">
        <input type="number" class="form-control" id="lsbangun" name="lsbangun" style="width: 420px;" placeholder="Luas Bangunan">
        <div class="input-group-append">
          <span class="input-group-text">Meter Persegi</span>
        </div>
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="jmlantai" class="col-sm-3 col-form-label">Jumlah Lantai</label>
    <div class="col-sm-9">
      <input type="number" class="form-control" id="jmlantai" name="jmlantai" style="width: 575px;" placeholder="jumlah lantai">
    </div>
  </div>
  <div class="form-group row">
    <label for="jmtidur" class="col-sm-3 col-form-label">Jumlah Kamar Tidur</label>
    <div class="col-sm-9">
      <input type="number" class="form-control" id="jmtidur" name="jmtidur" style="width: 575px;" placeholder="jumlah Kamar Tidur">
    </div>
  </div>
  <div class="form-group row">
    <label for="jmandi" class="col-sm-3 col-form-label">Jumlah kamar Mandi</label>
    <div class="col-sm-9">
      <input type="number" class="form-control" id="jmandi" name="jmandi" style="width: 575px;" placeholder="jumlah Kamar Mandi">
    </div>
  </div>
  <div class="form-group row">
    <label for="jmcarport" class="col-sm-3 col-form-label">Jumlah Garasi Carport</label>
    <div class="col-sm-9">
      <input type="number" class="form-control" id="jmcarport" name="jmcarport" style="width: 575px;" placeholder="jumlah Garasi Carport">
    </div>
  </div>
</div>
