<div class="card-body">
  <div class="form-group row">
    <label for="provin" class="col-sm-3 col-form-label">Provinsi</label>
    <div class="col-sm-9">
      <select class="form-control" id="provin" name="provin" style="width: 575px;">
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="ktaa" class="col-sm-3 col-form-label">Kabupaten Kota</label>
    <div class="col-sm-9">
      <select class="form-control" id="ktaa" name="ktaa" style="width: 575px;">
        <option data-display="Kabupaten Kota">- Kabupaten Kota -</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="kecama" class="col-sm-3 col-form-label">Kecamatan</label>
    <div class="col-sm-9">
      <select class="form-control" id="kecama" name="kecama" style="width: 575px;">
        <option data-display="Kabupaten Kota">- Kecamatan -</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="kelurahan" class="col-sm-3 col-form-label">Kelurahan</label>
    <div class="col-sm-9">
      <select class="form-control" id="kelur" name="kelur" style="width: 575px;">
        <option data-display="Kabupaten Kota">- Kelurahan -</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="kdps" class="col-sm-3 col-form-label">Kode Pos</label>
    <div class="col-sm-9">
      <input type="number" class="form-control" id="kdps" name="kdps" style="width: 575px;" placeholder="Kode Pos">
    </div>
  </div>
  <div class="form-group row">
    <label for="rrt" class="col-sm-3 col-form-label">RT/RW</label>
    <div class="col-sm-4">
      <input type="number" name="rt" id="rrt" class="form-control" placeholder="RT" value="">
    </div>
    /
    <div class="col-sm-4">
      <input type="number" name="rw" id="rrw" class="form-control" placeholder="RW" value="">
    </div>
  </div>
  <div class="form-group row">
    <label for="alamat" class="col-sm-3 col-form-label">Alamat Lengkap</label>
    <div class="col-sm-9">
      <textarea class="forom-control" name="alamat" id="alamat" placeholder="Alamat" rows="5" cols="61"></textarea>
    </div>
  </div>
  <div class="form-group row">
    <label for="dipeta" class="col-sm-3 col-form-label">Lokasi Di Peta</label>
    <div class="col-sm-9">
      <input type="text" id="pac-input" class="form-control col-sm-6" placeholder="Search Place" value="" style="margin-top: 14px;">
      <div id="map" style="width: 570px; height: 350px;"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="dipeta" class="col-sm-3 col-form-label">&nbsp;</label>
    <div class="col-sm-4">
      <input type="text" name="long" id="long" class="form-control" placeholder="Longitude" value="">
    </div>
    -
    <div class="col-sm-4">
      <input type="text" name="lat" id="lat" class="form-control" placeholder="Latitude" value="">
    </div>
  </div>
</div>
@section('js_lokasi')
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8Lo7ml-uJcGwnpY0JiSPv8IYG3nr5MTI&callback=initMap&libraries=places"></script>
<script type="text/javascript">
  function initMap() {
    const islatlng = { lat: -0.8405886016068369, lng: 117.1077304482181 };

    const map = new google.maps.Map(document.getElementById("map"), {
      center: islatlng,
      zoom: 4,
    });

    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    map.addListener("bounds_changed", () => {
      searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener("places_changed", () => {
      const places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      markers.forEach((marker) => {
        marker.setMap(null);
      });
      markers = [];

      const bounds = new google.maps.LatLngBounds();
      places.forEach((place) => {
        if (!place.geometry || !place.geometry.location) {
          console.log("Returned place contains no geometry");
          return;
        }
      });

      const icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };

      markers.push(
        new google.maps.Marker({
          map,
          icon,
          title: place.name,
          position: place.geometry.location,
        })
      );

    });

    let infoWindow = new google.maps.InfoWindow({
      content: "Click To get Logitude and Latitude",
      position: islatlng,
    });
    infoWindow.open(map);

    map.addListener("click", (mapsMouseEvent) => {

      infoWindow.close();
      $('#lat').val(mapsMouseEvent.latLng.toJSON().lat);
      $('#long').val(mapsMouseEvent.latLng.toJSON().lng);

      infoWindow = new google.maps.InfoWindow({
        position: mapsMouseEvent.latLng,
      });
      console.log(JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2));

      let coord = mapsMouseEvent.latLng.toJSON();
      $("#long").val(coord['lng']);
      $("#lat").val(coord['lat']);

      infoWindow.setContent('Here!');
      infoWindow.open(map);
    });
  }

  $("#ktaa").select2();
  $("#ktaa").prop("disabled", true);
  $("#kecama").select2();
  $("#kecama").prop("disabled", true);
  $("#kelur").select2();
  $("#kelur").prop("disabled", true);

  $.ajax({
    type:"GET",
    url : "{{ url('api/api_data_provinsi') }}",
    contentType:"application/json",
    dataType: "json",
    success:function(res) {
      let isi = '<option data-display="Provinsi">- Provinsi -</option>';
      for (var i = 0; i < res.length; i++) {
        isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
      }
      $("#provin").html(isi);
      $("#provin").select2();
    }
  });

  $('#provin').change(function () {
    $.ajax({
      type:"GET",
      url : "{{ url('api/api_data_kabkot') }}/"+$(this).val(),
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        let isi = '<option data-display="Kabupaten Kota">- Kabupaten Kota -</option>';
        for (var i = 0; i < res.length; i++) {
          isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
        }
        $("#ktaa").html(isi);
        $("#ktaa").select2();
        $("#ktaa").prop("disabled", false);
      }
    });
  });

  $('#ktaa').change(function () {
    $.ajax({
      type:"GET",
      url : "{{ url('api/api_data_kecamatan') }}/"+$(this).val(),
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        let isi = '<option data-display="Kecamatan">- Kecamatan -</option>';
        for (var i = 0; i < res.length; i++) {
          isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
        }
        $("#kecama").html(isi);
        $("#kecama").select2();
        $("#kecama").prop("disabled", false);
      }
    });
  });

  $('#kecama').change(function () {
    $.ajax({
      type:"GET",
      url : "{{ url('api/api_data_kelurahan') }}/"+$(this).val(),
      contentType:"application/json",
      dataType: "json",
      success:function(res) {
        let isi = '<option data-display="Kelurahan">- Kelurahan -</option>';
        for (var i = 0; i < res.length; i++) {
          isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
        }
        $("#kelur").html(isi);
        $("#kelur").select2();
        $("#kelur").prop("disabled", false);
      }
    });
  });
</script>
@endsection
