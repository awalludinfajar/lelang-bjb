@extends('backend::layouts.master')

@section('custom_css')
<link href="https://cdn.jsdelivr.net/npm/smartwizard@5/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
@yield('css_media')
@endsection

@section('content')
 <form id="formAgunan" novalidate="" enctype="multipart/form-data">
    <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <a href="{{ route('agunan') }}" class="btn btn-primary btn-fw" style="margin-bottom: 15px;" onclick="goBack()">Kembali</a>
            <div id="smartwizard">
                <ul class="nav">
                <li><a class="nav-link" href="#step-1">Step 1<br>Jenis Agunan</a></li>
                <li><a class="nav-link" href="#step-2">Step 2<br>Lokasi</a></li>
                <li><a class="nav-link" href="#step-3">Step 3<br>Detail </a></li>
                <li><a class="nav-link" href="#step-4">Step 4<br>Gambar/video</a></li>
                <li><a class="nav-link" href="#step-5">Step 5<br>Opsi</a></li>
                </ul>
                <div class="tab-content">
                <div id="step-1" class="tab-pane" role="tabpanel">
                    <div class="card-body">
                      <div class="form-group row">
                        <label for="kategori" class="col-sm-3 col-form-label">Judul Agunan</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="jdlag" id="jdlag" style="width: 480px;" placeholder="Judul Pada Agunan">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="kategori" class="col-sm-3 col-form-label">Jenis Agunan</label>
                        <div class="col-sm-9">
                          <select class="form-control" name="jnskp" id="jnskp" style="width: 480px;"></select>
                        </div>
                      </div>
                    </div>
                </div>
                <div id="step-2" class="tab-pane" role="tabpanel">
                    @include('agunan::input.lokasi')
                </div>
                <div id="step-3" class="tab-pane" role="tabpanel">
                    @include('agunan::input.detail')
                </div>
                <div id="step-4" class="tab-pane" role="tabpanel">
                    @include('agunan::input.media')
                </div>
                <div id="step-5" class="tab-pane" role="tabpanel">
                    @include('agunan::input.pilihan')
                </div>
                </div>
            </div>
          </div>
        </div>
    </div>
 </form>
@endsection

@section('custom_js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
  // Sintak Validasi
  $("#formAgunan").validate({
    rules: {
    },
    messages : {
    },
    submitHandler: function(form) {
      var formData = new FormData(form);
      storeData(formData);
      return false;
    }
  });
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/smartwizard@5.1.1/dist/js/jquery.smartWizard.min.js"></script>
<script type="text/javascript">
  window.onbeforeunload = function(event) {
    return confirm("Confirm refresh");
  };

  $(document).ready(function(){
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'arrows',
        autoAdjustHeight:false,
        transitionEffect:'fade',
        showStepURLhash: false,
        toolbarSettings: {
          toolbarExtraButtons: [
            $('<button type="submit"></button>').text('Finish').addClass('btn btn-info').attr('id', 'laststep')
          ]
        }
    });

    $('#laststep').hide();
    $('.sw-btn-next').on('click', function () {
      var cekk = window.location.href.split('#');
      if (cekk[1] == 'step-5') {
        $('#laststep').show();
      }
    });

    $('.sw-btn-prev').on('click', function () {
      $('#laststep').hide();
    });

    var onn = window.location.href.split('#');
    if (onn[1] == 'step-5') {
      $('#laststep').show();
    }

  });

  function storeData(formdata) {
	   $.ajax({
		   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		   type: "POST",
		   url : "{{ route('store') }}",
		   dataType:"json",
		   data: formdata,
		   cache: false,
		   processData: false,
		   contentType: false,
       beforeSend: function(){
         Swal.fire({
           title: 'Wait ...',
           onBeforeOpen () { Swal.showLoading () },
           onAfterClose () { Swal.hideLoading() },
           allowOutsideClick: false,
           allowEscapeKey: false,
           allowEnterKey: false
         })
       },
		   success: function(data) {
			   if(data.status==true) {
				   $('.progress').fadeOut();
				   Swal.fire({
             title: 'Berhasil!',
             text: data.msg,
             icon: 'success',
             confirmButtonText: `Oke`
           }).then((result) => {
             if (result.isConfirmed) {
               window.location = "{{ route('agunan') }}";
             }
           });
			   } else {
				   Swal.fire({
             title: 'Gagal!',
             text: data.msg,
             icon: 'error'
           });
			   }
		   },
       error: function(data) {
         console.log(data);
       }
     });
   }

  $.ajax({
    type:"GET",
    url : "{{ url('api/api_kategori_agunan') }}",
    contentType:"application/json",
    dataType: "json",
    success:function(res) {
      let isi = '<option data-display="Jenis Agunan">- Jenis Agunan -</option>';
      for (var i = 0; i < res.length; i++) {
        isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama_kategori']+'</option>';
      }
      $("#jnskp").html(isi);
      $("#jnskp").select2();
    }
  });
</script>
@yield('js_lokasi')
@yield('js_pilihan')
@yield('js_media')
@endsection
