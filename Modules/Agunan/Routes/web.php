<?php

use Modules\Backend\Http\Controllers\AuthController;
use Modules\Backend\Http\Controllers\BackendController;
use Modules\Agunan\Http\Controllers\AgunanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('backend/agunan')->group(function() {
  Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [BackendController::class, 'index'])->name('home');
    Route::get('logout',[AuthController::class, 'destroy'])->name('logout');
  });
  Route::get('/', [AgunanController::class, 'index'])->name('agunan');
  Route::get('/loaddata', [AgunanController::class, 'show'])->name('loaddataAgunan');
  Route::get('data/input', [AgunanController::class, 'create'])->middleware('auth')->name('input_agunan');
  Route::post('data/upload/image', [AgunanController::class, 'uploadImage'])->middleware('auth')->name('upload_image');
  Route::post('data/upload/denah', [AgunanController::class, 'uploadDenah'])->middleware('auth')->name('upload_denah');
  Route::post('data/remove/image/denah', [AgunanController::class, 'removeaset'])->middleware('auth')->name('removeimage');
  // Route::post('data/upload/video', [AgunanController::class, 'uploadVideo'])->middleware('auth')->name('upload_video');
  Route::post('data/store', [AgunanController::class, 'store'])->middleware('auth')->name('store');
  Route::get('data/remove/{id}', [AgunanController::class, 'destroy'])->middleware('auth')->name('removeagunan');
});

Route::get('/agunan/list/{cek}', [DataAgunanController::class, 'showlist']);
