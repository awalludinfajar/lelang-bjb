<?php

use Illuminate\Http\Request;
use Modules\Agunan\Http\Controllers\DataAgunanController;
// use Modules\Agunan\Http\Controllers\AgunanController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/agunan', function (Request $request) {
    return $request->user();
});

// filter data agunan frontend
Route::post('/agunan/filter_agunan', [DataAgunanController::class, 'show']);
Route::post('/agunan/publikasikann', [DataAgunanController::class, 'update']);
Route::get('/agunan/list/{name}', [DataAgunanController::class, 'showlist']);
