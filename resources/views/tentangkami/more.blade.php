<div class="sticky-top">
  <aside>
    <div class="widget__sidebar mt-0">
      <div class="widget__sidebar__header">
        <h6 class="text-capitalize">search</h6>
      </div>
      <div class="widget__sidebar__body">
        <div class="input-group">
          <input type="text" name="search_term_string" class="form-control" placeholder="Cari . . .">
          <span class="input-group-btn">
            <button type="submit" class="btn-search btn"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </div>
    </div>
  </aside>
  <aside>
    <div class="widget__sidebar">
      <div class="widget__sidebar__header">
        <h6 class="text-capitalize">recents news</h6>
      </div>
      <div class="widget__sidebar__body widg_news"></div>
    </div>
  </aside>
</div>
