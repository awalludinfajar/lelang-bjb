<div class="tab-pane fade show active" id="pills-tab-two" role="tabpanel" aria-labelledby="pills-tab-two">
  <div class="row">
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4">
      <div class="card__image card__box-v1">
        <div class="card__image-header h-250">
          <div class="ribbon text-capitalize">featured</div>
          <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
          <div class="info"> for sale</div>
        </div>
        <div class="card__image-body">
          <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
          <h6 class="text-capitalize">vila in coral gables</h6>
          <p class="text-capitalize"><i class="fa fa-map-marker"></i>Bandung, Jawa Barat</p>
          <ul class="list-inline card__content">
            <li class="list-inline-item">
              <span> KM <br><i class="fa fa-bath"></i> 2 </span>
            </li>
            <li class="list-inline-item">
              <span> KT <br><i class="fa fa-bed"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
            </li>
            <li class="list-inline-item">
              <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
            </li>
          </ul>
        </div>
        <div class="card__image-footer">
          <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
          <ul class="list-inline my-auto">
            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
          </ul>
          <ul class="list-inline my-auto ml-auto">
            <li class="list-inline-item"><h6>Call</h6></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="cleafix"></div>
</div>
