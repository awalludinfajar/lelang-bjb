@extends('layouts.detail')

@section('content_detail')
<div class="bg-light">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section__breadcrumb-v1">
          <ol class="breadcrumb mb-0 bg-light">
            <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i></a></li>
            <li class="breadcrumb-item"> <a href="#">Daftar </a></li>
            <li class="breadcrumb-item active"> <span class="text-capitalize"> agunan bjb Bangunan</span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>

@include('content.filter')
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="tabs__custom-v2 ">
              <ul class="nav nav-pills myTab" role="tablist">
                <li class="list-inline-item mr-auto">
                  <span class="title-text">Sort by</span>
                  <div class="btn-group">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bangunan</a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="javascript:void(0)">Low to High Price</a>
                      <a class="dropdown-item" href="javascript:void(0)">High to Low Price</a>
                      <a class="dropdown-item" href="javascript:void(0)">Jual Sukarela</a>
                      <a class="dropdown-item" href="javascript:void(0)">Lelang Segera</a>
                    </div>
                  </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="pill" href="#pills-tab-one" role="tab" aria-controls="pills-tab-one" aria-selected="true">
                    <span class="fa fa-th-list"></span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="pill" href="#pills-tab-two" role="tab" aria-controls="pills-tab-two" aria-selected="false">
                    <span class="fa fa-th-large"></span>
                  </a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                @include('listagunan.grid')
                @include('listagunan.box')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cta-v1 py-5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9">
        <h2 class="text-uppercase text-white">Ingin membeli property atau mengikuti lelang?</h2>
        <p class="text-capitalize text-white">Kami Akan Membantu Anda untuk Properti Terbaik</p>
      </div>
      <div class="col-lg-3">
        <a href="#" class="btn btn-light text-uppercase ">dapatkan penawaran <i class="fa fa-angle-right ml-3 arrow-btn "></i></a>
      </div>
    </div>
  </div>
</section>
@endsection
