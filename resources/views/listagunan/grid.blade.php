<div class="tab-pane fade" id="pills-tab-one" role="tabpanel" aria-labelledby="pills-tab-one">
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card__image card__box-v1">
        <div class="row no-gutters">
          <div class="col-md-4 col-lg-3 col-xl-4">
            <div class="card__image__header h-250">
              <a href="#">
                <div class="ribbon text-capitalize">sold out</div>
                <img src="images/600x400.jpg" alt="" class="img-fluid w100 img-transition">
                <div class="info"> for sale</div>
              </a>
            </div>
          </div>
          <div class="col-md-4 col-lg-6 col-xl-5 my-auto">
            <div class="card__image__body">
              <span class="badge badge-primary text-capitalize mb-2">bangunan Kode Lelang 12345678</span>
              <h6><a href="#">vila in coral gables</a></h6>
              <div class="card__image__body-desc">
                <p class="text-capitalize"><i class="fa fa-map-marker"></i> Bandung, Jawa Barat</p>
              </div>
              <ul class="list-inline card__content">
                <li class="list-inline-item">
                  <span> KM <br><i class="fa fa-bath"></i> 2 </span>
                </li>
                <li class="list-inline-item">
                  <span> KT <br><i class="fa fa-bed"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Ruangan <br><i class="fa fa-inbox"></i> 3 </span>
                </li>
                <li class="list-inline-item">
                  <span> Luas <br><i class="fa fa-map"></i> 4300 ㎡ </span>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
            <div class="card__image__footer">
              <figure><img src="images/80x80.jpg" alt="" class="img-fluid rounded-circle"></figure>
              <ul class="list-inline my-auto">
                <li class="list-inline-item name"><a href="#"> Div PPK bjb </a></li>
              </ul>
              <ul class="list-inline my-auto ml-auto price">
                <li class="list-inline-item "><h6>Call</h6></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
