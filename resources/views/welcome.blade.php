@extends('layouts.master')

@section('content_first')

@include('content.filter')

<section class="featured__property">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 mx-auto">
        <div class="title__head">
          <h2 class="text-center text-capitalize"> Jual Sukarela </h2>
          <p class="text-center text-capitalize">Jual Sukarela</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" id="sukarela"></div>
    </div>
  </div>
</section>

<section class="featured__property">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 mx-auto">
        <div class="title__head">
          <h2 class="text-center text-capitalize"> Lelang Segera </h2>
          <p class="text-center text-capitalize">Dalam Waktu Dekat</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" id="segera"></div>
    </div>
  </div>
</section>

<section class="featured__property">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 mx-auto">
        <div class="title__head">
          <h2 class="text-center text-capitalize"> Hot Price </h2>
          <p class="text-center text-capitalize">Agunan Dengan Harga Terbaik</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" id="pricehot"></div>
    </div>
  </div>
</section>

<section class="featured__property">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 mx-auto">
        <div class="title__head">
          <h2 class="text-center text-capitalize"> Terbaru </h2>
          <p class="text-center text-capitalize">Kami Memberikan Layanan Penuh Di Setiap Langkah.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" id="newst"></div>
    </div>
  </div>
</section>

<section class="featured__property">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 mx-auto">
        <div class="title__head">
          <h2 class="text-center text-capitalize"> Hasil Pencarian </h2>
          <p class="text-center text-capitalize">Kami Memberikan Layanan Penuh Di Setiap Langkah.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" id="alldata">
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
function detail(va){
  if (va == 0) {
    window.location = '/detailagunan/20';
  } else {
    window.location = '/detailagunan/'+va;
  }
}
</script>

@include('content.about')

@include('content.testimoni')

@include('content.list')

@include('content.berita')

@endsection
