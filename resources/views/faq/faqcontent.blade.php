<div class="single__blog-detail">
  <h1>F A Q</h1>
  <div class="single__blog-detail-info">
    <ul class="list-inline">
      <li class="list-inline-item">
        <figure class="image-profile">
          <img src="{{ asset('modules/frontend/img/80x80.jpg') }}" class="img-fluid" alt="">
        </figure>
      </li>
      <li class="list-inline-item">
        <span>Oleh <a href="#">Divisi PPK bank bjb,</a></span>
      </li>
    </ul>
  </div>
  <figure><img src="{{ asset('modules/frontend/img/faq.jpg') }}" class="img-fluid" alt=""></figure>
  <p>The European languages are members of the same family. Their separate existence is a myth.
  For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
  in their grammar, their pronunciation and their most common words. Everyone realizes why a
  new common language would be desirable: one could refuse to pay expensive translators.</p>

  <p>To achieve this, it would be necessary to have uniform grammar, pronunciation and more
  common words. If several languages coalesce, the grammar of the resulting language is more simple
  and regular than that of the individual languages. The new common language will be more simple
  and regular than the existing European languages. It will be as simple as Occidental; in fact,
  it will be Occidental. To an English person, it will seem like simplified English, as a skeptical
  Cambridge friend of mine told me what Occidental is.</p>

  <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
  there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the
  Semantics, a large language ocean. A small river named Duden flows by their place and
  supplies it with the necessary regelialia.</p>

  <div class="wrap__profile">
    <div class="wrap__profile-author">
      <figure><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid img-circle"></figure>
      <div class="wrap__profile-author-detail">
        <div class="wrap__profile-author-detail-name">author</div>
        <h5>Divisi PPK bjb</h5>
        <p>Divisi Penyelamatan dan Penyelesaian Kredit </p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#" class="btn btn-social btn-social-o facebook "><i class="fa fa-facebook"></i></a>
          </li>
          <li class="list-inline-item">
            <a href="#" class="btn btn-social btn-social-o twitter "><i class="fa fa-twitter"></i></a>
          </li>
          <li class="list-inline-item">
            <a href="#" class="btn btn-social btn-social-o instagram "><i class="fa fa-instagram"></i></a>
          </li>
          <li class="list-inline-item">
            <a href="#" class="btn btn-social btn-social-o telegram "><i class="fa fa-telegram"></i></a>
          </li>
          <li class="list-inline-item">
            <a href="#" class="btn btn-social btn-social-o linkedin "><i class="fa fa-linkedin"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
