@extends('layouts.detail')

@section('content_detail')
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        @include('faq.faqcontent')
      </div>
      <div class="col-lg-4">
        @include('tentangkami.more')
      </div>
    </div>
  </div>
</section>
@endsection
