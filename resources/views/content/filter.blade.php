<p>&nbsp;</p>
<h4 style="text-align:center;">Pencarian Khusus</h4>
<p></p>
<p></p>
<p></p>

<div class="clearfix"></div>
<div class="search__area bg-light">
  <div class="container">
    <div class="search__area-inner">
      <div class="row">
        <div class="col-6 col-lg-3 col-md-3 conditionhere">
          <div class="form-group">
            <label for="">Kategori</label>
            <select class="form-control" id="katgr" name="katgr">
            </select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <label for="">Provinsi</label>
            <select class="form-control" id="provin" name="provin">
            </select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <label for="">Kota</label>
            <select class="form-control" id="ktaa" name="ktaa">
            </select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <label for="">Jenis Kepemilikan</label>
            <select class="form-control" id="jnskp" name="jnskp"></select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <span>Harga Minimum</span>
            <select class="form-control" id="harmin" name="harmin">
            </select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <span>Harga Maksimum</span>
            <select class="form-control" id="harmax" name="harmax">
            </select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <span>Tanggal Lelang(Dari)</span>
            <input type="text" class="form-control" id="datepicker" name="tgldari" placeholder="mm/dd/yyyy"/>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <span>Tanggal Lelang(Sampai)</span>
            <input type="text" class="form-control" id="datepicker2" name="tglsamp" placeholder="mm/dd/yyyy"/>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-check">
            <span>&nbsp;</span>
            <div class="custom-control custom-checkbox checkbox-xl">
              <input type="checkbox" class="custom-control-input" id="checkbox-3" name="hot" value="Yes">
              <label class="custom-control-label" for="checkbox-3">Hot Price</label>
            </div>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <span>Cara Jual</span>
            <select class="form-group" id="carajual" name="carajual">
              <option value=""></option>
              <option value="1">Lelang</option>
              <option value="0">Jual Sukarela</option>
            </select>
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3">
          <div class="form-group">
            <span>Periode Lelang</span>
            <input type="text" class="form-control" id="periodelelang" name="periodelelang">
          </div>
        </div>
        <div class="col-6 col-lg-3 col-md-3"></div>
        <div class="col-12 col-lg-45 col-md-5">
          <div class="form-group">
            <span>Luas Tanah</span>
            <div class="filter__price">
              <input class="price-range" type="text" name="luas_tanah" id="luas_tanah" value="" />
            </div>
          </div>
        </div>
        <div class="col-1 col-lg-1 col-md-1"></div>
        <div class="col-12 col-lg-5 col-md-5">
          <div class="form-group">
            <span>Luas Bangunan</span>
            <div class="filter__price">
              <input class="price-range" type="text" name="luas_bangunan" id="luas_bangunan" value="" />
            </div>
          </div>
        </div>
        <div class="col-1 col-lg-1 col-md-1"></div>
        <div class="col-6 col-lg-3 col-md-3"></div>
        <div class="col-12 col-lg-3 col-md-3">
          <div class="form-group">
            <button type="submit" class="btn btn-primary text-uppercase btn-block" id="ceksend"> search <i class="fa fa-search ml-1"></i></button>
          </div>
        </div>
        <div class="col-12 col-lg-3 col-md-3">
          <div class="form-group">
            <button type="submit" class="btn btn-primary text-uppercase btn-block" id="resetsend"> Reset <i class="fa fa-refresh ml-1"></i></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
