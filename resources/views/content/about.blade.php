<section class="home__about bg-theme-v4">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="title__leading">
          <h2 class="text-capitalize text-white"> Tentang Kami</h2>
          <p class="text-capitalize text-white">
              Langkah pertama dalam menjual properti Anda adalah mendapatkan penilaian dari ahli lokal.
              Mereka akan
              periksa rumah Anda dan pertimbangkan fitur-fiturnya yang unik, area dan kondisi pasarnya
              sebelum memberi Anda penilaian yang paling akurat.
          </p>
          <p class="text-capitalize text-white"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              Quod libero amet, laborum qui nulla
              quae alias tempora. Placeat voluptatem eum numquam quas distinctio obcaecati quaerat,
              repudiandae qui! Quia, omnis, doloribus! Lorem ipsum dolor sit amet, consectetur adipisicing
              elit. Quod libero amet, laborum qui nullas tempora.</p>
          <a href="info/tentangkami" class="btn btn-primary mt-3 text-capitalize"> read more<i class="fa fa-angle-right ml-3 "></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
