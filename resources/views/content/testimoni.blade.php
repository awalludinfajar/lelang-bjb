<section>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 mx-auto">
        <div class="title__head">
          <h2 class="text-center text-capitalize">Testimony</h2>
          <p class="text-center text-capitalize">apa kata mereka</p>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="testimonial owl-carousel owl-theme owl-loaded owl-drag">
      <div class="owl-stage-outer owl-height" style="height: 238px;">
        <div class="owl-stage" style="transform: translate3d(-1665px, 0px, 0px); transition: all 1.2s ease 0s; width: 6105px;">
          <div class="owl-item cloned" style="width: 535px; margin-right: 20px;">
            <div class="item testimonial__block">
              <div class="testimonial__block-card bg-reviews">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Quod libero amet, laborum qui nulla
                  quae alias tempora. Placeat voluptatem eum numquam quas distinctio obcaecati quaerat,
                  repudiandae qui! Quia, omnis, doloribus! Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit. Quod libero amet, laborum qui nullas tempora.
                </p>
              </div>
              <div class="testimonial__block-users">
                <div class="testimonial__block-users-img"><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid"></div>
                <div class="testimonial__block-users-name">Aldi <br><span>math</span></div>
              </div>
            </div>
          </div>
          <div class="owl-item cloned" style="width: 535px; margin-right: 20px;">
            <div class="item testimonial__block">
              <div class="testimonial__block-card bg-reviews">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Quod libero amet, laborum qui nulla
                  quae alias tempora. Placeat voluptatem eum numquam quas distinctio obcaecati quaerat,
                  repudiandae qui! Quia, omnis, doloribus! Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit. Quod libero amet, laborum qui nullas tempora.
                </p>
              </div>
              <div class="testimonial__block-users">
                <div class="testimonial__block-users-img"><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid"></div>
                <div class="testimonial__block-users-name">Aldi <br><span>math</span></div>
              </div>
            </div>
          </div>
          <div class="owl-item cloned" style="width: 535px; margin-right: 20px;">
            <div class="item testimonial__block">
              <div class="testimonial__block-card bg-reviews">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Quod libero amet, laborum qui nulla
                  quae alias tempora. Placeat voluptatem eum numquam quas distinctio obcaecati quaerat,
                  repudiandae qui! Quia, omnis, doloribus! Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit. Quod libero amet, laborum qui nullas tempora.
                </p>
              </div>
              <div class="testimonial__block-users">
                <div class="testimonial__block-users-img"><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid"></div>
                <div class="testimonial__block-users-name">Aldi <br><span>math</span></div>
              </div>
            </div>
          </div>
          <div class="owl-item cloned" style="width: 535px; margin-right: 20px;">
            <div class="item testimonial__block">
              <div class="testimonial__block-card bg-reviews">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Quod libero amet, laborum qui nulla
                  quae alias tempora. Placeat voluptatem eum numquam quas distinctio obcaecati quaerat,
                  repudiandae qui! Quia, omnis, doloribus! Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit. Quod libero amet, laborum qui nullas tempora.
                </p>
              </div>
              <div class="testimonial__block-users">
                <div class="testimonial__block-users-img"><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid"></div>
                <div class="testimonial__block-users-name">Aldi <br><span>math</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="owl-nav disabled">
        <button type="button" role="presentation" class="owl-prev"><i class="fa fa-angle-left"></i></button>
        <button type="button" role="presentation" class="owl-next"><i class="fa fa-angle-right"></i></button>
      </div>
      <div class="owl-dots disabled"></div>
    </div>
  </div>
</section>
