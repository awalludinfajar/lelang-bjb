<section class="bg-theme-v1">
  <div class="cta">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-md-12 col-lg-12 text-center">
          <h2 class="text-uppercase text-white">DAFTAR & DAPATKAN RUMAH IMPIAN ANDA</h2>
          <p class="text-capitalize text-white">Pelayanan terbaik dari kami</p>
          <a href="#" class="btn btn-primary text-uppercase ">Dapatkan penawaran<i class="fa fa-angle-right ml-3 arrow-btn "></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
