@extends('layouts.detail')

@section('content_detail')
@foreach ($data as $berita)

<!-- LISTING LIST -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="single__blog-detail">
                    <h1>
                        {{ $berita->judul }}
                    </h1>

                    <div class="single__blog-detail-info">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <figure class="image-profile">
                                    <img src="{{ asset('modules/frontend/img/logo-putih.png') }}" class="img-fluid"
                                        alt="">
                                </figure>
                            </li>
                            <li class="list-inline-item">

                                <span>
                                    by
                                </span>
                                <a href="#">
                                    {{ $berita->author }}
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <span class="text-dark text-capitalize ml-1">
                                    <?php echo date_format($berita->created_at, "M, d Y")?>
                                </span>
                            </li>

                            <li class="list-inline-item">
                                <span class="text-dark text-capitalize ml-1">
                                    in
                                </span>
                                <a href="#">
                                    {{ $berita->jenis_berita}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <figure>
                        <img src="{{ asset('upload/blogs/img/'.$berita->image) }}" width="350px" class="img-fluid" alt="">
                    </figure>

                    <p class="drop-cap">
                        <?php echo $berita->description; ?>
                    </p>


                    <!-- TAGS -->
                    <!-- <div class="blog__tags mb-4">
                        <ul class="list-inline">

                            <li class="list-inline-item">
                                <a href="#">
                                    #property
                                </a>
                            </li>


                            <li class="list-inline-item">
                                <a href="#">
                                    #real estate
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    #listing
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    #rent
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    #sell
                                </a>
                            </li>
                        </ul>
                    </div> -->
                    <!-- END TAGS -->

                    <!-- AUTHOR -->
                    <!-- Profile author -->
                    <!-- <div class="wrap__profile">
                        <div class="wrap__profile-author">
                            <figure>
                                <img src="{{ asset('modules/frontend/img/logo-putih.png') }}" alt=""
                                    class="img-fluid img-circle">
                            </figure>
                            <div class="wrap__profile-author-detail">
                                <div class="wrap__profile-author-detail-name">author</div>
                                <h5>jhon doe</h5>
                                <p>Hi, nice to meet you let me introduce for you, I am Jhon Doe freelancer designer &
                                    web programmer from
                                    indonesia,
                                    base lampung</p>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a href="#" class="btn btn-social btn-social-o facebook ">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="#" class="btn btn-social btn-social-o twitter ">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="#" class="btn btn-social btn-social-o instagram ">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="#" class="btn btn-social btn-social-o telegram ">
                                            <i class="fa fa-telegram"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="#" class="btn btn-social btn-social-o linkedin ">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                    <!-- END AUTHOR -->
                    @endforeach
                    <hr>
                    <div class="row">
                    @foreach($previous as $prev)
                        <div class="col-md-6">
                            <div class="single_navigation-prev">
                                <a href="{{ asset('blogsread/'.$prev->judul) }}">
                                    <span>previous post</span>
                                    {{ $prev->judul}}
                                </a>
                            </div>
                        </div>
                        @endforeach
                    @foreach($nextpost as $np)
                        <div class="col-md-6">
                            <div class="single_navigation-next text-left text-md-right">
                                <a href="{{ asset('blogsread/'.$np->judul) }}">
                                    <span>next post</span>
                                    {{ $np->judul}}
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <!-- WIDGET BLOG -->
            <div class="col-lg-4">
                <div class="sticky-top">
                    <!-- <aside>
                        <div class="widget__sidebar mt-0">
                            <div class="widget__sidebar__header">
                                <h6 class="text-capitalize">search</h6>
                            </div>
                            <div class="widget__sidebar__body">
                                <div class="input-group">
                                    <input type="text" name="search_term_string" class="form-control"
                                        placeholder="Search article . . .">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn-search btn"><i
                                                class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>

                        </div>
                    </aside> -->
                    <!-- <aside>
                        <div class="widget__sidebar">
                            <div class="widget__sidebar__header">
                                <h6 class="text-capitalize">category</h6>
                            </div>
                            <div class="widget__sidebar__body">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#" class="text-capitalize">
                                            apartement
                                            <span class="badge badge-primary">14</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-capitalize">
                                            villa
                                            <span class="badge badge-primary">4</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-capitalize">
                                            family house
                                            <span class="badge badge-primary">2</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-capitalize">
                                            modern villa
                                            <span class="badge badge-primary">8</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-capitalize">
                                            town house
                                            <span class="badge badge-primary">10</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="text-capitalize">
                                            office
                                            <span class="badge badge-primary">12</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </aside> -->
                    <aside>
                        <div class="widget__sidebar">
                            <div class="widget__sidebar__header">
                                <h6 class="text-capitalize">recents news</h6>
                            </div>
                            <div class="widget__sidebar__body">
                                <!-- ONE -->
                                @foreach($terkini as $baru)
                                <div class="widget__sidebar__body-img">
                                    <img src="{{ asset('upload/blogs/img/'.$baru->image) }}" alt="" class="img-fluid" width="80px" height="80px">
                                    <div class="widget__sidebar__body-heading">
                                        <a href="{{ asset('blogsread/'.$baru->judul) }}"><h6 class="text-capitalize"> {{ $baru->judul }}</h6></a>

                                    </div>
                                    <span class="badge badge-secondary p-1 text-capitalize mb-1"><?php echo date_format($baru->created_at, "D M Y") ?> </span>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <!-- END WIDGET BLOG -->
    </div>
</section>
<!-- END LISTING LIST -->
@endsection
