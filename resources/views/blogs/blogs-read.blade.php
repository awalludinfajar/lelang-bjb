@extends('layouts.detail')

@section('content_detail')
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="single__blog-detail">
          <h1>{{ $data[0]->judul }}</h1>
          <div class="single__blog-detail-info">
            <ul class="list-inline">
              <li class="list-inline-item">
                <span> by </span><a href="#">{{ $data[0]->author }},</a>
              </li>
              <li class="list-inline-item">
                <span class="text-dark text-capitalize ml-1">{{ $data[0]->created_at }}</span>
              </li>
              <li class="list-inline-item">
                <span class="text-dark text-capitalize ml-1">in</span>
                <a href="#">{{ $data[0]->jenis_berita }}</a>
              </li>
            </ul>
          </div>
          <figure><img src="{{ asset('upload/blogs/img/'.$data[0]->image) }}" width="350px" class="img-fluid" alt=""></figure>
          {!! $data[0]->description !!}
          <div class="wrap__profile">
            <div class="wrap__profile-author">
              <div class="wrap__profile-author-detail">
                <div class="wrap__profile-author-detail-name">author</div>
                <h5>{{ $data[0]->author }}</h5>
                <p>Hi, nice to meet you let me introduce for you, I am Jhon Doe freelancer designer &amp; web programmer from
                    indonesia,
                    base lampung</p>
                <ul class="list-inline">
                  <li class="list-inline-item">
                    <a href="#" class="btn btn-social btn-social-o facebook "><i class="fa fa-facebook"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="btn btn-social btn-social-o twitter "><i class="fa fa-twitter"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="btn btn-social btn-social-o instagram "><i class="fa fa-instagram"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="btn btn-social btn-social-o telegram "><i class="fa fa-telegram"></i></a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="btn btn-social btn-social-o linkedin "><i class="fa fa-linkedin"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <hr>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="sticky-top">
          <aside>
            <div class="widget__sidebar mt-0">
              <div class="widget__sidebar__header">
                <h6 class="text-capitalize">search</h6>
              </div>
              <div class="widget__sidebar__body">
                <div class="input-group">
                  <input type="text" name="search_term_string" class="form-control" placeholder="Search article . . .">
                  <span class="input-group-btn">
                    <button type="submit" class="btn-search btn"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
            </div>
            <div class="widget__sidebar">
              <div class="widget__sidebar__header">
                <h6 class="text-capitalize">recents news</h6>
              </div>
              <div class="widget__sidebar__body widg_news">
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
