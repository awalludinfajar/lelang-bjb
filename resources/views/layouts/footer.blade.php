<!-- Footer  -->
<footer>
    <div class="wrapper__footer bg-theme-footer">
        <div class="container">
            <div class="row">
                <!-- ADDRESS -->
                <div class="col-md-4">
                    <div class="widget__footer">
                        <figure>
                            <img src="{{ asset('modules/frontend/img/logo-blue.png') }}" alt="" class="logo-footer">
                        </figure>
                        <p>
                            Kantor Pusat bank bjb
                            Menara bank bjb
                        </p>
                        <ul class="list-unstyled mb-0 mt-3">
                            <li> <b> <i class="fa fa-map-marker"></i></b><span>Jl.Naripan No. 12-14
                                    Bandung - 40111</span> </li>
                            <li> <b><i class="fa fa-phone-square"></i></b><span>Telp : 022-4234868</span> </li>
                            <li> <b><i class="fa fa-phone-square"></i></b><span>Fax : 022-4206099</span> </li>
                            <li> <b><i class="fa fa-headphones"></i></b><span>bjbcare@bankbjb.co.id</span> </li>
                        </ul>
                    </div>
                </div>
                <!-- END ADDRESS -->
                <!-- NEWSLETTERS -->
                <div class="col-md-4">
                    <div class="widget__footer">
                        <h4 class="footer-title">follow us </h4>
                        <p class="mb-2">
                            Follow us and stay in touch to get the latest news
                        </p>
                        <p>
                            <button class="btn btn-social btn-social-o facebook mr-1">
                                <i class="fa fa-facebook-f"></i>
                            </button>
                            <button class="btn btn-social btn-social-o twitter mr-1">
                                <i class="fa fa-twitter"></i>
                            </button>
                            <button class="btn btn-social btn-social-o linkedin mr-1">
                                <i class="fa fa-linkedin"></i>
                            </button>
                            <button class="btn btn-social btn-social-o instagram mr-1">
                                <i class="fa fa-instagram"></i>
                            </button>
                            <button class="btn btn-social btn-social-o youtube mr-1">
                                <i class="fa fa-youtube"></i>
                            </button>
                        </p>
                        <br>
                        <!--<h4 class="footer-title">newsletter</h4>
     Form Newsletter -->
                        <div class="widget__form-newsletter ">
                        </div>
                    </div>
                </div>
            </div>
            <!-- END NEWSLETTER -->
        </div>
    </div>
    <!-- Footer Bottom -->
    <div class="bg__footer-bottom-v1">
        <div class="container">
            <div class="row flex-column-reverse flex-md-row">
                <div class="col-md-6">
                    <span>
                        © 2020 Div PPK bjb
                        <a href="#">bjb</a>
                    </span>
                </div>
                <div class="col-md-6">
                    <ul class="list-inline ">
                        <li class="list-inline-item">
                            <a href="#">
                                disclaimer
                            </a>
                        </li>
                        <!--<li class="list-inline-item">
     <a href="#">
         contact
     </a>-->
                        <!--</li>-->
                        <li class="list-inline-item">
                            <a href="tentangkami.html">
                                about
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                faq
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer  -->
</footer>
