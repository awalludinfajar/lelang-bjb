<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Lelang bank bjb">
    <meta name="keywords" content="Lelang bank bjb" />
    <meta name="author" content="SKD">
    <title>Lelang bank bjb</title>
    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />
    <link rel="manifest" href="{{ asset('modules/frontend/site.webmanifest') }}">
    <!-- favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="blue.png">
    <meta name="theme-color" content="#3454d1">
    <link href="{{ asset('modules/frontend/css/styles.css?fd365619e86ad9137a29') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  </head>
  <body>

    <header class="bg-theme">
      @include('layouts.header')
      @include('layouts.search')
      @include('layouts.banner')
    </header>

    @yield('content_first')

    @include('layouts.footer')

    <!-- SCROLL TO TOP -->
    <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TO TOP -->

    <script src="{{ asset('modules/frontend/js/index.bundle.js') }}"></script>
    <script src="{{ asset('modules/frontend/js/custom.js') }}"></script>
    <script src="{{asset('js/sweetalert/sweetalert2.all.min.js')}}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $.noConflict();  //Not to conflict with other scripts
      jQuery(document).ready(function ($) {
        $("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#datepicker2").datepicker({ dateFormat: 'yy-mm-dd' });

        // select2
        $("#carajual").select2();
        $("#ktaa").select2({ placeholder: "- Kabupaten Kota -" });
        $("#ktaa").prop("disabled", true);

        $('#alldata').parent().parent().hide();

        function formatRupiah(angka, prefix){
          let ang = angka == NaN ? 0 : angka;
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
              split   		= number_string.split(','),
              sisa     		= split[0].length % 3,
              rupiah     	= split[0].substr(0, sisa),
              ribuan     	= split[0].substr(sisa).match(/\d{3}/gi);

          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }

          rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        // menu nav
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_menufr') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            $("#main_nav99").html(res['data']);
          }
        });

        // image slider
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_banner') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let slide = ""; let poi = ""; var srt = [];
            for (var i = 0; i < res.length; i++) { srt.push(res[i]['slide']); }
            srt.sort(function (a, b) { return a - b; });
            for (var i = 0; i < res.length; i++) {
              if (res[i]['slide'] == srt[0]) {
                poi = poi+'<li data-target="#carouselExampleIndicators" data-slide-to="'+res[i]['slide']+'" class="active"></li>';
                slide = slide+'<div class="carousel-item active">'
                +'<img class="d-block w-100 img-fluid img-transition" src="{{ asset('/upload/banner/img') }}/'+res[i]['image']+'" alt="Third slide">'
                +'</div>';
              } else {
                poi = poi+'<li data-target="#carouselExampleIndicators" data-slide-to="'+res[i]['slide']+'"></li>';
                slide = slide+'<div class="carousel-item">'
                +'<img class="d-block w-100 img-fluid img-transition" src="{{ asset('/upload/banner/img') }}/'+res[i]['image']+'" alt="Third slide">'
                +'</div>';
              }
            }
            $('#dicator').html(poi);
            $('#setslide').html(slide);
          }
        });

        // duration slide
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_duration_banner') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            $("#carouselExampleControls").attr('data-interval', res['duration']);
          }
        });

        // jeniskepemilikan
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_agunan_kategori') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              if (res[i]['id'] == 1 || res[i]['id'] == 5) {
                isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['jenis_kepemilikan']+'</option>';
              }
            }
            $("#jnskp").html(isi);
            $("#jnskp").select2({ placeholder: "- Jenis Kepemilikan -" });
          }
        });

        // Kategori
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_kategori_agunan') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama_kategori']+'</option>';
            }
            $("#katgr").html(isi);
            $("#katgr").select2({ placeholder: "- Kategori Agunan -" });
          }
        });

        // KisaranHarga
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_kisaran_harga') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi1 = '<option></option>';
            let isi2 = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              isi1 = isi1+'<option value="'+res[i]['id']+'">'+res[i]['min_harga']+'</option>';
              isi2 = isi2+'<option value="'+res[i]['id']+'">'+res[i]['max_harga']+'</option>';
            }
            $("#harmin").html(isi1);
            $("#harmax").html(isi2);

            $("#harmin").select2({ placeholder: "- Harga Minimum -" });
            $("#harmax").select2({ placeholder: "- Harga Maksimum -" });
          }
        });

        // data_provinsi
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_provinsi') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
            }
            $("#provin").html(isi);
            $("#provin").select2({ placeholder: "- Provinsi -" });
          }
        });

        // jual sukarela
        $.ajax({
          type:"GET",
          url : "{{ url('api/agunan/list/jualsukarela') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<div class="featured__property-carousel owl-carousel owl-theme">';
            for (var i = 0; i < res.length; i++) {
              let hashas = res[i]['jenis_jual'] == 1?'LELANG SEGERA':'JUAL SUKARELA';
              isi = isi+'<div class="item">'+
                          '<div class="card__image card__box" onclick="detail('+res[i]['id_ang']+')">'+
                            '<div class="card__image-header h-250">'+
                              '<div class="ribbon text-capitalize">DIJUAL</div>'+
                              '<img src="{{ asset("assets/img/agunan/") }}/'+res[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                              '<div class="info">'+hashas+'</div>'+
                            '</div>'+
                            '<div class="card__image-body">'+
                              '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+res[i]['kode_agunan']+'</span>'+
                              '<h6 class="text-capitalize">'+res[i]['judul_agunan']+'</h6>'+
                              '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+res[i]['kecamatan']+', '+res[i]['kabkot']+'</p>'+
                              '<ul class="list-inline card__content">'+
                                '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+res[i]['jumlah_kamar_mandi']+'</span></li>'+
                                '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+res[i]['jumlah_kamar_tidur']+'</span></li>'+
                                '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+res[i]['jumlah_lantai']+'</span></li>'+
                                '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i>'+res[i]['luas_bangunan']+' ㎡</span></li>'+
                              '</ul>'+
                            '</div>'+
                            '<div class="card__image-footer">'+
                              '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                              '<ul class="list-inline my-auto">'+
                                '<li class="list-inline-item">'+
                                  '<a href="single-detail-v1.html">Div PPK bjb<br></a>'+
                                '</li>'+
                              '</ul>'+
                              '<ul class="list-inline my-auto ml-auto">'+
                                '<li class="list-inline-item "><h6>Rp '+formatRupiah(res[i]['harga'].toString())+'</h6></li>'+
                              '</ul>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
            }
            isi = isi+"</div>";
            $('#sukarela').html(isi);
            $('.owl-carousel').owlCarousel({
              autoplay:true,
              loop:true,
              autoplayTimeout:3000,
              autoplayHoverPause:true
            });
          }
        });

        // lelang segera
        $.ajax({
          type:"GET",
          url : "{{ url('api/agunan/list/lelangsegera') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<div class="featured__property-carousel owl-carousel owl-theme">';
            for (var i = 0; i < res.length; i++) {
              let hashas = res[i]['jenis_jual'] == 1?'LELANG SEGERA':'JUAL SUKARELA';
              isi = isi+'<div class="item">'+
                          '<div class="card__image card__box" onclick="detail('+res[i]['id_ang']+')">'+
                            '<div class="card__image-header h-250">'+
                              '<div class="ribbon text-capitalize">DIJUAL</div>'+
                              '<img src="{{ asset("assets/img/agunan/") }}/'+res[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                              '<div class="info">'+hashas+'</div>'+
                            '</div>'+
                            '<div class="card__image-body">'+
                              '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+res[i]['kode_agunan']+'</span>'+
                              '<h6 class="text-capitalize">'+res[i]['judul_agunan']+'</h6>'+
                              '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+res[i]['kecamatan']+', '+res[i]['kabkot']+'</p>'+
                              '<ul class="list-inline card__content">'+
                                '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+res[i]['jumlah_kamar_mandi']+'</span></li>'+
                                '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+res[i]['jumlah_kamar_tidur']+'</span></li>'+
                                '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+res[i]['jumlah_lantai']+'</span></li>'+
                                '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i>'+res[i]['luas_bangunan']+' ㎡</span></li>'+
                              '</ul>'+
                            '</div>'+
                            '<div class="card__image-footer">'+
                              '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                              '<ul class="list-inline my-auto">'+
                                '<li class="list-inline-item">'+
                                  '<a href="single-detail-v1.html">Div PPK bjb<br></a>'+
                                '</li>'+
                              '</ul>'+
                              '<ul class="list-inline my-auto ml-auto">'+
                                '<li class="list-inline-item "><h6>Rp '+formatRupiah(res[i]['harga'].toString())+'</h6></li>'+
                              '</ul>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
            }
            isi = isi+"</div>";
            $('#segera').html(isi);
            $('.owl-carousel').owlCarousel({
              autoplay:true,
              loop:true,
              autoplayTimeout:3000,
              autoplayHoverPause:true
            });
          }
        });

        // hotprice
        $.ajax({
          type:"GET",
          url : "{{ url('api/agunan/list/hotprice') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<div class="featured__property-carousel owl-carousel owl-theme">';
            for (var i = 0; i < res.length; i++) {
              let hashas = res[i]['jenis_jual'] == 1?'LELANG SEGERA':'JUAL SUKARELA';
              isi = isi+'<div class="item">'+
                          '<div class="card__image card__box" onclick="detail('+res[i]['id_ang']+')">'+
                            '<div class="card__image-header h-250">'+
                              '<div class="ribbon text-capitalize">HOT PRICE</div>'+
                              '<img src="{{ asset("assets/img/agunan/") }}/'+res[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                              '<div class="info">'+hashas+'</div>'+
                            '</div>'+
                            '<div class="card__image-body">'+
                              '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+res[i]['kode_agunan']+'</span>'+
                              '<h6 class="text-capitalize">'+res[i]['judul_agunan']+'</h6>'+
                              '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+res[i]['kecamatan']+', '+res[i]['kabkot']+'</p>'+
                              '<ul class="list-inline card__content">'+
                                '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+res[i]['jumlah_kamar_mandi']+'</span></li>'+
                                '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+res[i]['jumlah_kamar_tidur']+'</span></li>'+
                                '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+res[i]['jumlah_lantai']+'</span></li>'+
                                '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i>'+res[i]['luas_bangunan']+' ㎡</span></li>'+
                              '</ul>'+
                            '</div>'+
                            '<div class="card__image-footer">'+
                              '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                              '<ul class="list-inline my-auto">'+
                                '<li class="list-inline-item">'+
                                  '<a href="single-detail-v1.html">Div PPK bjb<br></a>'+
                                '</li>'+
                              '</ul>'+
                              '<ul class="list-inline my-auto ml-auto">'+
                                '<li class="list-inline-item "><h6>Rp '+formatRupiah(res[i]['harga'].toString())+'</h6></li>'+
                              '</ul>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
            }
            isi = isi+"</div>";
            $('#pricehot').html(isi);
            $('.owl-carousel').owlCarousel({
              autoplay:true,
              loop:true,
              autoplayTimeout:3000,
              autoplayHoverPause:true
            });
          }
        });

        // terbaru
        $.ajax({
          type:"GET",
          url : "{{ url('api/agunan/list/terbaru') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<div class="featured__property-carousel owl-carousel owl-theme">';
            for (var i = 0; i < res.length; i++) {
              let hashas = res[i]['jenis_jual'] == 1?'LELANG SEGERA':'JUAL SUKARELA';
              isi = isi+'<div class="item">'+
                          '<div class="card__image card__box" onclick="detail('+res[i]['id_ang']+')">'+
                            '<div class="card__image-header h-250">'+
                              '<div class="ribbon text-capitalize">DIJUAL</div>'+
                              '<img src="{{ asset("assets/img/agunan/") }}/'+res[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                              '<div class="info">'+hashas+'</div>'+
                            '</div>'+
                            '<div class="card__image-body">'+
                              '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+res[i]['kode_agunan']+'</span>'+
                              '<h6 class="text-capitalize">'+res[i]['judul_agunan']+'</h6>'+
                              '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+res[i]['kecamatan']+', '+res[i]['kabkot']+'</p>'+
                              '<ul class="list-inline card__content">'+
                                '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+res[i]['jumlah_kamar_mandi']+'</span></li>'+
                                '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+res[i]['jumlah_kamar_tidur']+'</span></li>'+
                                '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+res[i]['jumlah_lantai']+'</span></li>'+
                                '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i>'+res[i]['luas_bangunan']+' ㎡</span></li>'+
                              '</ul>'+
                            '</div>'+
                            '<div class="card__image-footer">'+
                              '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                              '<ul class="list-inline my-auto">'+
                                '<li class="list-inline-item">'+
                                  '<a href="single-detail-v1.html">Div PPK bjb<br></a>'+
                                '</li>'+
                              '</ul>'+
                              '<ul class="list-inline my-auto ml-auto">'+
                                '<li class="list-inline-item "><h6>Rp '+formatRupiah(res[i]['harga'].toString())+'</h6></li>'+
                              '</ul>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
            }
            isi = isi+"</div>";
            $('#newst').html(isi);
            $('.owl-carousel').owlCarousel({
              autoplay:true,
              loop:true,
              autoplayTimeout:3000,
              autoplayHoverPause:true
            });
          }
        });

        $('#provin').change(function () {
          $.ajax({
            type:"GET",
            url : "{{ url('api/api_data_kabkot') }}/"+$(this).val(),
            contentType:"application/json",
            dataType: "json",
            success:function(res) {
              let isi = '<option></option>';
              for (var i = 0; i < res.length; i++) {
                isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
              }
              $("#ktaa").html(isi);
              $("#ktaa").select2({ placeholder: "- Kabupaten Kota -" });
              $("#ktaa").prop("disabled", false);
            }
          });
        });

        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_blogsdanberita') }}/x/"+0,
          contentType:"application/json",
          dataType: "json",
          success:function (res) {
            let isi = "";
            for (var i = 0; i < 3; i++) {
              var desc = res[i]['description'];
              isi = isi+'<div class="col-md-4">'+
                          '<div class="card__image">'+
                            '<div class="card__image-header h-250">'+
                              '<img src="{{ asset("upload/blogs/img/") }}/'+res[i]['image']+'" alt="" class="img-fluid w100 img-transition">'+
                              '<div class="info">'+res[i]['jenis_berita']+'</div>'+
                            '</div>'+
                            '<div class="card__image-body">'+
                              '<h6 class="text-capitalize"><a href="blog-single.html">'+res[i]['judul']+'</a></h6>'+
                              '<div class="mb-0">'+desc.substr(0,350)+'...</div>'+
                            '</div>'+
                            '<div class="card__image-footer">'+
                              '<figure>'+
                                '<img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle">'+
                              '</figure>'+
                              '<ul class="list-inline my-auto">'+
                                '<li class="list-inline-item "><a href="#">Div PPK bjb</a></li>'+
                              '</ul>'+
                              '<ul class="list-inline my-auto ml-auto">'+
                                '<li class="list-inline-item ">'+
                                  '<a href="{{ url("baca/berita") }}/'+res[i]['judul']+'/'+res[i]['id']+'" class="btn btn-sm btn-primary ">'+
                                    '<small class="text-white ">read more <i class="fa fa-angle-right ml-1"></i></small>'+
                                  '</a>'+
                                '</li>'+
                              '</ul>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
            }
            $('.list-newsn').html(isi);
          }
        });

      });

      $('#resetsend').click(function() {
        location.reload();
      });

      $("#ceksend").click(function () {
        var cek = '';
        if ($("#checkbox-3").is(':checked')) {
          cek = 'Yes';
        } else {
          cek = 'No';
        }

        var ar_filter = {
          katgr         : $("#katgr").val(),
          provin        : $("#provin").val(),
          ktaa          : $("#ktaa").val(),
          jnskp         : $("#jnskp").val(),
          harmin        : $("#harmin").val(),
          harmax        : $("#harmax").val(),
          datepicker    : $("#datepicker").val(),
          datepicker2   : $("#datepicker2").val(),
          checkbox3     : cek,
          carajual      : $("#carajual").val(),
          periodelelang : $("#periodelelang").val(),
          luas_tanah    : $("#luas_tanah").val(),
          luas_bangunan : $("#luas_bangunan").val()
        }

        $.ajax({
          type:"POST",
          url : "{{ url('api/agunan/filter_agunan') }}",
          contentType:"application/json",
          dataType: "json",
          data: JSON.stringify(ar_filter),
          beforeSend: function(){
            Swal.fire({
              title: 'Wait ...',
              onBeforeOpen () { Swal.showLoading () },
              onAfterClose () { Swal.hideLoading() },
              allowOutsideClick: false,
              allowEscapeKey: false,
              allowEnterKey: false
            })
          },
          success:function(data) {
            Swal.close();

            Swal.fire({
              title: 'Berhasil!',
              icon: 'success',
              timer: 1000
            });

            if ($("#carajual").val() == '0') {
              $('#alldata').parent().parent().hide();
              $('#segera').parent().parent().hide();
              $('#sukarela').parent().parent().show();
              $('#sukarela').html('');
              var lst = '<div class="row">';
              for (var i = 0; i < data.length; i++) {
                if (data[i]['jenis_jual'] == $("#carajual").val()) {
                  let jenis_j = data[i]['jenis_jual'];
                  var lst = lst+'<div class="item col-md-4">'+
                      '<div class="card__image card__box" onclick="detail('+data[i]['id_ang']+')">'+
                        '<div class="card__image-header h-250">'+
                          '<div class="ribbon text-capitalize">Dijual</div>'+
                          '<img src="{{ asset("assets/img/agunan/") }}/'+data[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                          '<div class="info">'+(jenis_j=='1'?'LELANG':'SUKARELA')+'</div>'+
                        '</div>'+
                        '<div class="card__image-body">'+
                          '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+data[i]['kode_agunan']+'</span>'+
                          '<h6 class="text-capitalize">'+data[i]['judul_agunan']+'</h6>'+
                          '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+data[i]['kecamatan']+', '+data[i]['kabkot']+'</p>'+
                          '<ul class="list-inline card__content">'+
                            '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+data[i]['jumlah_kamar_mandi']+'</span></li>'+
                            '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+data[i]['jumlah_kamar_tidur']+'</span></li>'+
                            '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+data[i]['jumlah_lantai']+'</span></li>'+
                            '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i> '+data[i]['luas_bangunan']+' ㎡</span></li>'+
                          '</ul>'+
                        '</div>'+
                        '<div class="card__image-footer">'+
                          '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                          '<ul class="list-inline my-auto">'+
                            '<li class="list-inline-item"><a href="single-detail-v1.html">Div PPK bjb</a></li>'+
                          '</ul>'+
                          '<ul class="list-inline my-auto ml-auto">'+
                            '<li class="list-inline-item"><h6>'+data[i]['harga']+' Milyar</h6></li>'+
                          '</ul>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
                }
              }
              lst = lst+'</div>';
              $('#sukarela').html(lst);
              $('#pricehot').parent().parent().css('margin-top', '-150px');
            }

            if ($("#carajual").val() == '1') {
              $('#alldata').parent().parent().hide();
              $('#sukarela').parent().parent().hide();
              $('#segera').parent().parent().show();
              $('#segera').html('');
              var lst = '<div class="row">';
              for (var i = 0; i < data.length; i++) {
                if (data[i]['jenis_jual'] == $("#carajual").val()) {
                  let jenis_j = data[i]['jenis_jual'];
                  var lst = lst+'<div class="item col-md-4">'+
                      '<div class="card__image card__box" onclick="detail('+data[i]['id_ang']+')">'+
                        '<div class="card__image-header h-250">'+
                          '<div class="ribbon text-capitalize">Dijual</div>'+
                          '<img src="{{ asset("assets/img/agunan/") }}/'+data[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                          '<div class="info">'+(jenis_j=='1'?'LELANG':'SUKARELA')+'</div>'+
                        '</div>'+
                        '<div class="card__image-body">'+
                          '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+data[i]['kode_agunan']+'</span>'+
                          '<h6 class="text-capitalize">'+data[i]['judul_agunan']+'</h6>'+
                          '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+data[i]['kecamatan']+', '+data[i]['kabkot']+'</p>'+
                          '<ul class="list-inline card__content">'+
                            '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+data[i]['jumlah_kamar_mandi']+'</span></li>'+
                            '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+data[i]['jumlah_kamar_tidur']+'</span></li>'+
                            '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+data[i]['jumlah_lantai']+'</span></li>'+
                            '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i> '+data[i]['luas_bangunan']+' ㎡</span></li>'+
                          '</ul>'+
                        '</div>'+
                        '<div class="card__image-footer">'+
                          '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                          '<ul class="list-inline my-auto">'+
                            '<li class="list-inline-item"><a href="single-detail-v1.html">Div PPK bjb</a></li>'+
                          '</ul>'+
                          '<ul class="list-inline my-auto ml-auto">'+
                            '<li class="list-inline-item"><h6>'+data[i]['harga']+' Milyar</h6></li>'+
                          '</ul>'+
                        '</div>'+
                      '</div>'+
                    '</div>';
                }
              }
              lst = lst+'</div>';
              $('#segera').html(lst);
              $('#segera').parent().parent().css('margin-top', '-150px');
            }

            if ($("#carajual").val() == '') {
              $('#sukarela').parent().parent().hide();
              $('#segera').parent().parent().hide();
              $('#pricehot').parent().parent().hide();
              $('#newst').parent().parent().hide();
              $('#alldata').parent().parent().show();
              $('#alldata').html('');
              var lst = '<div class="row">';
              for (var i = 0; i < data.length; i++) {
                let jenis_j = data[i]['jenis_jual'];
                var lst = lst+'<div class="item col-md-4">'+
                    '<div class="card__image card__box" onclick="detail('+data[i]['id_ang']+')">'+
                      '<div class="card__image-header h-250">'+
                        '<div class="ribbon text-capitalize">Dijual</div>'+
                        '<img src="{{ asset("assets/img/agunan/") }}/'+data[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                        '<div class="info">'+(jenis_j=='1'?'LELANG':'SUKARELA')+'</div>'+
                      '</div>'+
                      '<div class="card__image-body">'+
                        '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+data[i]['kode_agunan']+'</span>'+
                        '<h6 class="text-capitalize">'+data[i]['judul_agunan']+'</h6>'+
                        '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+data[i]['kecamatan']+', '+data[i]['kabkot']+'</p>'+
                        '<ul class="list-inline card__content">'+
                          '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+data[i]['jumlah_kamar_mandi']+'</span></li>'+
                          '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+data[i]['jumlah_kamar_tidur']+'</span></li>'+
                          '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+data[i]['jumlah_lantai']+'</span></li>'+
                          '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i> '+data[i]['luas_bangunan']+' ㎡</span></li>'+
                        '</ul>'+
                      '</div>'+
                      '<div class="card__image-footer">'+
                        '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                        '<ul class="list-inline my-auto">'+
                          '<li class="list-inline-item"><a href="single-detail-v1.html">Div PPK bjb</a></li>'+
                        '</ul>'+
                        '<ul class="list-inline my-auto ml-auto">'+
                          '<li class="list-inline-item"><h6>'+data[i]['harga']+' Milyar</h6></li>'+
                        '</ul>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
              }
              lst = lst+'</div>';
              $('#alldata').html(lst);
              $('#alldata').parent().parent().css('margin-top', '-600px');
            }

            if ($("#checkbox-3").is(':checked')) {
              $('#pricehot').html('');
              var lst = '<div class="row">';
              for (var i = 0; i < data.length; i++) {
                if (data[i]['hot_price'] == $("#checkbox-3").val()) {
                let jenis_j = data[i]['jenis_jual'];
                var lst = lst+'<div class="item col-md-4">'+
                    '<div class="card__image card__box" onclick="detail('+data[i]['id_ang']+')">'+
                      '<div class="card__image-header h-250">'+
                        '<div class="ribbon text-capitalize">HOT PRICE</div>'+
                        '<img src="{{ asset("assets/img/agunan/") }}/'+data[i]['image']+'" alt="single-detail-v1.html" class="img-fluid w100 img-transition">'+
                        '<div class="info">'+(jenis_j=='1'?'LELANG':'SUKARELA')+'</div>'+
                      '</div>'+
                      '<div class="card__image-body">'+
                        '<span class="badge badge-primary text-capitalize mb-2">Kode agunan '+data[i]['kode_agunan']+'</span>'+
                        '<h6 class="text-capitalize">'+data[i]['judul_agunan']+'</h6>'+
                        '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+data[i]['kelurahan']+', Awiligar</p>'+
                        '<ul class="list-inline card__content">'+
                          '<li class="list-inline-item"><span>KM<br><i class="fa fa-bath"></i>'+data[i]['jumlah_kamar_mandi']+'</span></li>'+
                          '<li class="list-inline-item"><span>KT<br><i class="fa fa-bed"></i>'+data[i]['jumlah_kamar_tidur']+'</span></li>'+
                          '<li class="list-inline-item"><span>Ruangan<br><i class="fa fa-inbox"></i>'+data[i]['jumlah_lantai']+'</span></li>'+
                          '<li class="list-inline-item"><span>Luas<br><i class="fa fa-map"></i> '+data[i]['luas_bangunan']+' ㎡</span></li>'+
                        '</ul>'+
                      '</div>'+
                      '<div class="card__image-footer">'+
                        '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="single-detail-v1.html" class="img-fluid rounded-circle"></figure>'+
                        '<ul class="list-inline my-auto">'+
                          '<li class="list-inline-item"><a href="single-detail-v1.html">Div PPK bjb</a></li>'+
                        '</ul>'+
                        '<ul class="list-inline my-auto ml-auto">'+
                          '<li class="list-inline-item"><h6>'+data[i]['harga']+' Milyar</h6></li>'+
                        '</ul>'+
                      '</div>'+
                    '</div>'+
                  '</div>'
                }
              }
              lst = lst+'</div>';
              $('#pricehot').html(lst);
            }
          }
        });
      });
    </script>
  </body>
</html>
