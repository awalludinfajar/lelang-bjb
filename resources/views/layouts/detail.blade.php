<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Lelang bank bjb">
    <meta name="keywords" content="Lelang bank bjb" />
    <meta name="author" content="SKD">
    <title>Lelang bank bjb</title>
    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />
    <link rel="manifest" href="{{ asset('modules/frontend/site.webmanifest') }}">
    <!-- favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="blue.png">
    <meta name="theme-color" content="#3454d1">
    <link href="{{ asset('modules/frontend/css/styles.css?fd365619e86ad9137a29') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  </head>
  <body>

    <header>
      @include('layouts.header')
    </header>
    <div class="clearfix"></div>

    @yield('content_detail')
    @include('layouts.footer')

    <!-- SCROLL TO TOP -->
    <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TO TOP -->

    <script src="{{ asset('modules/frontend/js/index.bundle.js') }}"></script>
    <script src="{{ asset('modules/frontend/js/custom.js') }}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script type="text/javascript">
      $.noConflict();  //Not to conflict with other scripts
      jQuery(document).ready(function ($) {
        $("#datepicker").datepicker();
        $("#datepicker2").datepicker();

        $('.conditionhere').hide();

        // select2
        $("#carajual").select2({ placeholder: "- Cara Jual -" });
        $("#ktaa").select2({ placeholder: "- Kabupaten Kota -" });
        $("#ktaa").prop("disabled", true);

        // menu nav
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_menufr') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            $("#main_nav99").html(res['data']);
          }
        });

        // jeniskepemilikan
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_agunan_kategori') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              if (res[i]['id'] == 1 || res[i]['id'] == 5) {
                isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['jenis_kepemilikan']+'</option>';
              }
            }
            $("#jnskp").html(isi);
            $("#jnskp").select2({ placeholder: "- Jenis Kepemilikan -" });
          }
        });

        // Kategori
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_kategori_agunan') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama_kategori']+'</option>';
            }
            $("#katgr").html(isi);
            $("#katgr").select2({ placeholder: "- Kategori Agunan -" });
          }
        });

        // KisaranHarga
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_kisaran_harga') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi1 = '<option></option>';
            let isi2 = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              isi1 = isi1+'<option value="'+res[i]['id']+'">'+res[i]['min_harga']+'</option>';
              isi2 = isi2+'<option value="'+res[i]['id']+'">'+res[i]['max_harga']+'</option>';
            }
            $("#harmin").html(isi1);
            $("#harmax").html(isi2);

            $("#harmin").select2({ placeholder: "- Harga Minimum  -" });
            $("#harmax").select2({ placeholder: "- Harga Maksimum -" });
          }
        });

        // data_provinsi
        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_provinsi') }}",
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = '<option></option>';
            for (var i = 0; i < res.length; i++) {
              isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
            }
            $("#provin").html(isi);
            $("#provin").select2({ placeholder: "- Provinsi -" });
          }
        });

        $('#provin').change(function () {
          $.ajax({
            type:"GET",
            url : "{{ url('api/api_data_kabkot') }}/"+$(this).val(),
            contentType:"application/json",
            dataType: "json",
            success:function(res) {
              let isi = '<option></option>';
              for (var i = 0; i < res.length; i++) {
                isi = isi+'<option value="'+res[i]['id']+'">'+res[i]['nama']+'</option>';
              }
              $("#ktaa").html(isi);
              $("#ktaa").select2({ placeholder: "- Kabupaten Kota -" });
              $("#ktaa").prop("disabled", false);
            }
          });
        });

        $.ajax({
          type:"GET",
          url : "{{ url('api/api_data_blogsdanberita') }}/x/"+0,
          contentType:"application/json",
          dataType: "json",
          success:function(res) {
            let isi = "";
            for (var i = 0; i < res.length; i++) {
              let dte = res[i]['created_at'].split('T');
              let sse = dte[0].split('-');
              isi = isi+'<div class="widget__sidebar__body-img">'+
                          '<img src="{{ asset("upload/blogs/img/") }}/'+res[i]['image']+'" alt="" class="img-fluid" width="80px" height="80px">'+
                          '<div class="widget__sidebar__body-heading">'+
                              '<h6 class="text-capitalize">'+res[i]['judul']+'</h6>'+
                          '</div>'+
                          '<span class="badge badge-secondary p-1 text-capitalize mb-1">'+sse[2]+'/'+sse[1]+'/'+sse[0]+'</span>'+
                        '</div>';
            }
            $('.widg_news').html(isi);
          }
        });

        $('#resetsend').click(function() {
          location.reload();
        });

        $("#ceksend").click(function () {
          var cek = '';
          if ($("#checkbox-3").is(':checked')) {
            cek = 'Yes';
          } else {
            cek = 'No';
          }

          var ar_filter = {
            katgr         : $("#idjeniss").val(),
            provin        : $("#provin").val(),
            ktaa          : $("#ktaa").val(),
            jnskp         : $("#jnskp").val(),
            harmin        : $("#harmin").val(),
            harmax        : $("#harmax").val(),
            datepicker    : $("#datepicker").val(),
            datepicker2   : $("#datepicker2").val(),
            checkbox3     : cek,
            carajual      : $("#carajual").val(),
            periodelelang : $("#periodelelang").val(),
            luas_tanah    : $("#luas_tanah").val(),
            luas_bangunan : $("#luas_bangunan").val()
          }

          $.ajax({
            type:"POST",
            url : "{{ url('api/agunan/filter_agunan') }}",
            contentType:"application/json",
            dataType: "json",
            data: JSON.stringify(ar_filter),
            beforeSend: function(){
              Swal.fire({
                title: 'Wait ...',
                onBeforeOpen () { Swal.showLoading () },
                onAfterClose () { Swal.hideLoading() },
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false
              })
            },
            success:function(res) {
              Swal.close();

              Swal.fire({
                title: 'Berhasil!',
                icon: 'success',
                timer: 1000
              });
              let hrz = ""; let vrt = '<div class="row">';
              for (var i = 0; i < res.length; i++) {
                let hashas = res[i]['jenis_jual'] == 1?'LELANG SEGERA':'JUAL SUKARELA';
                let htp = res[i]['hot_price']=='Yes'?'HOT PRICE':'DIJUAL';
                hrz = hrz+'<div class="row">'+
                            '<div class="col-lg-12">'+
                              '<div class="card__image card__box-v1">'+
                                '<div class="row no-gutters">'+
                                  '<div class="col-md-4 col-lg-3 col-xl-4">'+
                                    '<div class="card__image__header h-250">'+
                                      '<a href="#">'+
                                        '<div class="ribbon text-capitalize">'+htp+'</div>'+
                                        '<img src="{{ asset("assets/img/agunan/") }}/'+res[i]['image']+'" alt="" class="img-fluid w100 img-transition">'+
                                        '<div class="info">'+hashas+'</div>'+
                                      '</a>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="col-md-4 col-lg-6 col-xl-5 my-auto">'+
                                    '<div class="card__image__body">'+
                                      '<span class="badge badge-primary text-capitalize mb-2">'+res[i]['agunan']+' Kode Lelang '+res[i]['kode_agunan']+'</span>'+
                                      '<h6><a href="#">'+res[i]['judul_agunan']+'</a></h6>'+
                                      '<div class="card__image__body-desc">'+
                                        '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+res[i]['kabkot']+', '+res[i]['kecamatan']+'</p>'+
                                      '</div>'+
                                      '<ul class="list-inline card__content">'+
                                        '<li class="list-inline-item"><span>KM <br><i class="fa fa-bath"></i>'+res[i]['jumlah_kamar_mandi']+'</span></li>'+
                                        '<li class="list-inline-item"><span>KT <br><i class="fa fa-bed"></i>'+res[i]['jumlah_kamar_tidur']+'</span></li>'+
                                        '<li class="list-inline-item"><span>Luas Bangunan <br><i class="fa fa-inbox"></i>'+res[i]['luas_bangunan']+'㎡</span></li>'+
                                        '<li class="list-inline-item"><span>Luas Tanah <br><i class="fa fa-map"></i>'+res[i]['luas_tanah']+'㎡</span></li>'+
                                      '</ul>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">'+
                                    '<div class="card__image__footer">'+
                                      '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="" class="img-fluid rounded-circle"></figure>'+
                                      '<ul class="list-inline my-auto">'+
                                        '<li class="list-inline-item name"><a href="#">Div PPK bjb</a></li>'+
                                      '</ul>'+
                                      '<ul class="list-inline my-auto ml-auto price">'+
                                        '<li class="list-inline-item "><h6>Call</h6></li>'+
                                      '</ul>'+
                                    '</div>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>';

                vrt = vrt+'<div class="col-md-4 col-lg-4">'+
                            '<div class="card__image card__box-v1">'+
                              '<div class="card__image-header h-250">'+
                                '<div class="ribbon text-capitalize">'+htp+'</div>'+
                                '<img src="{{ asset("assets/img/agunan/") }}/'+res[i]['image']+'" alt="" class="img-fluid w100 img-transition">'+
                                '<div class="info">'+hashas+'</div>'+
                              '</div>'+
                              '<div class="card__image-body">'+
                                '<span class="badge badge-primary text-capitalize mb-2">'+res[i]['agunan']+' Kode Lelang '+res[i]['kode_agunan']+'</span>'+
                                '<h6 class="text-capitalize">'+res[i]['judul_agunan']+'</h6>'+
                                '<p class="text-capitalize"><i class="fa fa-map-marker"></i>'+res[i]['kabkot']+', '+res[i]['kecamatan']+'</p>'+
                                '<ul class="list-inline card__content">'+
                                '<li class="list-inline-item"><span>KM <br><i class="fa fa-bath"></i>'+res[i]['jumlah_kamar_mandi']+'</span></li>'+
                                '<li class="list-inline-item"><span>KT <br><i class="fa fa-bed"></i>'+res[i]['jumlah_kamar_tidur']+'</span></li>'+
                                '<li class="list-inline-item"><span>Luas Bangunan <br><i class="fa fa-inbox"></i>'+res[i]['luas_bangunan']+'㎡</span></li>'+
                                '<li class="list-inline-item"><span>Luas Tanah <br><i class="fa fa-map"></i>'+res[i]['luas_tanah']+'㎡</span></li>'+
                                '</ul>'+
                              '</div>'+
                              '<div class="card__image-footer">'+
                                '<figure><img src="{{ asset("modules/frontend/img/80x80.jpg") }}" alt="" class="img-fluid rounded-circle"></figure>'+
                                '<ul class="list-inline my-auto">'+
                                  '<li class="list-inline-item "><a href="#">Div PPK bjb</a></li>'+
                                '</ul>'+
                                '<ul class="list-inline my-auto ml-auto">'+
                                  '<li class="list-inline-item"><h6>Call</h6></li>'+
                                '</ul>'+
                              '</div>'+
                            '</div>'+
                          '</div>';
              }
              vrt = vrt+'</div>';
              $('#pills-tab-one').html(hrz);
              $('#pills-tab-two').html(vrt);
            }
          });
        });
      });
    </script>
  </body>
</html>
