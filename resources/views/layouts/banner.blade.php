<div id="carouselExampleControls" class="wrap__intro align-items-md-center carousel slide" data-ride="carousel">
  <ol class="carousel-indicators" id="dicator" style="margin-bottom: 150px;"></ol>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <div class="carousel-inner pic_set_img" id="setslide"></div>
</div>
<div class="container" style="margin-top: -170px;">
  <div class="row">
    <div class="col-lg-12">
      <div class="card-body">
        <div class="card">
          <div class="row align-items-center justify-content-start flex-wrap">
            <div class="col-md-10 mx-auto">
              <div class="text-center" style="margin: 15px;">
                <h2>Temukan property pilihan anda di bjb Lelang </h2>
                <hr>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="search__container" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                      <div class="row input-group no-gutters">
                        <div class="col-sm-12 col-md-5">
                          <input type="text" class="form-control" aria-label="Text input" placeholder="Cari . . . .">
                        </div>
                        <div class="col-sm-12 col-md-4 input-group">
                          <select class="select_option form-control" name="select" id="categories">
                            <option>Kategori</option>
                            <option>Jual Sukarela</option>
                            <option>Lelang Segera </option>
                          </select>
                        </div>
                        <div class="col-sm-12 col-md-3 input-group-append">
                          <button class="btn btn-primary btn-block" type="submit">
                            <i class="fa fa-search"></i> <span class="ml-1 text-uppercase">Cari</span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
