  <!-- NAVBAR -->
  <nav class="navbar navbar-hover navbar-expand-lg navbar-soft navbar-transparent">
    <div class="container">
      <a class="navbar-brand" href="index.html">
        <img src="{{ asset('modules/frontend/img/logo-blue.png') }}" alt="index.html">
        <img src="{{ asset('modules/frontend/img/logo-blue-stiky.png') }}" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav99">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="main_nav99">
      </div>
    </div>
  </nav>
  <!-- END NAVBAR -->
