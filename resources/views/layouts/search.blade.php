<!-- Search content bar.// -->
<div class="top-search navigation-shadow">
    <div class="container">
        <div class="input-group ">
            <form action="#">

                <div class="row no-gutters mt-3">
                    <div class="col">
                        <input class="form-control border-secondary border-right-0 rounded-0"
                            type="search" value="" placeholder="Search " id="example-search-input4">
                    </div>
                    <div class="col-auto">
                        <a class="btn btn-outline-secondary border-left-0 rounded-0 rounded-right"
                            href="/search-result.html">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- Search content bar.// -->
