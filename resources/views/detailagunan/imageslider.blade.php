<div class="slider__image__detail-large owl-carousel owl-theme">
  @foreach ($img as $key)
  <div class="item">
    <div class="slider__image__detail-large-one">
      <img src="{{ asset('assets/img/agunan/'.$key->image) }}" alt="" class="img-fluid w-100 img-transition">
      <div class="description">
        <figure><img src="{{ asset('modules/frontend/img/logo-putih.png') }}" alt="" class="img-fluid"></figure>
        <span class="badge badge-primary text-capitalize mb-2">{{ $key->nama_kategori }}</span>
        <!-- <div class="price"><h5 class="text-capitalize">Rp.500.000.000</h5></div> -->
        <h4 class="text-capitalize">{{ $key->judul_agunan }}</h4>
      </div>
    </div>
  </div>
  @endforeach
</div>
<div class="slider__image__detail-thumb owl-carousel owl-theme">
  @foreach ($img as $key)
  <div class="item">
    <div class="slider__image__detail-thumb-one">
      <img src="{{ asset('assets/img/agunan/'.$key->image) }}" alt="" class="img-fluid w-100 img-transition">
    </div>
  </div>
  @endforeach
</div>

<!-- judul dan harga -->
<div class="row">
  <div class="col-lg-7">
    <div class="single__detail-title mt-4">
      <h3 class="text-capitalize">{{ $list->judul_agunan }}</h3>
      <p> {{ $list->kecamatan }}, {{ $list->kabkot }}</p>
    </div>
  </div>
  <div class="col-lg-5">
    <div class="single__detail-price mt-4">
      <h4 class="text-capitalize text-gray">Rp.{{ number_format($list->harga) }}</h4>
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="#" class="badge badge-primary p-2 rounded"><i class="fa fa-print"></i></a>
        </li>
        <li class="list-inline-item">
          <a href="#" class="badge badge-primary p-2 rounded"><i class="fa fa-exchange"></i></a>
        </li>
        <li class="list-inline-item">
          <a href="#" class="badge badge-primary p-2 rounded"><i class="fa fa-heart"></i></a>
        </li>
      </ul>
    </div>
  </div>
</div>
