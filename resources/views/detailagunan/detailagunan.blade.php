@extends('layouts.detail')

@section('content_detail')
<section class="single__Detail">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        @include('detailagunan.imageslider')
        @include('detailagunan.description')
      </div>
      <div class="col-lg-4">
        @include('detailagunan.filterdetail')
        @include('detailagunan.formminat')
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cta-v1 py-5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9">
        <h2 class="text-uppercase text-white">Ingin membeli property atau mengikuti lelang?</h2>
        <p class="text-capitalize text-white">Kami Akan Membantu Anda untuk Properti Terbaik</p>
      </div>
      <div class="col-lg-3">
        <a href="#" class="btn btn-light text-uppercase ">dapatkan penawaran <i class="fa fa-angle-right ml-3 arrow-btn "></i></a>
      </div>
    </div>
  </div>
</section>
@endsection
