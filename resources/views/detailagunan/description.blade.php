<div class="row">
  <div class="col-lg-12">
    <div class="single__detail-desc">
      <h6 class="text-capitalize detail-heading">description</h6>
      <div class="show__more">
        {!! $list->keterangan !!}
        <a href="javascript:void(0)" class="show__more-button ">read more</a>
      </div>
    </div>
    <div class="clearfix"></div>
    <!-- more detail -->
    <div class="single__detail-features">
      <h6 class="text-capitalize detail-heading">Detail Agunan</h6>
      <div class="property__detail-info">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <ul class="property__detail-info-list list-unstyled">
              <li><b>Kode Property :</b> {{ $list->kode_agunan }}</li>
              <li><b>Jenis Agunan :</b> {{ $list->agunan }}</li>
              <li><b>Harga :</b> Rp.{{ number_format($list->harga) }}</li>
              <li><b>Luas Bangunan:</b> {{ $list->luas_bangunan }} ㎡</li>
              <li><b>Luas Tanah:</b> {{ $list->luas_tanah }} ㎡</li>
              <li><b>Property Status:</b> {{ $list->jenis_jual == 1 ? 'Lelang':'Jual Sukarela' }}</li>
            </ul>
          </div>
          <div class="col-md-6 col-lg-6">
            <ul class="property__detail-info-list list-unstyled">
              <li><b>Kamar Tidur:</b> {{ $list->jumlah_kamar_tidur }}</li>
              <li><b>Kamar Mandi:</b> {{ $list->jumlah_kamar_mandi }}</li>
              <li><b>Jumlah Lantai:</b> {{ $list->jumlah_lantai }}</li>
              <li><b>Garasi:</b> {{ $list->jumlah_garasi_carport }}</li>
              <li><b>Jenis Kepemilikan:</b> {{ $list->jenis_kepemilikan }}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="single__detail-features">
      <h6 class="text-capitalize detail-heading">Denah Property</h6>
      <div id="accordion" class="floorplan" role="tablist">
        @foreach ($dnh as $key=>$isi)
        <div class="card">
          <div class="card-header" role="tab" id="heading{{ $isi->id }}">
            <a class="text-capitalize" data-toggle="collapse" href="#collapse{{ $isi->id }}" aria-expanded="true" aria-controls="collapse{{ $isi->id }}">
              Denah {{ ++$key }}
            </a>
          </div>
          <div id="collapse{{ $isi->id }}" class="collapse show" role="tabpanel" aria-labelledby="heading{{ $isi->id }}" data-parent="#accordion">
            <div class="card-body">
              <figure> <img src="{{ asset('assets/img/denah/'.$isi->image) }}" alt="" class="img-fluid"> </figure>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
    <!-- on video -->
    <div class="single__detail-features">
      <h6 class="text-capitalize detail-heading">property video</h6>
      <div class="single__detail-features-video">
        <figure class=" mb-0">
          <div class="home__video-area text-center">
            <div class="row">
              @foreach ($vid as $key)
              <div class="col-md-6">
                <iframe width="340" height="300" src="{{ $key->linkyoutube }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <a href="{{ $key->linkyoutube }}" class="play-video- "><i class="icon fa fa-pla text-whit"></i></a>
              </div>
              @endforeach
            </div>
          </div>
        </figure>
      </div>
    </div>
    <div class="single__detail-features">
      <h6 class="text-capitalize detail-heading">Peta Lokasi</h6>
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-map-location" role="tabpanel" aria-labelledby="pills-map-location-tab">
          <div id="map-canvas">
            <iframe class="h600 w100"
              src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA8Lo7ml-uJcGwnpY0JiSPv8IYG3nr5MTI&q={{ $mkr->latitude }},{{ $mkr->logitude }};z=14"
              style="border:0;" allowfullscreen="" aria-hidden="false"
              tabindex="0"></iframe>
            <!-- <iframe class="h600 w100"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.753696494286!2d107.60857001508975!3d-6.920021095000047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e62f992e820b%3A0x673fbe01d2e3feaa!2sbank%20bjb%20Kantor%20Pusat!5e0!3m2!1sid!2sid!4v1608113118007!5m2!1sid!2sid"
              style="border:0;" allowfullscreen="" aria-hidden="false"
              tabindex="0"></iframe> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
