<div class="products__filter mb-30">
  <div class="products__filter__group">
    <div class="products__filter__header" style="padding: 1rem;">
      <h5 class="text-center text-capitalize">Cari Agunan</h5>
    </div>
    <div class="products__filter__body">
      <div class="form-group">
        <select class="form-group" id="carajual" name="carajual">
          <option value=""></option>
          <option value="1">Lelang</option>
          <option value="0">Jual Sukarela</option>
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" id="katgr" name="katgr">
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" id="harmin" name="harmin">
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" id="harmax" name="harmax">
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" id="provin" name="provin">
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" id="ktaa" name="ktaa">
          <option data-display="Kabupaten Kota">- Kabupaten Kota -</option>
        </select>
      </div
      >
    </div>
    <div class="products__filter__footer">
      <div class="products__filter__footer">
        <div class="form-group mb-0">
          <button class="btn btn-primary text-capitalize btn-block"><i class="fa fa-search ml-1"></i> cari property </button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="products__filter mb-30">
  <div class="products__filter__group">
    <div class="products__filter__header">
      <h5 class="text-center text-capitalize">simulasi KPR </h5>
      <a> hubungi call centre untuk info lebih lanjut</a>
    </div>
    <div class="products__filter__body">
      <div class="form-group">
        <label>Harga Jual</label>
        <div class="input-group">
          <div class="input-group-prepend">
              <span class="input-group-text">Rp</span>
          </div>
          <input type="text" class="form-control" placeholder="500.000.000">
        </div>
      </div>
      <div class="form-group">
        <label>Uang Muka</label>
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Rp</span>
          </div>
          <input type="text" class="form-control" placeholder=" 100.000.000 ">
        </div>
      </div>
      <div class="form-group">
        <label>Tenor</label>
        <select class="select_option wide">
          <option value="1">10</option>
          <option value="2">15</option>
          <option value="3">20</option>
          <option value="4">25</option>
        </select>
      </div>
      <div class="form-group">
        <label>Suku bunga</label>
        <div class="input-group">
          <div class="input-group-prepend"><span class="input-group-text">%</span></div>
          <input type="text" class="form-control" placeholder="7% fixed 1 tahun">
        </div>
      </div>
    </div>
    <div class="products__filter__footer">
      <div class="form-group mb-0">
        <button class="btn btn-primary text-capitalize btn-block"> hitung <i class="fa fa-calculator ml-1"></i></button>
      </div>
    </div>
  </div>
</div>
