<div class="sticky-top">
  <div class="profile__agent mb-30">
    <div class="profile__agent__group">
      <div class="profile__agent__header">
        <div class="profile__agent__header-avatar">
          <figure><img src="{{ asset('modules/frontend/img/logobjb.png') }}" alt="" class="img-fluid"></figure>
          <ul class="list-unstyled mb-0">
            <li><h5 class="text-capitalize">Formulir Minat</h5></li>
            <li><a href="tel:123456"><i class="fa fa-phone-square mr-1"></i>(123)456-7890</a></li>
            <li>
              <a href="javascript:void(0)"><i class=" fa fa-building mr-1"></i>Div PPK bank bjb</a><p>
              <p class="mb-2">Hubungi Call Centre Div PPK bjb</p>
              <button class="btn btn-social btn-social-o whatsapp mr-1"><i class="fa fa-whatsapp"></i></button>
            </li>
          </ul>
        </div>
      </div>
      <div class="profile__agent__body">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Nama">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Mobile Phone">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Alamat Email">
        </div>
        <div class="form-group mb-0">
          <textarea class="form-control required" rows="5" required="required" placeholder="I'm interest in [ Listing Single ]"></textarea>
        </div>
      </div>
      <div class="profile__agent__footer">
        <div class="form-group mb-0">
          <button class="btn btn-primary text-capitalize btn-block"> send message <i class="fa fa-paper-plane ml-1"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="download mb-0">
    <h5 class="text-center text-capitalize">Property Attachments</h5>
    <div class="download__item">
      <a href="#" target="_blank"><i class="fa fa-file-pdf-o mr-3" aria-hidden="true"></i>Download Document.Pdf</a>
    </div>
  </div>
</div>
