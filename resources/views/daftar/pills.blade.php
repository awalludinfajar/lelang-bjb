<ul class="nav nav-pills myTab" role="tablist">
  <li class="list-inline-item mr-auto">
    <span class="title-text">Sort by</span>
    <div class="btn-group">
      <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $bagian }}</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="javascript:void(0)">Low to High Price</a>
        <a class="dropdown-item" href="javascript:void(0)">High to Low Price</a>
      </div>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="pill" href="#pills-tab-one" role="tab" aria-controls="pills-tab-one" aria-selected="true">
      <span class="fa fa-th-list"></span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" data-toggle="pill" href="#pills-tab-two" role="tab" aria-controls="pills-tab-two" aria-selected="false">
      <span class="fa fa-th-large"></span>
    </a>
  </li>
</ul>
