@extends('layouts.detail')

@section('content_detail')

@include('daftar.breadcumb')

@include('content.filter')

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="tabs__custom-v2 ">
              @include('daftar.pills')
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="pills-tab-one" role="tabpanel" aria-labelledby="pills-tab-one">
                  @foreach ($data as $key)
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="card__image card__box-v1">
                        <div class="row no-gutters">
                          <div class="col-md-4 col-lg-3 col-xl-4">
                            <div class="card__image__header h-250">
                              <a href="#">
                                <div class="ribbon text-capitalize">@if($key->hot_price == 'Yes') HOT PRICE @else DIJUAL @endif</div>
                                <img src="{{ asset('assets/img/agunan/'.$key->image) }}" alt="" class="img-fluid w100 img-transition">
                                <div class="info">@if($key->jenis_jual == 1) LELANG SEGERA @else JUAL SUKARELA @endif</div>
                              </a>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-6 col-xl-5 my-auto" onclick="return window.location = '{{ url('detailagunan/'.$key->id_ang) }}'">
                            <div class="card__image__body">
                              <span class="badge badge-primary text-capitalize mb-2">{{ $key->agunan }} Kode Lelang {{ $key->kode_agunan }}</span>
                              <h6><a href="#">{{ $key->judul_agunan }}</a></h6>
                              <div class="card__image__body-desc">
                                <p class="text-capitalize"><i class="fa fa-map-marker"></i>{{ $key->kabkot }}, {{ $key->kecamatan }}</p>
                              </div>
                              <ul class="list-inline card__content">
                                <li class="list-inline-item"><span>KM <br><i class="fa fa-bath"></i>{{ $key->jumlah_kamar_mandi }}</span></li>
                                <li class="list-inline-item"><span>KT <br><i class="fa fa-bed"></i>{{ $key->jumlah_kamar_tidur }}</span></li>
                                <li class="list-inline-item"><span>Luas Bangunan <br><i class="fa fa-inbox"></i>{{ $key->luas_bangunan }} ㎡</span></li>
                                <li class="list-inline-item"><span>Luas Tanah <br><i class="fa fa-map"></i>{{ $key->luas_tanah }} ㎡</span></li>
                              </ul>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-3 col-xl-3 my-auto card__image__footer-first">
                            <div class="card__image__footer">
                              <figure><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid rounded-circle"></figure>
                              <ul class="list-inline my-auto">
                                <li class="list-inline-item name"><a href="#">Div PPK bjb</a></li>
                              </ul>
                              <ul class="list-inline my-auto ml-auto price">
                                <li class="list-inline-item "><h6>Call</h6></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
                <div class="tab-pane fade show active" id="pills-tab-two" role="tabpanel" aria-labelledby="pills-tab-two">
                  <div class="row">
                    @foreach ($data as $key)
                    <div class="col-md-4 col-lg-4">
                      <div class="card__image card__box-v1">
                        <div class="card__image-header h-250">
                          <div class="ribbon text-capitalize">@if($key->hot_price == 'Yes') HOT PRICE @else DIJUAL @endif</div>
                          <img src="{{ asset('assets/img/agunan/'.$key->image) }}" alt="" class="img-fluid w100 img-transition">
                          <div class="info">@if($key->jenis_jual == 1) LELANG SEGERA @else JUAL SUKARELA @endif</div>
                        </div>
                        <div class="card__image-body" onclick="return window.location = '{{ url('detailagunan/'.$key->id_ang) }}'">
                          <span class="badge badge-primary text-capitalize mb-2">{{ $key->agunan }} Kode Lelang {{ $key->kode_agunan }}</span>
                          <h6 class="text-capitalize">{{ $key->judul_agunan }}</h6>
                          <p class="text-capitalize"><i class="fa fa-map-marker"></i>{{ $key->kabkot }}, {{ $key->kecamatan }}</p>
                          <ul class="list-inline card__content">'+
                            <li class="list-inline-item"><span>KM <br><i class="fa fa-bath"></i>{{ $key->jumlah_kamar_mandi }}</span></li>
                            <li class="list-inline-item"><span>KT <br><i class="fa fa-bed"></i>{{ $key->jumlah_kamar_tidur }}</span></li>
                            <li class="list-inline-item"><span>Luas Bangunan <br><i class="fa fa-inbox"></i>{{ $key->luas_bangunan }} ㎡</span></li>
                            <li class="list-inline-item"><span>Luas Tanah <br><i class="fa fa-map"></i>{{ $key->luas_tanah }} ㎡</span></li>
                          </ul>
                        </div>
                        <div class="card__image-footer">
                          <figure><img src="{{ asset('modules/frontend/img/80x80.jpg') }}" alt="" class="img-fluid rounded-circle"></figure>
                          <ul class="list-inline my-auto">
                            <li class="list-inline-item "><a href="#">Div PPK bjb</a></li>
                          </ul>
                          <ul class="list-inline my-auto ml-auto">
                            <li class="list-inline-item"><h6>Call</h6></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cta-v1 py-5">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-9">
        <h2 class="text-uppercase text-white">Ingin membeli property atau mengikuti lelang?</h2>
        <p class="text-capitalize text-white">Kami Akan Membantu Anda untuk Properti Terbaik</p>
      </div>
      <div class="col-lg-3">
        <a href="#" class="btn btn-light text-uppercase ">dapatkan penawaran <i class="fa fa-angle-right ml-3 arrow-btn "></i></a>
      </div>
    </div>
  </div>
</section>
@endsection
