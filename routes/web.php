<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AgunanController;
use Modules\Backend\Http\Controllers\BeritaController;
use Modules\Agunan\Http\Controllers\DataAgunanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('info')->group(function() {
  Route::get('/tentangkami', function (){
    return view('tentangkami.moreabout');
  });
  Route::get('/faq', function (){
    return view('faq.faqq');
  });
});

Route::get('baca/berita/{judul}/{id}', [BeritaController::class, 'showfrontend']);
Route::get('detailagunan/{id}', [DataAgunanController::class, 'showdetail']);
Route::get('list/{jenis}', [DataAgunanController::class, 'listagunan']);
// Route::get('/bangunan', [AgunanController::class, 'bangunan']);
