<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrnAgunan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('trn_agunan', function (Blueprint $table) {
        $table->id();
        $table->string('kode_agunan');
        $table->string('judul_agunan');
        $table->integer('id_jenis_agunan');
        $table->text('alamat');
        $table->integer('rt');
        $table->integer('rw');
        $table->bigInteger('id_kelurahan');
        $table->integer('id_kecamatan');
        $table->integer('id_kota');
        $table->integer('id_provinsi');
        $table->integer('id_kodepos');
        $table->integer('jumlah_kamar_tidur');
        $table->integer('jumlah_kamar_mandi');
        $table->integer('jumlah_garasi_carport');
        $table->integer('jumlah_lantai');
        $table->integer('luas_tanah');
        $table->integer('luas_bangunan');
        $table->bigInteger('price');
        $table->integer('id_kepemilikan');
        $table->date('tanggal_kadaluarsa_kepemilikan');
        $table->text('keterangan');
        $table->integer('user_input');
        $table->enum('hot_price',['Yes','No']);
        $table->enum('status',[0,1]);
        $table->string('jenis_jual');
        $table->date('tanggal_lelang');
        $table->enum('publikasi',[0,1]);
        $table->integer('user_publikasi');
        $table->enum('disabld',[0,1]);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trn_agunan');
    }
}
