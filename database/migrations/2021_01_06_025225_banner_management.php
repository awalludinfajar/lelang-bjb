<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BannerManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('banner_management', function (Blueprint $table) {
        $table->id();
        $table->string('nama_banner');
        $table->text('deskripsi');
        $table->string('image');
        $table->integer('slide');
        $table->enum('active',[0,1]);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('banner_management');
    }
}
