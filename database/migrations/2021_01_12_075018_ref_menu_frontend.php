<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefMenuFrontend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ref_menu_frontend', function (Blueprint $table) {
        $table->id();
        $table->integer('parrent');
        $table->string('nama_menu');
        $table->string('link');
        $table->integer('urutan');
        $table->enum('class_active',[0,1]);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
