<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefBlogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ref_blogs', function (Blueprint $table) {
        $table->id();
        $table->string('author');
        $table->string('judul');
        $table->text('description');
        $table->string('image');
        $table->string('jenis_berita');
        $table->enum('active',[0,1]);
        $table->enum('tampil_beranda',[0,1]);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
