<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrnJumlahHariLelangSegera extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('trn_jumlah_hari_lelang_segera', function (Blueprint $table) {
        $table->id();
        $table->integer('id_agunan');
        $table->date('tanggal_batas_akhir');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
