<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\RefBlogs;
use Illuminate\Routing\Controller as BaseController;

class BlogsController extends Controller
{
  public function blogs($value='')
  {

    $data['blogs'] = RefBlogs::all();
    return view('blogs.blogs-read')->with('blogs', $data['blogs']);
  }
  
      // kontak
      public function detail($judul)
      {
          $model  = new RefBlogs();
          $data = RefBlogs::where('ref_blogs.judul',$judul)
                    ->select
                      (
                        'author',
                        'judul',
                        'description',
                        'image',
                        'jenis_berita',
                        'active',
                        'created_at'
                      )
                    ->orderby('ref_blogs.id','asc')
                    ->get();

          $terkini = RefBlogs::select
                      (
                        'author',
                        'judul',
                        'description',
                        'image',
                        'jenis_berita',
                        'active',
                        'created_at'
                      )
                    ->orderby('ref_blogs.created_at','asc')
                    ->where('ref_blogs.active','1')
                    ->limit(5)
                    ->get();

          $previous = RefBlogs::select
                      (
                        'author',
                        'judul',
                        'description',
                        'image',
                        'jenis_berita',
                        'active',
                        'created_at'
                      )
                    ->where('ref_blogs.active','1')
                    ->inRandomOrder()
                    ->limit(1)
                    ->get();

          $nextpost = RefBlogs::select
                      (
                        'author',
                        'judul',
                        'description',
                        'image',
                        'jenis_berita',
                        'active',
                        'created_at'
                      )
                    ->where('ref_blogs.active','1')
                    ->inRandomOrder()
                    ->limit(1)
                    ->get();
  
    return view('blogs.blogs-read',['data'=>$data, 'terkini'=>$terkini, 'previous'=>$previous, 'nextpost'=>$nextpost]);
      }
}