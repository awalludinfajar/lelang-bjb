class global{

    readURL(input, idName){
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#'+idName).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    delete(id, url){
        var token = $('meta[name="csrf-token"]').attr('content');
        Swal.fire({
            title: 'Apa anda yakin?',
            text: "Menghapus data ini",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Iya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if (result.isConfirmed) {
                $.post(url,{id:id,_token: token}, function (data) {
                    if(data.status == 200){
                        Swal.fire({
                            title: 'Hapus!',
                            text: data.message,
                            icon: 'success'
                        }).then((result) => {
                            location.reload();
                        });
                    }else{
                        Swal.fire(
                            'Hapus!',
                            'Gagal Dihapus',
                            'error'
                        )
                    }
                })
            }
        })
    }

    dragdropImage(tag,foldern,idName,idPreview, idProgress, url, link){
        var dropRegion = document.getElementById(idName);
        var previewRegion = document.getElementById(idPreview);
        var progressImage = document.getElementById(idProgress);

        var fakeInput = document.createElement("input");
            fakeInput.type = "file";
            fakeInput.accept = "image/*";
            fakeInput.multiple = true;

            dropRegion.addEventListener('click', function(){
                fakeInput.click();
            })

            fakeInput.addEventListener('change', function(){
                let files = fakeInput.files;
                handleFile(files);
            })

        function preventDefault(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        dropRegion.addEventListener('dragenter', preventDefault, false);
        dropRegion.addEventListener('dragleave', preventDefault, false);
        dropRegion.addEventListener('dragover', preventDefault, false);
        dropRegion.addEventListener('drop', preventDefault, false);

        function handleDrop(d) {
            let dt = d.dataTransfer;
            var files = dt.files;

            if(files.length){
                handleFile(files);
            }
        }

        dropRegion.addEventListener('drop', handleDrop, false);

        function handleFile(files) {
            // for (let index = 0; index < files.length; index++) {
            if(files[0]){
                const validate = files[0];
                if(validateImage(validate)){
                    previewandupload(validate);
                }
            }
            // }
        }

        function validateImage(image) {
            // check type image
            var validType = ['image/jpg','image/jpeg','image/png','image/gif'];
            if(validType.indexOf(image.type) === -1){
                alert('Invalid File Type');
                return false;
            }

            // check size
            var maxSizeByte = 10e6; // 10MB
            if(image.size > maxSizeByte){
                alert('File too large')
                return false;
            }

            return true;
        }

        function previewandupload(image) {
            progressImage.innerHTML = "";
            let imgPreview = document.createElement('div');
                progressImage.appendChild(imgPreview);

            let progressDiv = document.createElement('div');
                progressDiv.className = "progress mt-2";
                progressDiv.id="progress";
                progressDiv.style = "height: 15px;";

                imgPreview.appendChild(progressDiv);

            let progressBar = document.createElement('div');
                progressBar.className = "progress-bar";
                progressBar.role = "progressbar";
                progressBar.style = "width: 0%;";
                progressBar.innerHTML = "0%";

                progressDiv.appendChild(progressBar);

            // create FormData
            var formData = new FormData();
            formData.append('image', image);

            // console.log(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url : url,
                type : 'POST',
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                enctype: 'multipart/form-data',
                xhr: function(){
                    //upload Progress
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) {
                        xhr.upload.addEventListener('progress', function(event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable)
                            {
                                percent = Math.ceil(position / total * 100);
                            }
                            progressBar.style = "width: "+percent+"%";
                            progressBar.innerHTML = percent+"%";
                            // console.log(percent);
                        }, true);
                    }
                    return xhr;
                },
                success: function(params) {
                  progressDiv.style = "display: none;"
                    if(params.status == 200){
                      let listDiv = document.createElement('div');
                          listDiv.id = "image_"+params.fullName;
                          listDiv.className = "col-md-4";
                          listDiv.style = "margin-bottom: 15px;";
                          previewRegion.appendChild(listDiv);

                      let crdimg = document.createElement('div');
                          crdimg.className = "card";
                          listDiv.appendChild(crdimg);

                      let img = document.createElement('img');
                          img.style = "width: 250px;";
                          img.src = link+"/assets/img/"+foldern+"/"+params.fullName;
                          listDiv.appendChild(img);

                      let inputList = document.createElement('input');
                          inputList.value = params.fullName;
                          inputList.name = tag+"[]";
                          inputList.id = tag;
                          inputList.hidden = true;
                          listDiv.appendChild(inputList);

                      let actionList = document.createElement('button');
                          actionList.className = 'btn btn-danger btn-block';
                          actionList.type = "button";
                          actionList.onclick = function(){
                              destroyImage(params.fullName, foldern);
                          };
                          actionList.style = 'border-top-right-radius: 2px !important;border-bottom-right-radius: 2px !important;'
                          actionList.innerHTML = '<i class="fa fa-trash"></i> Hapus';
                          listDiv.appendChild(actionList);

                        // let listDiv = document.createElement('div');
                        //     listDiv.id = "listImage_"+params.id;
                        //     listDiv.className = "card m-0 mt-2";
                        //     previewRegion.appendChild(listDiv);
                        //
                        // let valueList = document.createElement('div');
                        //     valueList.className = "d-flex justify-content-between";
                        //     listDiv.appendChild(valueList);
                        //
                        // let img = document.createElement("img");
                        //     img.className = 'p-2 rounded w-50';
                        //     img.src = link+"/assets/img/agunan/"+params.fullName;
                        //     valueList.appendChild(img);
                        //
                        // let inputList = document.createElement('input');
                        //     inputList.value = params.fullName;
                        //     inputList.name = "list_img[]";
                        //     // inputList.type = 'file';
                        //     inputList.hidden = true;
                        //     img.appendChild(inputList);
                        //
                        // let actionList = document.createElement('button');
                        //     actionList.className = 'btn btn-danger rounded-0 ';
                        //     actionList.type = "button";
                        //     actionList.onclick = function(){
                        //         destroyImage(params.id);
                        //     };
                        //     actionList.style = 'border-top-right-radius: 2px !important;border-bottom-right-radius: 2px !important;'
                        //     actionList.innerHTML = '<i class="fa fa-trash"></i>';
                        //     valueList.appendChild(actionList);
                    }
                }
            })
        }
    }

    dragdropVideo(idName,idPreview, idProgress, url, link){
        var dropRegion = document.getElementById(idName);
        var previewRegion = document.getElementById(idPreview);
        var progressImage = document.getElementById(idProgress);

        var fakeInput = document.createElement("input");
            fakeInput.type = "file";
            fakeInput.accept = "video/*";
            fakeInput.multiple = true;

            dropRegion.addEventListener('click', function(){
                fakeInput.click();
            })

            fakeInput.addEventListener('change', function(){
                let files = fakeInput.files;
                handleFile(files);
            })

        function preventDefault(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        dropRegion.addEventListener('dragenter', preventDefault, false);
        dropRegion.addEventListener('dragleave', preventDefault, false);
        dropRegion.addEventListener('dragover', preventDefault, false);
        dropRegion.addEventListener('drop', preventDefault, false);

        function handleDrop(d) {
            let dt = d.dataTransfer;
            var files = dt.files;

            if(files.length){
                handleFile(files);
            }
        }

        dropRegion.addEventListener('drop', handleDrop, false);

        function handleFile(files) {
            // for (let index = 0; index < files.length; index++) {
            if(files[0]){
                const validate = files[0];
                if(validateVideo(validate)){
                    previewandupload(validate);
                }
            }
            // }
        }

        function validateVideo(image) {
            // check type image
            var validType = ['video/mp4','video/3gp','video/MOV','video/mkv'];
            if(validType.indexOf(image.type) === -1){
                alert('Invalid File Type');
                return false;
            }

            // check size
            var maxSizeByte = 10e6; // 10MB
            if(image.size > maxSizeByte){
                alert('File too large')
                return false;
            }

            return true;
        }

        function previewandupload(image) {
            progressImage.innerHTML = "";
            let imgPreview = document.createElement('div');
                progressImage.appendChild(imgPreview);

            let progressDiv = document.createElement('div');
                progressDiv.className = "progress mt-2";
                progressDiv.id="progress";
                progressDiv.style = "height: 15px;";

                imgPreview.appendChild(progressDiv);

            let progressBar = document.createElement('div');
                progressBar.className = "progress-bar";
                progressBar.role = "progressbar";
                progressBar.style = "width: 0%;";
                progressBar.innerHTML = "0%";

                progressDiv.appendChild(progressBar);

            // create FormData
            var formData = new FormData();
            formData.append('video', image);

            console.log(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url : url,
                type : 'POST',
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                enctype: 'multipart/form-data',
                xhr: function(){
                    //upload Progress
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) {
                        xhr.upload.addEventListener('progress', function(event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable)
                            {
                                percent = Math.ceil(position / total * 100);
                            }
                            progressBar.style = "width: "+percent+"%";
                            progressBar.innerHTML = percent+"%";
                            // console.log(percent);
                        }, true);
                    }
                    return xhr;
                },
                success: function(params) {
                    if(params.status == 200){
                        let listDiv = document.createElement('div');
                            listDiv.id = "listVideo_"+params.id;
                            listDiv.className = "card m-0 mt-2";
                            previewRegion.appendChild(listDiv);

                        let valueList = document.createElement('div');
                            valueList.className = "d-flex justify-content-between";
                            listDiv.appendChild(valueList);

                        let vid = document.createElement('video');
                            vid.setAttribute('width','320');
                            vid.setAttribute('height','240');
                            vid.setAttribute('controls','');
                            valueList.appendChild(vid);

                        let soc = document.createElement('source');
                            soc.className = 'p-2 rounded float-left w-50';
                            soc.src = link+"/assets/vid/agunan/"+params.fullName;
                            // soc.setAttribute('type')
                            vid.appendChild(soc);

                        // let img = document.createElement("img");
                        //     img.className = 'p-2 rounded float-left w-75';
                        //     img.src = link+"/assets/vid/agunan/"+params.fullName;
                        //     valueList.appendChild(img);

                        let inputList = document.createElement('input');
                            inputList.value = params.fullName;
                            inputList.name = "list_vid[]";
                            inputList.type = 'file';
                            inputList.hidden = true;
                            vid.appendChild(inputList);

                        let actionList = document.createElement('button');
                            actionList.className = 'btn btn-danger rounded-0 ';
                            actionList.type = "button";
                            actionList.onclick = function(){
                                destroyImage(params.id);
                            };
                            actionList.style = 'border-top-right-radius: 2px !important;border-bottom-right-radius: 2px !important;'
                            actionList.innerHTML = '<i class="fa fa-trash"></i>';
                            valueList.appendChild(actionList);
                    }
                }
            })
        }
    }
}
var Global = new global();
